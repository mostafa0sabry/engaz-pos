﻿
using System;
using System.Data;

class Employee_Attendance_cls :DataAccessLayer
    {

    //MaxID
    public String MaxID_Employee_Attendance()
    {
        return Execute_SQL("select ISNULL (MAX(AttendanceID)+1,1) from Employee_Attendance", CommandType.Text);
    }


    // InsertSTR
    public void Insert_Employee_Attendance(String AttendanceID, String EmployeeID, DateTime MyDate, DateTime Hower_Come, Boolean States)
    {
        Execute_SQL("insert into Employee_Attendance(AttendanceID ,EmployeeID ,MyDate ,Hower_Come ,States )Values (@AttendanceID ,@EmployeeID ,@MyDate ,@Hower_Come ,@States )", CommandType.Text,
        Parameter("@AttendanceID", SqlDbType.Int, AttendanceID),
        Parameter("@EmployeeID", SqlDbType.Int, EmployeeID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@Hower_Come", SqlDbType.DateTime, Hower_Come),
        //Parameter("@Hower_Go", SqlDbType.DateTime, Hower_Go),
        Parameter("@States", SqlDbType.Bit, States));
    }

    //Update
    public void Update_Employee_Attendance(string AttendanceID, DateTime Hower_Go,string Hower_Work, Boolean States)
    {
        Execute_SQL("Update Employee_Attendance Set AttendanceID=@AttendanceID ,Hower_Go=@Hower_Go,Hower_Work=@Hower_Work ,States=@States  where AttendanceID=@AttendanceID", CommandType.Text,
        Parameter("@AttendanceID", SqlDbType.NVarChar, AttendanceID),
        Parameter("@Hower_Go", SqlDbType.DateTime, Hower_Go),
          Parameter("@Hower_Work", SqlDbType.NVarChar, Hower_Work),
        Parameter("@States", SqlDbType.Bit, States));
    }




    //Delete
    public void Delete_Employee_Attendance(string AttendanceID)
    {
        Execute_SQL("Delete  From Employee_Attendance where AttendanceID=@AttendanceID", CommandType.Text,
        Parameter("@AttendanceID", SqlDbType.NVarChar, AttendanceID));
    }


    //Details ID
    public DataTable Search_Employee_Attendance(DateTime MyDate)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Attendance.AttendanceID, dbo.Employee_Attendance.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Attendance.MyDate, dbo.Employee_Attendance.Hower_Come, 
                         dbo.Employee_Attendance.Hower_Go, dbo.Employee_Attendance.States
FROM            dbo.Employees INNER JOIN
                         dbo.Employee_Attendance ON dbo.Employees.EmployeeID = dbo.Employee_Attendance.EmployeeID Where MyDate=@MyDate", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate));
    }

    //Details ID
    public DataTable Search_Employee_Attendance(DateTime MyDate,Boolean States)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Attendance.AttendanceID, dbo.Employee_Attendance.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Attendance.MyDate, dbo.Employee_Attendance.Hower_Come, 
                         dbo.Employee_Attendance.Hower_Go, dbo.Employee_Attendance.States
FROM            dbo.Employees INNER JOIN
                         dbo.Employee_Attendance ON dbo.Employees.EmployeeID = dbo.Employee_Attendance.EmployeeID Where MyDate=@MyDate and dbo.Employee_Attendance.States=@States", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
          Parameter("@States", SqlDbType.Bit, States));
    }
    //Details ID
    public DataTable NameSearch_Employee_Attendance(string EmployeeID,DateTime MyDate)
    {
        return ExecteRader("Select *  from Employee_Attendance Where EmployeeID=@EmployeeID and MyDate=@MyDate", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
                Parameter("@MyDate", SqlDbType.Date, MyDate));
    }

    public DataTable NameSearch_Employee_Attendance(string EmployeeID,Boolean States)
    {
        return ExecteRader("Select *  from Employee_Attendance  Where EmployeeID=@EmployeeID and States=@States", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
        Parameter("@States",SqlDbType.Bit, States));
    }


    //Details ID
    public DataTable Search_RPT_Employee_Attendance(string EmployeeID, DateTime MyDate, Boolean States)
    {
        return ExecteRader("SELECT * FROM Employee_Attendance WHERE  EmployeeID=@EmployeeID and MONTH(MyDate)='"+MyDate.Month+ "' and YEAR(MyDate)='" + MyDate.Year + "' and States=@States", CommandType.Text,
       Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
          Parameter("@States", SqlDbType.Bit, States));
    }
    public DataTable Search_RPT_Total_Employee_Attendance()
    {
        return ExecteRader("select * from Attendance_Report where AttendanceID='0'", CommandType.Text);
    }
}

