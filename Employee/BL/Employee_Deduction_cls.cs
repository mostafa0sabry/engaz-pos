﻿using System;
using System.Data;
// الاستحقاقات
class Employee_Deduction_cls : DataAccessLayer
    {
    //MaxID
    public String MaxID_Employee_Deduction()
    {
        return Execute_SQL("select ISNULL (MAX(Deduction_ID)+1,1) from Employee_Deduction", CommandType.Text);
    }


    // Insert
    public void Insert_Employee_Deduction(string Deduction_ID,DateTime MyDate, string EmployeeID, String DeductionType_ID, string Deduction_Value, string TreasuryID, string Remarks, int UserAdd)
    {
        Execute_SQL("insert into Employee_Deduction(Deduction_ID ,MyDate,EmployeeID ,DeductionType_ID ,Deduction_Value ,TreasuryID ,Remarks ,UserAdd )Values (@Deduction_ID,@MyDate ,@EmployeeID ,@DeductionType_ID ,@Deduction_Value ,@TreasuryID ,@Remarks ,@UserAdd )", CommandType.Text,
        Parameter("@Deduction_ID", SqlDbType.NVarChar, Deduction_ID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID), 
        Parameter("@DeductionType_ID", SqlDbType.NVarChar, DeductionType_ID),
        Parameter("@Deduction_Value", SqlDbType.Float, Deduction_Value),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void Update_Employee_Deduction(string Deduction_ID,DateTime MyDate, string EmployeeID, String DeductionType_ID, string Deduction_Value, string TreasuryID, string Remarks)
    {
        Execute_SQL("Update Employee_Deduction Set Deduction_ID=@Deduction_ID ,EmployeeID=@EmployeeID,MyDate=@MyDate ,DeductionType_ID=@DeductionType_ID ,Deduction_Value=@Deduction_Value ,TreasuryID=@TreasuryID ,Remarks=@Remarks  where Deduction_ID=@Deduction_ID", CommandType.Text,
        Parameter("@Deduction_ID", SqlDbType.NVarChar, Deduction_ID),
                Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
        Parameter("@DeductionType_ID", SqlDbType.NVarChar, DeductionType_ID),
        Parameter("@Deduction_Value", SqlDbType.Float, Deduction_Value),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@Remarks", SqlDbType.NText, Remarks));
    }

    //Delete
    public void Delete_Employee_Deduction(string Deduction_ID)
    {
        Execute_SQL("Delete  From Employee_Deduction where Deduction_ID=@Deduction_ID", CommandType.Text,
        Parameter("@Deduction_ID", SqlDbType.NVarChar, Deduction_ID));
    }



    //Details ID
    public DataTable Details_Employee_Deduction(string Deduction_ID)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Deduction.Deduction_ID, dbo.Employee_Deduction.MyDate, dbo.Employee_Deduction.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Deduction.DeductionType_ID, 
                         dbo.Employee_DeductionType.DeductionType_Name, dbo.Employee_Deduction.Deduction_Value, dbo.Employee_Deduction.TreasuryID, dbo.Employee_Deduction.Remarks, dbo.Employee_Deduction.UserAdd, 
                         dbo.UserPermissions.EmpName
FROM            dbo.Employee_Deduction INNER JOIN
                         dbo.Employee_DeductionType ON dbo.Employee_Deduction.DeductionType_ID = dbo.Employee_DeductionType.DeductionType_ID INNER JOIN
                         dbo.Employees ON dbo.Employee_Deduction.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                         dbo.UserPermissions ON dbo.Employee_Deduction.UserAdd = dbo.UserPermissions.ID
WHERE(dbo.Employee_Deduction.Deduction_ID =@Deduction_ID)", CommandType.Text,
        Parameter("@Deduction_ID", SqlDbType.NVarChar, Deduction_ID));
    }

    //Details ID
    public DataTable Search_Employee_Deduction(string EmployeeID, DateTime MyDate, DateTime MyDate2)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Deduction.Deduction_ID, dbo.Employee_Deduction.MyDate, dbo.Employee_Deduction.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Deduction.DeductionType_ID, 
                         dbo.Employee_DeductionType.DeductionType_Name, dbo.Employee_Deduction.Deduction_Value, dbo.Employee_Deduction.TreasuryID, dbo.Employee_Deduction.Remarks, dbo.Employee_Deduction.UserAdd, 
                         dbo.UserPermissions.EmpName
FROM            dbo.Employee_Deduction INNER JOIN
                         dbo.Employee_DeductionType ON dbo.Employee_Deduction.DeductionType_ID = dbo.Employee_DeductionType.DeductionType_ID INNER JOIN
                         dbo.Employees ON dbo.Employee_Deduction.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                         dbo.UserPermissions ON dbo.Employee_Deduction.UserAdd = dbo.UserPermissions.ID
Where dbo.Employee_Deduction.EmployeeID=@EmployeeID  and MyDate>= @MyDate And MyDate<= @MyDate2", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    //Details ID
    public DataTable Search_Employee_Deduction( DateTime MyDate, DateTime MyDate2)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Deduction.Deduction_ID, dbo.Employee_Deduction.MyDate, dbo.Employee_Deduction.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Deduction.DeductionType_ID, 
                         dbo.Employee_DeductionType.DeductionType_Name, dbo.Employee_Deduction.Deduction_Value, dbo.Employee_Deduction.TreasuryID, dbo.Employee_Deduction.Remarks, dbo.Employee_Deduction.UserAdd, 
                         dbo.UserPermissions.EmpName
FROM            dbo.Employee_Deduction INNER JOIN
                         dbo.Employee_DeductionType ON dbo.Employee_Deduction.DeductionType_ID = dbo.Employee_DeductionType.DeductionType_ID INNER JOIN
                         dbo.Employees ON dbo.Employee_Deduction.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                         dbo.UserPermissions ON dbo.Employee_Deduction.UserAdd = dbo.UserPermissions.ID
Where  MyDate>= @MyDate And MyDate<= @MyDate2", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }
}

