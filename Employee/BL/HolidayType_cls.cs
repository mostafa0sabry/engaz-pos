﻿using System;
using System.Data;

class HolidayType_cls : DataAccessLayer
    {
    //MaxID
    public String MaxID_EmpHoliday()
    {
        return Execute_SQL("select ISNULL (MAX(HolidayID)+1,1) from HolidayType", CommandType.Text);
    }

    // Insert
    public void Insert_EmpHoliday(int HolidayID, String HolidayName, String DayNumber, String Note)
    {
        Execute_SQL("insert into HolidayType(HolidayID ,HolidayName ,DayNumber ,Note )Values (@HolidayID ,@HolidayName ,@DayNumber ,@Note )", CommandType.Text,
         Parameter("@HolidayID", SqlDbType.Int, HolidayID),
        Parameter("@HolidayName", SqlDbType.NVarChar, HolidayName),
        Parameter("@DayNumber", SqlDbType.NVarChar, DayNumber),
        Parameter("@Note", SqlDbType.NVarChar, Note));
    }

    //Update
    public void Update_EmpHoliday(int HolidayID, String HolidayName, String DayNumber, String Note)
    {
        Execute_SQL("Update HolidayType Set HolidayID=@HolidayID ,HolidayName=@HolidayName ,DayNumber=@DayNumber ,Note=@Note  where HolidayID=@HolidayID", CommandType.Text,
         Parameter("@HolidayID", SqlDbType.Int, HolidayID),
        Parameter("@HolidayName", SqlDbType.NVarChar, HolidayName),
        Parameter("@DayNumber", SqlDbType.NVarChar, DayNumber),
        Parameter("@Note", SqlDbType.NVarChar, Note));
    }

    //Delete
    public void Delete_EmpHoliday(int HolidayID)
    {
        Execute_SQL("Delete  From HolidayType where HolidayID=@HolidayID", CommandType.Text,
        Parameter("@HolidayID", SqlDbType.Int, HolidayID));
    }

    //Details ID
    public DataTable NODelete_Employee_Holiday(string HolidayID)
    {
        return ExecteRader("Select HolidayID  from Employee_Holiday Where HolidayID=@HolidayID", CommandType.Text,
        Parameter("@HolidayID", SqlDbType.Int, HolidayID));
    }


    //Details ID
    public DataTable Details_EmpHoliday(int HolidayID)
    {
        return ExecteRader("Select *  from HolidayType where HolidayID = @HolidayID", CommandType.Text,
        Parameter("@HolidayID", SqlDbType.Int, HolidayID));
    }

    //Details ID
    public DataTable NameSearch_EmpHoliday(String HolidayName)
    {
        return ExecteRader("Select HolidayName  from HolidayType where HolidayName=@HolidayName", CommandType.Text,
        Parameter("@HolidayName", SqlDbType.NVarChar, HolidayName));
    }

    //Search
    public DataTable Search_EmpHoliday( String Search)
    {
        return ExecteRader("Select *  from HolidayType where convert(nvarchar, HolidayID) + HolidayName like '%' + @Search + '%'", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }


}

