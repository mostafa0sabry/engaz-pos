﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Employee_Additions_frm : Form
    {
        public Employee_Additions_frm()
        {
            InitializeComponent();
        }
        Employee_Additions_cls cls = new Employee_Additions_cls();

        private void Discound_frm_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingForm(this);
                btnNew_Click(null, null);
                LoadData();
                combEmployee.Text = "";
                txtEmployeeID.Text = "";
                combEmployee.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        public void LoadData()
        {

            Employee_cls Employee_cls = new Employee_cls();
            string EmployeeName = combEmployee.Text;
            string EmployeeID = txtEmployeeID.Text;
            combEmployee.DataSource = Employee_cls.Search_Employees("");
            combEmployee.DisplayMember = "EmployeeName";
            combEmployee.ValueMember = "EmployeeID";
            combEmployee.Text = EmployeeName;
            txtEmployeeID.Text = EmployeeID;
            //======================================================================================
            string Additions = combAdditionsType.Text;
            string AdditionsID = txtAdditionsType_ID.Text;
            Employee_AdditionsType_cls Employee_AdditionsType_cls = new Employee_AdditionsType_cls();
            combAdditionsType.DataSource = Employee_AdditionsType_cls.Search__Employee_AdditionsType("");
            combAdditionsType.DisplayMember = "AdditionsType_Name";
            combAdditionsType.ValueMember = "AdditionsType_ID";
            combAdditionsType.Text= Additions;
            txtAdditionsType_ID.Text= AdditionsID;

        }



        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {


                combAdditionsType.Text = "";
                txtAdditionsType_ID.Text = "";
                     //======================================================================================
                     txtAdditions_ID.Text = cls.MaxID_Employee_Additions();
                D1.Value = DateTime.Now;
                txtAddition_Value.Text = "";
                txtAdditionsType_ID.Text = "";
                txtRemarks.Text = "";
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                combAdditionsType.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // condetion
                // condetion
                if (txtEmployeeID.Text.Trim() == "")
                {
                    combEmployee.BackColor = Color.Pink;
                    combEmployee.Focus();
                    return;
                }

                // condetion
                else if (txtAdditionsType_ID.Text.Trim() == "")
                {
                    combAdditionsType.BackColor = Color.Pink;
                    combAdditionsType.Focus();
                    return;
                }
                // condetion
                else if (txtAddition_Value.Text.Trim() == "")
                {
                    txtAddition_Value.BackColor = Color.Pink;
                    txtAddition_Value.Focus();
                    return;
                }
                else if (Convert.ToDouble(txtAddition_Value.Text.Trim()) == 0)
                {
                    txtAddition_Value.BackColor = Color.Pink;
                    txtAddition_Value.Focus();
                    return;
                }

                txtAdditions_ID.Text = cls.MaxID_Employee_Additions();
                cls.Insert_Employee_Additions(txtAdditions_ID.Text,D1.Value, txtEmployeeID.Text, txtAdditionsType_ID.Text, txtAddition_Value.Text, txtRemarks.Text, Properties.Settings.Default.UserID);             
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // condetion
                if (txtEmployeeID.Text.Trim() == "")
                {
                    combEmployee.BackColor = Color.Pink;
                    combEmployee.Focus();
                    return;
                }

                // condetion
                else if (txtAdditionsType_ID.Text.Trim() == "")
                {
                    combAdditionsType.BackColor = Color.Pink;
                    combAdditionsType.Focus();
                    return;
                }
                // condetion
                else if (txtAddition_Value.Text.Trim() == "")
                {
                    txtAddition_Value.BackColor = Color.Pink;
                    txtAddition_Value.Focus();
                    return;
                }
                else if (Convert.ToDouble(txtAddition_Value.Text.Trim()) == 0)
                {
                    txtAddition_Value.BackColor = Color.Pink;
                    txtAddition_Value.Focus();
                    return;
                }


                cls.Update_Employee_Additions(txtAdditions_ID.Text, D1.Value, txtEmployeeID.Text, txtAdditionsType_ID.Text, txtAddition_Value.Text, txtRemarks.Text);

                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    cls.Delete_Employee_Additions(txtAdditions_ID.Text);
                    btnNew_Click(null, null);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {


            try
            {
                Employee_Additions_Search_frm frm = new Employee_Additions_Search_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    DataTable Dt = cls.Details_Employee_Additions(frm.DGV1.CurrentRow.Cells["Additions_ID"].Value.ToString());
                    txtAdditions_ID.Text = Dt.Rows[0]["Additions_ID"].ToString();
                    txtEmployeeID.Text = Dt.Rows[0]["EmployeeID"].ToString();
                    combEmployee.Text = Dt.Rows[0]["EmployeeName"].ToString();
                    D1.Value = Convert.ToDateTime(Dt.Rows[0]["MyDate"]);
                    txtAddition_Value.Text = Dt.Rows[0]["Addition_Value"].ToString();
                    txtAdditionsType_ID.Text = Dt.Rows[0]["AdditionsType_ID"].ToString();
                    combAdditionsType.Text = Dt.Rows[0]["AdditionsType_Name"].ToString();
                    txtRemarks.Text = Dt.Rows[0]["Remarks"].ToString();
                    btnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                txtEmployeeID.Text = "";
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void Discound_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            try
            {
          txtAddition_Value.BackColor = Color.White;

            if (txtAddition_Value.Text == "0" || txtAddition_Value.Text == "")
            {
                txtArbic.Text = "";
            }
            else
            {
                txtArbic.Text = DataAccessLayer.horof.تحويل_الأرقام_الى_حروف(Convert.ToDecimal(txtAddition_Value.Text), 3,"", "", true, true);
            }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
  
        }


        private void combEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combEmployee.BackColor = Color.White;
                txtEmployeeID.Text = combEmployee.SelectedValue.ToString();
            }
            catch
            {
                txtEmployeeID.Text = "";
                return;
            }
        }

        private void combEmployee_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (combEmployee.Text.Trim() == "")
                {
                    txtEmployeeID.Text = "";

                }
                else
                {
                    combEmployee.BackColor = Color.White;
                    txtEmployeeID.Text = combEmployee.SelectedValue.ToString();
                }

            }
            catch
            {
                txtEmployeeID.Text = "";
                return;
            }
        }

        private void combAdditionsType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combAdditionsType.BackColor = Color.White;
                txtAdditionsType_ID.Text = combAdditionsType.SelectedValue.ToString();
            }
            catch
            {
                txtAdditionsType_ID.Text = "";
                return;
            }
        }

        private void combAdditionsType_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (combAdditionsType.Text.Trim() == "")
                {
                    txtAdditionsType_ID.Text = "";

                }
                else
                {
                    combAdditionsType.BackColor = Color.White;
                    txtAdditionsType_ID.Text = combAdditionsType.SelectedValue.ToString();
                }

            }
            catch
            {
                txtAdditionsType_ID.Text = "";
                return;
            }
        }




    }
}
