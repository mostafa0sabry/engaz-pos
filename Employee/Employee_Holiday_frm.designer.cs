﻿namespace ByStro.PL
{
    partial class Employee_Holiday_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Employee_Holiday_frm));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnGetData = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAddition_Value = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtEmployeeID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAdditions_ID = new System.Windows.Forms.TextBox();
            this.combEmployee = new System.Windows.Forms.ComboBox();
            this.combAdditionsType = new System.Windows.Forms.ComboBox();
            this.txtAdditionsType_ID = new System.Windows.Forms.TextBox();
            this.D_MyDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.RbSelectAll = new System.Windows.Forms.RadioButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblMassege = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(527, 217);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(96, 30);
            this.btnClose.TabIndex = 66;
            this.btnClose.Text = "اغلاق";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGetData
            // 
            this.btnGetData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnGetData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGetData.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnGetData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetData.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetData.ForeColor = System.Drawing.Color.White;
            this.btnGetData.Location = new System.Drawing.Point(527, 185);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(96, 30);
            this.btnGetData.TabIndex = 65;
            this.btnGetData.Text = "بحث";
            this.btnGetData.UseVisualStyleBackColor = false;
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Enabled = false;
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(527, 153);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(96, 30);
            this.btnDelete.TabIndex = 64;
            this.btnDelete.Text = "حذف";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(527, 121);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(96, 30);
            this.btnUpdate.TabIndex = 63;
            this.btnUpdate.Text = "تعديل";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(527, 89);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(96, 30);
            this.btnSave.TabIndex = 62;
            this.btnSave.Text = "حفظ";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNew.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNew.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.ForeColor = System.Drawing.Color.White;
            this.btnNew.Location = new System.Drawing.Point(527, 57);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(96, 30);
            this.btnNew.TabIndex = 61;
            this.btnNew.Text = "جديد";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label7.Location = new System.Drawing.Point(43, 70);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 17);
            this.label7.TabIndex = 76;
            this.label7.Text = "اسم الموظف :";
            // 
            // txtAddition_Value
            // 
            this.txtAddition_Value.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtAddition_Value.Location = new System.Drawing.Point(43, 190);
            this.txtAddition_Value.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtAddition_Value.Name = "txtAddition_Value";
            this.txtAddition_Value.Size = new System.Drawing.Size(451, 24);
            this.txtAddition_Value.TabIndex = 79;
            this.txtAddition_Value.TextChanged += new System.EventHandler(this.txtValue_TextChanged);
            this.txtAddition_Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label5.Location = new System.Drawing.Point(43, 169);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 17);
            this.label5.TabIndex = 82;
            this.label5.Text = "عدد أيام الاجازة :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label9.Location = new System.Drawing.Point(43, 215);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 17);
            this.label9.TabIndex = 88;
            this.label9.Text = "ملاحظات :";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtRemarks.Location = new System.Drawing.Point(43, 236);
            this.txtRemarks.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(451, 53);
            this.txtRemarks.TabIndex = 87;
            // 
            // txtEmployeeID
            // 
            this.txtEmployeeID.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtEmployeeID.Location = new System.Drawing.Point(589, 12);
            this.txtEmployeeID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtEmployeeID.Name = "txtEmployeeID";
            this.txtEmployeeID.ReadOnly = true;
            this.txtEmployeeID.Size = new System.Drawing.Size(10, 24);
            this.txtEmployeeID.TabIndex = 90;
            this.txtEmployeeID.Visible = false;
            this.txtEmployeeID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label2.Location = new System.Drawing.Point(43, 119);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 92;
            this.label2.Text = "نوع الاجازة :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label1.Location = new System.Drawing.Point(225, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 98;
            this.label1.Text = "رقم الاجازة :";
            // 
            // txtAdditions_ID
            // 
            this.txtAdditions_ID.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtAdditions_ID.Location = new System.Drawing.Point(223, 43);
            this.txtAdditions_ID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtAdditions_ID.Name = "txtAdditions_ID";
            this.txtAdditions_ID.ReadOnly = true;
            this.txtAdditions_ID.Size = new System.Drawing.Size(271, 24);
            this.txtAdditions_ID.TabIndex = 97;
            // 
            // combEmployee
            // 
            this.combEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combEmployee.FormattingEnabled = true;
            this.combEmployee.Location = new System.Drawing.Point(43, 93);
            this.combEmployee.Name = "combEmployee";
            this.combEmployee.Size = new System.Drawing.Size(451, 24);
            this.combEmployee.TabIndex = 106;
            this.combEmployee.SelectedIndexChanged += new System.EventHandler(this.combEmployee_SelectedIndexChanged);
            this.combEmployee.TextChanged += new System.EventHandler(this.combEmployee_TextChanged);
            // 
            // combAdditionsType
            // 
            this.combAdditionsType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combAdditionsType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combAdditionsType.FormattingEnabled = true;
            this.combAdditionsType.Location = new System.Drawing.Point(43, 141);
            this.combAdditionsType.Name = "combAdditionsType";
            this.combAdditionsType.Size = new System.Drawing.Size(451, 24);
            this.combAdditionsType.TabIndex = 105;
            this.combAdditionsType.SelectedIndexChanged += new System.EventHandler(this.combAdditionsType_SelectedIndexChanged);
            this.combAdditionsType.TextChanged += new System.EventHandler(this.combAdditionsType_TextChanged);
            // 
            // txtAdditionsType_ID
            // 
            this.txtAdditionsType_ID.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtAdditionsType_ID.Location = new System.Drawing.Point(575, 12);
            this.txtAdditionsType_ID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtAdditionsType_ID.Name = "txtAdditionsType_ID";
            this.txtAdditionsType_ID.ReadOnly = true;
            this.txtAdditionsType_ID.Size = new System.Drawing.Size(10, 24);
            this.txtAdditionsType_ID.TabIndex = 107;
            this.txtAdditionsType_ID.Visible = false;
            // 
            // D_MyDate
            // 
            this.D_MyDate.CustomFormat = "";
            this.D_MyDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.D_MyDate.Location = new System.Drawing.Point(43, 43);
            this.D_MyDate.Name = "D_MyDate";
            this.D_MyDate.Size = new System.Drawing.Size(175, 24);
            this.D_MyDate.TabIndex = 108;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label3.Location = new System.Drawing.Point(43, 23);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 17);
            this.label3.TabIndex = 109;
            this.label3.Text = "التاريخ :";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(228, 70);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(99, 21);
            this.radioButton1.TabIndex = 111;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "تحديد موظف";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // RbSelectAll
            // 
            this.RbSelectAll.AutoSize = true;
            this.RbSelectAll.Location = new System.Drawing.Point(333, 70);
            this.RbSelectAll.Name = "RbSelectAll";
            this.RbSelectAll.Size = new System.Drawing.Size(88, 21);
            this.RbSelectAll.TabIndex = 112;
            this.RbSelectAll.Text = "تحديد الكل";
            this.RbSelectAll.UseVisualStyleBackColor = true;
            this.RbSelectAll.CheckedChanged += new System.EventHandler(this.RbSelectAll_CheckedChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblMassege});
            this.statusStrip1.Location = new System.Drawing.Point(0, 294);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(650, 22);
            this.statusStrip1.TabIndex = 113;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblMassege
            // 
            this.lblMassege.Name = "lblMassege";
            this.lblMassege.Size = new System.Drawing.Size(84, 17);
            this.lblMassege.Text = "يرجي الانتظار ...";
            // 
            // Employee_Holiday_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(650, 316);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.RbSelectAll);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.D_MyDate);
            this.Controls.Add(this.txtAdditionsType_ID);
            this.Controls.Add(this.combEmployee);
            this.Controls.Add(this.combAdditionsType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAdditions_ID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtEmployeeID);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtRemarks);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAddition_Value);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnGetData);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnNew);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "Employee_Holiday_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تسجيل اجازة";
            this.Load += new System.EventHandler(this.Discound_frm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Discound_frm_KeyDown);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnGetData;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtAddition_Value;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtRemarks;
        public System.Windows.Forms.TextBox txtEmployeeID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtAdditions_ID;
        private System.Windows.Forms.ComboBox combEmployee;
        private System.Windows.Forms.ComboBox combAdditionsType;
        public System.Windows.Forms.TextBox txtAdditionsType_ID;
        private System.Windows.Forms.DateTimePicker D_MyDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton RbSelectAll;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblMassege;
    }
}