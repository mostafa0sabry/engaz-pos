﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Employee_AdditionsType_frm : Form
    {
        public Employee_AdditionsType_frm()
        {
            InitializeComponent();
        }
        Employee_AdditionsType_cls cls  = new Employee_AdditionsType_cls();
   

        private void Discound_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null,null);
        }

  

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
              txtAdditionsType_ID.Text=  cls.MaxID_Employee_AdditionsType();
              txtAdditionsType_Name.Text = "";
              txtRemarks.Text = "";
               
              btnSave.Enabled = true;
              btnUpdate.Enabled = false;
              btnDelete.Enabled = false;
                txtAdditionsType_Name.Focus();

                if (Application.OpenForms["Employee_Additions_frm"] != null)
                {
                   ((Employee_Additions_frm) Application.OpenForms["Employee_Additions_frm"]).LoadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // condetion
                if (txtAdditionsType_Name.Text.Trim()=="")
                {
                    txtAdditionsType_Name.BackColor = Color.Pink;
                    txtAdditionsType_Name.Focus();
                    return;
                }

        
                txtAdditionsType_ID.Text = cls.MaxID_Employee_AdditionsType();
               cls.Insert_Employee_AdditionsType(txtAdditionsType_ID.Text,txtAdditionsType_Name.Text,txtRemarks.Text,Properties.Settings.Default.UserID);
                btnNew_Click(null,null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // condetion
                if (txtAdditionsType_Name.Text.Trim() == "")
                {
                    txtAdditionsType_Name.BackColor = Color.Pink;
                    txtAdditionsType_Name.Focus();
                    return;
                }
               
             
            

               cls.Update_Employee_AdditionsType(txtAdditionsType_ID.Text, txtAdditionsType_Name.Text, txtRemarks.Text);
               // Mass.Saved();
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (cls.NODelete_Employee_AdditionsType(txtAdditionsType_ID.Text).Rows.Count > 0)
                {
                    Mass.NoDelete();
                    return;
                }
                if (Mass.Delete()==true)
                {
                    cls.Delete_Employee_AdditionsType(txtAdditionsType_ID.Text);
                    btnNew_Click(null, null);      
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
       

            try
            {
                Employee_AdditionsType_Search_frm frm = new Employee_AdditionsType_Search_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    DataTable Dt = cls.Details_Employee_AdditionsType(frm.DGV1.CurrentRow.Cells["AdditionsType_ID"].Value.ToString());
                    txtAdditionsType_ID.Text = Dt.Rows[0]["AdditionsType_ID"].ToString();
                    txtAdditionsType_Name.Text = Dt.Rows[0]["AdditionsType_Name"].ToString();
                    txtRemarks.Text = Dt.Rows[0]["Remarks"].ToString();
                    btnSave.Enabled = false;
                    btnUpdate.Enabled =true ;
                    btnDelete.Enabled = true;
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void Discound_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

  

        private void txtAdditionsType_Name_TextChanged(object sender, EventArgs e)
        {
            txtAdditionsType_Name.BackColor = Color.White;

        }

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAdditionsType_Name_TextChanged_1(object sender, EventArgs e)
        {
            txtAdditionsType_Name.BackColor = Color.White;
        }
    }
}
