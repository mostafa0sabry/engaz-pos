﻿using ByStro.RPT;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class BillMaintenance_Search_frm : Form
    {
        public BillMaintenance_Search_frm()
        {
            InitializeComponent();
        }
        BillMaintenance_cls cls = new BillMaintenance_cls();
        public Boolean LoadData = false;
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                DataTable dt = new DataTable();
                if (DataAccessLayer.TypeUser == true)
                {
                    dt = cls.Search_Maintenance_Main(D1.Value, D2.Value, txtSearch.Text);
                }
                else
                {
                    dt = cls.Search_Maintenance_Main(D1.Value, D2.Value, Properties.Settings.Default.UserID, txtSearch.Text);
                }


                DGV2.AutoGenerateColumns = false;
                DGV2.DataSource = dt;
                sumbills();
            }
            catch
            {
                return;
            }

        }

        private void SalesReturn_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            ERB_Setting.SettingDGV(DGV2);


            LoadBill();
            ERB_Setting.SettingDGV(DGV1);


        }





        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            LoadBill();
        }


        private void LoadBill()
        {
            try
            {

                DataTable dt = new DataTable();
                if (DataAccessLayer.TypeUser == true)
                {
                    dt = cls.Load_Maintenance_Main(D1.Value, D2.Value);
                }
                else
                {
                    dt = cls.Load_Maintenance_Main(D1.Value, D2.Value, Properties.Settings.Default.UserID);
                }

                DGV2.AutoGenerateColumns = false;
                DGV2.DataSource = dt;
                sumbills();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            LoadBill();
        }


        public void sumbills()
        {


            try
            {


                Decimal NetInvoice = 0;
                Decimal paid_Invoice = 0;
                Decimal RestInvoise = 0;
                lblCount.Text = "0";

                for (int i = 0; i < DGV2.Rows.Count; i++)
                {
                    NetInvoice += Convert.ToDecimal(DGV2.Rows[i].Cells["NetInvoice"].Value);
                    paid_Invoice += Convert.ToDecimal(DGV2.Rows[i].Cells["PaidInvoice"].Value);
                    RestInvoise += Convert.ToDecimal(DGV2.Rows[i].Cells["RestInvoise"].Value);
                }
                lblSum.Text = NetInvoice.ToString();
                lblPay.Text = paid_Invoice.ToString();
                lblRest.Text = RestInvoise.ToString();
                lblCount.Text = DGV2.Rows.Count.ToString(); ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }




        }



        private void DGV2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGV2.Rows.Count == 0)
                {
                    return;
                }
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                DataTable dt = cls.Details_Maintenance_Details(DGV2.CurrentRow.Cells["MainID"].Value.ToString());
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        public Boolean ReturnInvoice = false;
        private void btnView_Click(object sender, EventArgs e)
        {

            if (DGV2.Rows.Count == 0)
            {
                return;
            }
           
            LoadData = true;




            Close();
        }

        private void BillBay_Search_frm_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.WindowState == FormWindowState.Maximized)
                {
                    DGV2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                }
                else
                {
                    DGV2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                    DGV2.Columns[0].Width = 150;
                    DGV2.Columns[1].Width = 150;
                    DGV2.Columns[2].Width = 250;
                    DGV2.Columns[3].Width = 150;
                    DGV2.Columns[4].Width = 150;
                    DGV2.Columns[5].Width = 100;
                    DGV2.Columns[6].Width = 150;
                    DGV2.Columns[7].Width = 100;
                    DGV2.Columns[8].Width = 150;
                    DGV2.Columns[9].Width = 150;
                    DGV2.Columns[10].Width = 300;
                    DGV2.Columns[11].Width = 200;
                }

                panel3.Height = this.Height / 2;
            }
            catch
            {

            }

        }

      
        //private void PrintBill()
        //{


        //    try
        //    {
        //       
        //        if (BillSetting_cls.PrintSize == 1)
        //        {
        //            PrintSizeA4();
        //        }
        //        else if (BillSetting_cls.PrintSize == 2)
        //        {
        //            PrintSize1_2A4();   //Task.Run(() => );
        //        }
        //        else
        //        {
        //            PrintSize80MM();

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }

        //}






        //private void PrintSizeA4()
        //{
        //    //Invoke((MethodInvoker)delegate
        //    //     {
        //    DataTable dt_cus = TreasuryMovement_CLS.Balence_ISCusSupp(DGV2.CurrentRow.Cells["CustomerID"].Value.ToString(), "Cus");
        //    BillMaintenance_cls cls_print = new BillMaintenance_cls();
        //    DataTable dt_Print = cls_print.Details_Maintenance_Details(DGV2.CurrentRow.Cells["MainID"].Value.ToString());

        //    Reportes_Form frm = new Reportes_Form();

        //    var report = new CRBill_Maintenance_A4();
        //    report.Database.Tables["Bill_Maintenance"].SetDataSource(dt_Print);
        //    report.SetParameterValue(0, DGV2.CurrentRow.Cells["CustomerName"].Value.ToString());
        //    report.SetParameterValue(1, DGV2.CurrentRow.Cells["Phone"].Value.ToString());
        //    report.SetParameterValue(2, DGV2.CurrentRow.Cells["MainID"].Value.ToString());
        //    report.SetParameterValue(3, DGV2.CurrentRow.Cells["TypeKind"].Value.ToString());
        //    report.SetParameterValue(4, DGV2.CurrentRow.Cells["Remarks"].Value.ToString());
        //    report.SetParameterValue(5, DGV2.CurrentRow.Cells["TotalInvoice"].Value.ToString());
        //    report.SetParameterValue(6, DGV2.CurrentRow.Cells["Discount"].Value.ToString());
        //    report.SetParameterValue(7, DGV2.CurrentRow.Cells["NetInvoice"].Value.ToString());
        //    report.SetParameterValue(8, DGV2.CurrentRow.Cells["PaidInvoice"].Value.ToString());
        //    report.SetParameterValue(9, DGV2.CurrentRow.Cells["RestInvoise"].Value.ToString());
        //    report.SetParameterValue(10, dt_cus.Rows[0]["balence"].ToString());
        //    report.SetParameterValue(11, D1.Value);
        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();
        //    #region"Crystal Report | Printing | Default Printer"
        //    var dialog = new PrintDialog();
        //    report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    report.PrintToPrinter(1, false, 0, 0);

        //    //});



        //    #endregion
        //    // });
        //}

        //private void PrintSize1_2A4()
        //{
        //    //Invoke((MethodInvoker)delegate
        //    //   {
        //    DataTable dt_cus = TreasuryMovement_CLS.Balence_ISCusSupp(DGV2.CurrentRow.Cells["CustomerID"].Value.ToString(), "Cus");//Cus_Trans_cls.Cus_Balence(DGV2.CurrentRow.Cells["CustomerID"].Value.ToString());
        //    BillMaintenance_cls cls_print = new BillMaintenance_cls();
        //    DataTable dt_Print = cls_print.Details_Maintenance_Details(DGV2.CurrentRow.Cells["MainID"].Value.ToString());
        //    Reportes_Form frm = new Reportes_Form();
        //    var report = new CRBill_Maintenance();
        //    report.Database.Tables["Bill_Maintenance"].SetDataSource(dt_Print);
        //    report.SetParameterValue(0, DGV2.CurrentRow.Cells["CustomerName"].Value.ToString());
        //    report.SetParameterValue(1, DGV2.CurrentRow.Cells["Phone"].Value.ToString());
        //    report.SetParameterValue(2, DGV2.CurrentRow.Cells["MainID"].Value.ToString());
        //    report.SetParameterValue(3, DGV2.CurrentRow.Cells["TypeKind"].Value.ToString());
        //    report.SetParameterValue(4, DGV2.CurrentRow.Cells["Remarks"].Value.ToString());
        //    report.SetParameterValue(5, DGV2.CurrentRow.Cells["TotalInvoice"].Value.ToString());
        //    report.SetParameterValue(6, DGV2.CurrentRow.Cells["Discount"].Value.ToString());
        //    report.SetParameterValue(7, DGV2.CurrentRow.Cells["NetInvoice"].Value.ToString());
        //    report.SetParameterValue(8, DGV2.CurrentRow.Cells["PaidInvoice"].Value.ToString());
        //    report.SetParameterValue(9, DGV2.CurrentRow.Cells["RestInvoise"].Value.ToString());
        //    report.SetParameterValue(10, dt_cus.Rows[0]["balence"].ToString());
        //    report.SetParameterValue(11, D1.Value);
        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();
        //    #region"Crystal Report | Printing | Default Printer"
        //    var dialog = new PrintDialog();
        //    report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    report.PrintToPrinter(1, false, 0, 0);
        //    #endregion

        //    //frm.ShowDialog();
        //    // });
        //}
        //TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();
        //private void PrintSize80MM()
        //{
        //    //Invoke((MethodInvoker)delegate
        //    //   {
        //    DataTable dt_cus = TreasuryMovement_CLS.Balence_ISCusSupp(DGV2.CurrentRow.Cells["CustomerID"].Value.ToString(), "Cus"); //Cus_Trans_cls.Cus_Balence(DGV2.CurrentRow.Cells["CustomerID"].Value.ToString());
        //    BillMaintenance_cls cls_print = new BillMaintenance_cls();
        //    DataTable dt_Print = cls_print.Details_Maintenance_Details(DGV2.CurrentRow.Cells["MainID"].Value.ToString());
        //    Reportes_Form frm = new Reportes_Form();
        //    var report = new CRBill_Maintenance80MM();
        //    report.Database.Tables["Bill_Maintenance"].SetDataSource(dt_Print);
        //    report.SetParameterValue(0, DGV2.CurrentRow.Cells["CustomerName"].Value.ToString());
        //    report.SetParameterValue(1, DGV2.CurrentRow.Cells["Phone"].Value.ToString());
        //    report.SetParameterValue(2, DGV2.CurrentRow.Cells["MainID"].Value.ToString());
        //    report.SetParameterValue(3, DGV2.CurrentRow.Cells["TypeKind"].Value.ToString());
        //    report.SetParameterValue(4, DGV2.CurrentRow.Cells["Remarks"].Value.ToString());
        //    report.SetParameterValue(5, DGV2.CurrentRow.Cells["TotalInvoice"].Value.ToString());
        //    report.SetParameterValue(6, DGV2.CurrentRow.Cells["Discount"].Value.ToString());
        //    report.SetParameterValue(7, DGV2.CurrentRow.Cells["NetInvoice"].Value.ToString());
        //    report.SetParameterValue(8, DGV2.CurrentRow.Cells["PaidInvoice"].Value.ToString());
        //    report.SetParameterValue(9, DGV2.CurrentRow.Cells["RestInvoise"].Value.ToString());
        //    report.SetParameterValue(10, dt_cus.Rows[0]["balence"].ToString());
        //    report.SetParameterValue(11, D1.Value);
        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();
        //    #region"Crystal Report | Printing | Default Printer"
        //    var dialog = new PrintDialog();
        //    report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    report.PrintToPrinter(1, false, 0, 0);
        //    #endregion

        //    //frm.ShowDialog();
        //    // });
        //}

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
           // PrintBill();
        }









    }
}