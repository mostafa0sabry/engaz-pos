﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class ReportMaintenance_frm : Form
    {
        public ReportMaintenance_frm()
        {
            InitializeComponent();
        }
        Maintenance_cls cls = new Maintenance_cls();
        public Boolean LoadData = false;
        private void CustomersPayment_FormAR_Load(object sender, EventArgs e)
        {

            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);

                UserPermissions_CLS Store_cls = new UserPermissions_CLS();
                combUser.DataSource = Store_cls.Search_UserPermissions();
                combUser.DisplayMember = "EmpName";
                combUser.ValueMember = "ID";
                combUser.Text = null;
                combUser.SelectedIndex = -1;

            }
            catch
            {

            }

        }




        private void datefrom_ValueChanged(object sender, EventArgs e)
        {

        }

        private void datetto_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();

                int U = combUser.SelectedIndex;
                int D = combDeviceStatus.SelectedIndex;

                if (U >= 0 && D < 0) // SELECT U
                {
                    dt = cls.Search_MaintenanceUser("", datefrom.Value, datetto.Value, combUser.SelectedValue.ToString());
                }
                else if (U < 0 && D >= 0) // SELECT D
                {
                    dt = cls.Search_Maintenance("", datefrom.Value, datetto.Value, D.ToString());
                }
                else if (U >= 0 && D >= 0) // seletct 2 comb
                {
                    dt = cls.Search_MaintenanceUser("", datefrom.Value, datetto.Value, D.ToString(), combUser.SelectedValue.ToString());
                }
                else // NO SELECT
                {
                    dt = cls.Search_Maintenance("", datefrom.Value, datetto.Value);
                }
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void combDeviceStatus_TextChanged(object sender, EventArgs e)
        {

        }

        private void combDeviceStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void combDeviceStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
