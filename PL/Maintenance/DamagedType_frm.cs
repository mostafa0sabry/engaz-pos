﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class DamagedType_frm : Form
    {
        public DamagedType_frm()
        {
            InitializeComponent();
        }
        DamagedType_cls cls = new DamagedType_cls();
        public Boolean LoadData = false;
        private void AccountEndAdd_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);

        }


        private void AccountEndAdd_Form_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
          
        }

        private void txtAccountName_TextChanged(object sender, EventArgs e)
        {
            txtDeviceTypeName.BackColor = Color.White;
        }

        private void txtCurrencyVal_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

  

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //"Conditional"
                if (txtDeviceTypeName.Text.Trim() == "")
                {
                    txtDeviceTypeName.BackColor = Color.Pink;
                    txtDeviceTypeName.Focus();
                    return;
                }

                // " Search TextBox "
                DataTable DtSearch = cls.NameSearch__DamagedType(txtDeviceTypeName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDeviceTypeName.BackColor = Color.Pink;
                    txtDeviceTypeName.Focus();
                    return;
                }

                txtDeviceTypeID.Text = cls.MaxID_DamagedType();
                cls.Insert_DamagedType(txtDeviceTypeID.Text, txtDeviceTypeName.Text.Trim(),txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // "Conditional"
                if (txtDeviceTypeName.Text.Trim() == "")
                {
                    txtDeviceTypeName.BackColor = Color.Pink;
                    txtDeviceTypeName.Focus();
                    return;
                }

                //"Conditional"
             


                cls.Update_DamagedType(txtDeviceTypeID.Text, txtDeviceTypeName.Text.Trim(), txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                                // fill combBox combParent
            
                //=============================================================================================

                txtDeviceTypeID.Text = cls.MaxID_DamagedType();
                txtDeviceTypeName.Text = "";
                txtRemark.Text ="";
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                if (Application.OpenForms["MaintenanceRecived_frm"]!=null)
                {
                    ((MaintenanceRecived_frm)Application.OpenForms["MaintenanceRecived_frm"]).FillDamagedType();
                }
                txtDeviceTypeName.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {





                #region " Search TextBox "



                //DataTable DtItem = cls.Search_Category_Delete( txtDeviceTypeID.Text);
                //if (DtItem.Rows.Count > 0)
                //{
                //    MessageBox.Show("لا يمكنك حذف مجموعة يوجد بداخلها اصناف ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    return;
                //}

                #endregion

                if (Mass.Delete() == true)
                {
                    cls.Delete_DamagedType( txtDeviceTypeID.Text);
                    btnNew_Click(null, null); 
                    LoadData = true;
                }  
             
            
            



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
           

            try
            {
                DamagedTypeSearch_frm frm = new DamagedTypeSearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata==false)
                {
                    return;
                }

                DataTable dt = cls.Details_DamagedType( frm.DGV1.CurrentRow.Cells["DamagedTypeID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtDeviceTypeID.Text = Dr["DamagedTypeID"].ToString();
                txtDeviceTypeName.Text = Dr["DamagedTypeName"].ToString();

                txtRemark.Text =Dr["Remark"].ToString();
     
                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

     

      



  

       












    }
}
