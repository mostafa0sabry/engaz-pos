﻿using System;
using System.Data;

class DeviceType_cls : DataAccessLayer
    {
    //MaxID
    public String MaxID_DeviceType()
    {
        return Execute_SQL("select ISNULL (MAX(DeviceTypeID)+1,1) from DeviceType", CommandType.Text);
    }

    // Insert
    public void Insert_DeviceType(string DeviceTypeID, String DeviceTypeName, string CompenyID, string Remark)
    {
        Execute_SQL("insert into DeviceType(DeviceTypeID ,DeviceTypeName ,CompenyID ,Remark )Values (@DeviceTypeID ,@DeviceTypeName ,@CompenyID ,@Remark )", CommandType.Text,
        Parameter("@DeviceTypeID", SqlDbType.NVarChar, DeviceTypeID),
        Parameter("@DeviceTypeName", SqlDbType.NVarChar, DeviceTypeName),
        Parameter("@CompenyID", SqlDbType.NVarChar, CompenyID),
        Parameter("@Remark", SqlDbType.NText, Remark));
    }

    //Update
    public void Update_DeviceType(string DeviceTypeID, String DeviceTypeName, string CompenyID, string Remark)
    {
        Execute_SQL("Update DeviceType Set DeviceTypeID=@DeviceTypeID ,DeviceTypeName=@DeviceTypeName ,CompenyID=@CompenyID ,Remark=@Remark  where DeviceTypeID=@DeviceTypeID", CommandType.Text,
        Parameter("@DeviceTypeID", SqlDbType.NVarChar, DeviceTypeID),
        Parameter("@DeviceTypeName", SqlDbType.NVarChar, DeviceTypeName),
        Parameter("@CompenyID", SqlDbType.NVarChar, CompenyID),
        Parameter("@Remark", SqlDbType.NText, Remark));
    }


    //Delete
    public void Delete_DeviceType(string DeviceTypeID)
    {
        Execute_SQL("Delete  From DeviceType where DeviceTypeID=@DeviceTypeID", CommandType.Text,
        Parameter("@DeviceTypeID", SqlDbType.NVarChar, DeviceTypeID));
    }

    //Details ID
    public DataTable Details_DeviceType(string DeviceTypeID)
    {
        return ExecteRader(@"SELECT        dbo.DeviceType.*, dbo.MaintenanceCompeny.CompenyName
FROM            dbo.DeviceType INNER JOIN
                         dbo.MaintenanceCompeny ON dbo.DeviceType.CompenyID = dbo.MaintenanceCompeny.CompenyID Where DeviceTypeID=@DeviceTypeID", CommandType.Text,
        Parameter("@DeviceTypeID", SqlDbType.NVarChar, DeviceTypeID));
    }


    //Details ID
    public DataTable Details_DeviceTypeCompeny(string CompenyID)
    {
        return ExecteRader(@"SELECT * from DeviceType  DeviceType  Where CompenyID=@CompenyID", CommandType.Text,
        Parameter("@CompenyID", SqlDbType.NVarChar, CompenyID));
    }
    //Details ID
    public DataTable NameSearch__DeviceType(String DeviceTypeName)
    {
        return ExecteRader("Select *  from DeviceType Where DeviceTypeName=@DeviceTypeName", CommandType.Text,
        Parameter("@DeviceTypeName", SqlDbType.NVarChar, DeviceTypeName));
    }

    //Search 
    public DataTable Search__DeviceType(string Search)
    {
        return ExecteRader("Select *  from DeviceType Where convert(nvarchar,DeviceTypeID)+DeviceTypeName like '%'+@Search+ '%' ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }


}

