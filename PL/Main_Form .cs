﻿using ByStro.BL;
using ByStro.Clases;
using ByStro.Forms;
using ByStro.Properties;
using ByStro.UControle;
using DevExpress.LookAndFeel;
using DevExpress.XtraBars.Navigation;
using DevExpress.XtraEditors;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;


namespace ByStro.PL
{
    public partial class Main_frm :XtraForm 
    {
        List<Element> screens = new List<Element>();
        List<Type> ListFormsType = new List<Type>();
        Form formDB;
        UCCustomIconButton uCCustomIconButton;

        public static Main_frm Instance { get {

                return Application.OpenForms[nameof(Main_frm)] as Main_frm;
            }
        }
        public Main_frm()
        {
            #region تفعيل البرنامج
            DataAccessLayer.Finull_Sarial = BL.Final_key.GetHash(Properties.Settings.Default.computerID);
            #endregion
         
            InitializeComponent();
           
            screens = Clases.Screens.GetScreens.OrderBy(x=>x.Id).ToList();
            ListFormsType = Assembly.GetExecutingAssembly().GetTypes().ToList();
            formDB = new Form {Text="الرئيسية" }; 
            formDB.MdiParent = this;
            formDB.ControlBox = false;
            formDB.FormClosing += (s, e) => { e.Cancel = true; };
            uCCustomIconButton = new UCCustomIconButton
            {
                Dock = DockStyle.Fill,
                ElementColumnCount = 5,
                ElementOrientation = Orientation.Vertical,
                ElementItemSize = new Size(150,150)
            };
            uCCustomIconButton.ElementClick += UCCustomIconButton_ElementClick;
            uCCustomIconButton.ElementsDataSource = screens.Where(x => x.IsPanel).ToList();
            formDB.Controls.Add(uCCustomIconButton);
            formDB.Show();

            accordionControl.ElementClick += AccordionControl1_ElementClick;

            StatusCompanyName.Caption  = DataAccessLayer.Version;
            txt_User.Caption  = $"اسم المستخدم: {DataAccessLayer.UserNameLogIn}";
        }
        private void Main_Form_Load_1(object sender, EventArgs e)
        {
            accordionControl.Elements.Clear();          
            screens.Where(s => s.MasterId == 0).ToList().ForEach(s =>
            { 
                AddAccordionGroup(s); 
            });
            
           
            DevExpress.XtraEditors.WindowsFormsSettings.DefaultFont = Settings.Default.SystemFont;

             
            Active_CHec();

            #region MyRegion
            timer1.Start();

            
            if (Properties.Settings.Default.BukUPState == "3")
            {
                DataAccessLayer.paukupautomatic = "3";
                Baukup_Form frm = new Baukup_Form();
                frm.ControlBox = true;
                frm.ShowDialog();
            }

            NotifacationProdect();
            NotifacationCustomer();
            #endregion

            Program.CheckForDBUpdate();

        }
        #region Accordion
        void AddAccordionGroup(Element element,AccordionControlElement parent =null)
        {
            AccordionControlElement elm = new AccordionControlElement();
            elm.Text = element.Caption;
            elm.Tag = element.Name;
            elm.Name = element.Name;
            elm.ImageOptions.SvgImage =  element.SvgImage ;  
            elm.Style = ElementStyle.Group;
            
            if (parent==null)
                accordionControl.Elements.Add(elm);
            else
                parent.Elements.Add(elm);

            AddAccordionElement(elm, element.Id);
        }
        void AddAccordionElement(AccordionControlElement parent, int parentID)
        {

            foreach (var s in screens.Where(s => s.MasterId == parentID))
            {
                switch (s.Name)//اذا كان العنصور يتضمن اكثر من عنصر اخر
                {
                    case "Delivery":
                    case "Salesman":
                    case "Tables":
                        AddAccordionGroup(s, parent);
                        break;
                    default:
                        AccordionControlElement elm = new AccordionControlElement();
                        elm.Text = s.Caption;
                        elm.Tag = s.Name;
                        elm.Name = s.Name;
                        elm.ImageOptions.SvgImage = s.SvgImage;
                        elm.Style = ElementStyle.Item;
                        parent.Elements.Add(elm);
                        break;

                }
            }
        }
        private void AccordionControl1_ElementClick(object sender, DevExpress.XtraBars.Navigation.ElementClickEventArgs e)
        {
            var tag = e.Element.Tag as string;
            if (tag!=null&&tag != string.Empty)
            {
                ShowForm(tag, screens.SingleOrDefault(x => x.Name == tag).Caption);
            }
        }
        private void UCCustomIconButton_ElementClick(object sender, Element e)
        {
            if (e.Name != string.Empty)
                ShowForm(e.Name,e.Caption);
        }

        bool isBusy;
        public void ShowForm(string name,string Text="")
        {
            if (isBusy) return;
            isBusy = true;
            splashScreen.ShowWaitForm();
            Form frm = null;
            switch (name)
            {
                case "AddInvoiceRangeSalesInvoice":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_AddInvoiceRange(true);
                        frm.Name = name;
                    }
                    break;  
                case "AddInvoiceRangePurchaseInvoice":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_AddInvoiceRange(false);
                        frm.Name = name;
                    }
                    break;   
                case "frm_AddCustomerRange":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_AddVendorCustomerRange(true);
                        frm.Name = name;
                    }
                    break;  
                case "frm_AddVendorRange":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_AddVendorCustomerRange(false);
                        frm.Name = name;
                    }
                    break;
                case "uc_Administrative_Report":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new Form();
                        frm.Controls.Add(new uc_Administrative_Report() { Dock = DockStyle.Fill }) ;
                        frm.Name = name;
                    }
                    break;
                case "ItemOpenBalance":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.ItemOpenBalance);
                        frm.Name = name;
                    }
                    break;
                case "ItemDamage":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.ItemDamage);
                        frm.Name = name;
                    }
                    break;
                case "ItemStoreMove":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.ItemStoreMove);
                        frm.Name = name;
                    }
                    break;
                case "FontDialog":
                    ChangeFont();
                    return;
                case "SalesInvoice":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.SalesInvoice);
                        frm.Name = name;
                    }
                    break;
                case "SalesReturn":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.SalesReturn);
                        frm.Name = name;
                    }
                    break;

                case "frm_DeliveryOrderListGoing":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_DeliveryOrderList(false);
                        frm.Name = name;
                    }
                    break;
                case "frm_DeliveryOrderListEnd":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_DeliveryOrderList(true);
                        frm.Name = name;
                    }
                    break;


                case "PurchaseInvoice":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.PurchaseInvoice);
                        frm.Name = name;
                    }
                    break;
                case "PurchaseReturn":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.PurchaseReturn);
                        frm.Name = name;
                    }
                    break;
              

               

           
                
                case "ProductBalance":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_EditReportFilters(frm_Report.ReportType.ProductBalance);
                        frm.Name = name;
                    }
                    break;
                case "ProductMovment":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_EditReportFilters(frm_Report.ReportType.ProductMovment);
                        frm.Name = name;
                    }
                    break;
                case "ProductExpire":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_EditReportFilters(frm_Report.ReportType.ProductExpire);
                        frm.Name = name;
                    }
                    break;
                case "ProductReachedReorderLevel":
                    frm = IsFormActive(null, name);
                    if (frm == null)
                    {
                        frm = new frm_EditReportFilters(frm_Report.ReportType.ProductReachedReorderLevel);
                        frm.Name = name;
                    }
                    break;
                case "LogOutUser":
                    LogOutUser();
                    return;
                case "CloseApp":
                    CloseApp();
                    return;
                default:
                    var ins = ListFormsType.FirstOrDefault(x => x.Name == name);
                    if (ins != null)
                    {
                        frm = IsFormActive(ins);
                        if (frm == null)
                            frm = Activator.CreateInstance(ins) as Form;
                    }
                    break;

            }
            splashScreen.CloseWaitForm();
            isBusy = false;
            if (frm != null)
            {
                frm.Name = name;
                frm.Tag = name;
                frm.Text = Text == "" ? frm.Text : Text;
                var IsShowDialog = screens.First(x => x.Name == name).IsShowDialog;
                ShowForm(frm, IsShowDialog);
            }
        }
        public void ShowForm(Form frm ,bool  IsShowDialog = false )
        {
            if (!IsShowDialog) frm.MdiParent = this;
         
            if (IsShowDialog == false)
                frm.FormBorderStyle = FormBorderStyle.None;
            OpenForm(frm, IsShowDialog);
        }
        private Form IsFormActive(Type type, string Tag = null)
        {
            foreach (Form item in this.MdiChildren)
            {
                if (!string.IsNullOrEmpty(Tag) && item.Tag?.ToString() == Tag)
                {
                    item.Activate();
                    return item;
                }
                else if (item.GetType() == type)
                {
                    item.Activate();
                    return item;
                }
            }
            return null;
        }
        public static void OpenForm(Form frm, bool IsShowDialog = false)
        {
            //Cheak Access Open Form
            if (true)
            {
                if (IsShowDialog)
                {
                    frm.StartPosition = FormStartPosition.CenterScreen;
                    frm.ShowDialog();
                }
                else
                    frm.Show();
            }
            //else
            //    XtraMessageBox.Show("غير مصرح لك");
        }
        #endregion

        public void LogOutUser()
        {
            DataAccessLayer.closeMainform = true;
            this.Close();

            if (Application.OpenForms["LoginUser_Form"] != null)
            {
                ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).Show();
                if (Properties.Settings.Default.RemmberPassword == false)
                {
                    ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).txtUserName.Text = "";
                    ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).txtPassword.Text = "";
                }

                ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).txtUserName.Focus();
                DataAccessLayer.closeMainform = false;
            }
        }
        public void CloseApp()
        {
            if (Properties.Settings.Default.BukUPState == "2")
            {
                DataAccessLayer.paukupautomatic = "OK";
                Baukup_Form frm = new Baukup_Form();
                frm.ShowDialog();
            }
            else
            {
                DataAccessLayer.closeMainform = true;
                Application.Exit();
            }
        }

        Double countSales = 0;
        private void Active_CHec()
        {
            #region"تفعيل البرنامج "

            try
            {
                Trail_cls sales_CLS = new Trail_cls();
                countSales = Convert.ToDouble(sales_CLS.COUNT_Sales_Main());
            }
            catch
            {

                countSales = 0;
            }

            try
            {
                if (Properties.Settings.Default.Finull_Sarial != DataAccessLayer.Finull_Sarial)
                {

                    if (countSales >= Final_key.SalesNumber)
                    {

                        SerilFull_FORM FrmAc = new SerilFull_FORM();
                        DataAccessLayer.closeMainform = true;
                        this.Close();
                        FrmAc.Show();
                    }
                    else
                    {
                        Properties.Settings.Default.LicenseType = "Trail";
                        Properties.Settings.Default.Finull_Sarial = "1E01-192B-FBD3-75EE-A005-9910-E120-6A30";
                        this.Text = DataAccessLayer.Version + " Trail Version";
                        Properties.Settings.Default.Save();
                    }
                }
                else
                {
                    RegistryKey regkey = default(RegistryKey);
                    regkey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\ByStor", true);
                    string code = "";
                    code = regkey.GetValue("System_Activation").ToString();
                    if (DataAccessLayer.DecryptData(code) == DataAccessLayer.Finull_Sarial)
                    {
                        Properties.Settings.Default.LicenseType = "Full";
                        this.Text = DataAccessLayer.Version + " Full Version";
                        Properties.Settings.Default.Save();
                    }
                    else
                    {
                        if (countSales >= Final_key.SalesNumber)
                        {
                            SerilFull_FORM FrmAc = new SerilFull_FORM();
                            DataAccessLayer.closeMainform = true;
                            this.Close();
                            FrmAc.Show();
                        }

                    }

                }


            }
            catch
            {
                Properties.Settings.Default.LicenseType = "Trail";
                Properties.Settings.Default.Finull_Sarial = "1E01-192B-FBD3-75EE-A005-9910-E120-6A30";
                this.Text = DataAccessLayer.Version + " Trail Version";
                Properties.Settings.Default.Save();
            }
            #endregion

        }
        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("سوف يتم الخروج من البرنامج : هل تريد الاستمرار", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {

                if (Properties.Settings.Default.BukUPState == "2")
                {
                    DataAccessLayer.paukupautomatic = "OK";
                    Baukup_Form frm = new Baukup_Form();
                    frm.ShowDialog();
                }
                else
                {
                    DataAccessLayer.closeMainform = true;
                    Application.Exit();
                }

            }



        }
         
        private void Main_Form_FormClosing(object sender, FormClosingEventArgs e)
        {

 
            Settings.Default["ApplicationSkinName"] = UserLookAndFeel.Default.SkinName;
            Settings.Default.Save();

            if (Properties.Settings.Default.LastBackUpTime.Year < 1950 || ((Properties.Settings.Default.LastBackUpTime - DateTime.Now).TotalDays > 3))
            {


                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(Properties.Settings.Default.Connection_String);
                if (builder.IntegratedSecurity)
                {
                    if (XtraMessageBox.Show("هل تريد اخذ نسخه احتياطيه ؟", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        new frm_BackupRestore().ShowDialog();
                    }
                }

            }

            try
            {
                Application.ExitThread();
            }
            catch
            {


            }



        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            { 
               StatusDate.Caption = $"الوقت الا:{DateTime.Now.ToString("HH:MM:ss")}";
                if (Properties.Settings.Default.BukUPState == "4")
                {
                    DateTime startTime = Convert.ToDateTime(Properties.Settings.Default.myDate);
                    DateTime endTime = DateTime.Now;
                    TimeSpan duration = DateTime.Parse(endTime.ToString()).Subtract(DateTime.Parse(startTime.ToString()));
                    //MessageBox.Show((duration.Hours).ToString());
                    int ddd = duration.Hours - Convert.ToInt32(Properties.Settings.Default.Hower);
                    if (ddd >= int.Parse(Properties.Settings.Default.BakupTime))
                    {
                        //timer1.Stop();

                        Properties.Settings.Default.Hower = (duration.Hours).ToString();
                        Properties.Settings.Default.Save();
                        DataAccessLayer.paukupautomatic = "3";
                        Baukup_Form frm = new Baukup_Form();
                        frm.ShowDialog();
                    }



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } 
        private void ChangeFont()
        {
            FontDialog fd = new FontDialog();
            fd.Font = Settings.Default.SystemFont;
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Properties.Settings.Default.SystemFont = fd.Font;
                Properties.Settings.Default.Save();
                DevExpress.XtraEditors.WindowsFormsSettings.DefaultFont = Settings.Default.SystemFont;

            }
        }

        
        Store_Prodecut_cls Store_Prodecut_cls = new Store_Prodecut_cls();
        TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();
        private void NotifacationProdect()
        {
            if (BillSetting_cls.NotificationProdect == false)
            {
                return;
            }
            try
            {

                DataTable dt = Store_Prodecut_cls.Store_Prodecut_MinimNotifcation();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Notifications_UC US = new Notifications_UC();
                    US.Icon = this.Icon;
                    US.pictureBox1.Image = Properties.Resources.Move_by_Trolley_48px;
                    US.Text = dt.Rows[i]["ProdecutName"].ToString();
                    US.label3.Text = " الرصيد : " + dt.Rows[i]["Balence"].ToString() + Environment.NewLine + " المخزن : " + dt.Rows[i]["StoreName"].ToString();

                    US.MdiParent = this;
                    US.Show();

                }
                // lblCount.Text = DGV1.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
        }
        private void NotifacationCustomer()
        {
            try
            {

                if (BillSetting_cls.NotificationCustomers == false)
                {
                    return;
                }

                Double BalencyCus = double.Parse(BillSetting_cls.MaxBalance);

                Customers_cls Customers_cls = new Customers_cls();
                DataTable dt_cus = Customers_cls.Search_Customers("");

                for (int i = 0; i < dt_cus.Rows.Count; i++)
                {
                    double balenceDebit = 0;
                    DataTable dt = TreasuryMovement_CLS.Balence_ISCusSupp(dt_cus.Rows[i]["CustomerID"].ToString(), "Cus");



                    balenceDebit = double.Parse(dt.Rows[0]["balence"].ToString());
                    if (BalencyCus <= balenceDebit)
                    {
                        Notifications_UC US = new Notifications_UC();
                        US.Icon = this.Icon;

                        US.pictureBox1.Image = Properties.Resources.customerss;
                        US.Text = dt_cus.Rows[i]["CustomerName"].ToString();
                        US.label3.Text = " الرصيد : " + balenceDebit.ToString() + Environment.NewLine + " رقم الهاتف : " + dt_cus.Rows[i]["Phone"].ToString();

                        US.MdiParent = this;
                        US.Show();
                    }



                    //  AddRowDgv(dt_cus.Rows[i]["CustomerID"].ToString(), dt_cus.Rows[i][""].ToString(), dt_cus.Rows[i]["Address"].ToString(), dt_cus.Rows[i]["Phone"].ToString(), dt.Rows[0]["Debit"].ToString(), dt.Rows[0]["Credit"].ToString(), balenceDebit, balenceCredit);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ChangeFont();
        }
    }
    
}
