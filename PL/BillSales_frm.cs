﻿using ByStro.DAL;
using ByStro.Forms; 
using System;
using System.Data;
using System.Drawing; 
using System.Windows.Forms; 
using System.Linq; 
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors; 
using DevExpress.XtraGrid.Views.Grid; 
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors.Controls; 
using ByStro.Clases;
using System.Collections.Generic;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using System.Linq.Expressions;
using System.Collections.ObjectModel;

namespace ByStro.PL
{
    public partial class BillSales_frm : Form
    {
        public BillSales_frm()
        {
            InitializeComponent();
            toolStripButton2.Click += frm_SearchItems.ShowSearchWindow;
        }
        Sales_CLS cls = new Sales_CLS();
        TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();
        Store_Prodecut_cls Store_Prodecut_cls = new Store_Prodecut_cls();
        AVG_cls AVG_cls = new AVG_cls();
        PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();

        string TransID = "0";
        string TreasuryID = "0";
        // Open New Bill
        public Boolean OpenNewBill = false;
        int UserID;
        private void PureItemToStoreForm_Load(object sender, EventArgs e)
        {

            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);

            FormBorderStyle = FormBorderStyle.Sizable;
            MaximizeBox = true;
            try
            {



                UserID = Properties.Settings.Default.UserID;
                FillCombAccount();
                //  FillProdecut();
                FillCurrency();
                FillPayType();
                CS_44.Visible = DataAccessLayer.CS_44;
                btnNew_Click(null, null);
                //     DGV1.Columns["CurrencyPrice"].ReadOnly = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            gridLookUpEdit1.Properties.DataSource = db.Drivers.Select(x => new { x.Name, x.ID, State = (db.DeliveryOrders.Where(d => d.HasReturned == false && d.DriverID == x.ID).Count() > 0) }).ToList();
            gridLookUpEdit1.Properties.ValueMember = "ID";
            gridLookUpEdit1.Properties.DisplayMember = "Name";
            var view = gridLookUpEdit1.Properties.View;
            view.PopulateColumns(gridLookUpEdit1.Properties.DataSource);
            view.Columns["State"].Caption = "الحاله";
            view.Columns["State"].ColumnEdit = new RepositoryItemTextEdit();
            view.Columns["ID"].Visible = false;
            view.Columns["Name"].Caption = "الاسم";
            view.RowCellStyle += View_RowCellStyle;
            view.CustomColumnDisplayText += View_CustomColumnDisplayText;



            GridControl_Items.ProcessGridKey += gridControl1_ProcessGridKey;
        }
        DAL.DBDataContext DetailsDataContexst;
        bool IsGridIntialized;
        RepositoryItemGridLookUpEdit repoItems = new RepositoryItemGridLookUpEdit();
        RepositoryItemLookUpEdit repoUOM = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoSize = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoColor = new RepositoryItemLookUpEdit();
        RepositoryItemDateEdit repoDate = new RepositoryItemDateEdit();
        RepositoryItemLookUpEdit Revenuerepo = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit PaySourceRepo = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoUsers = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoCurrency = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoStore = new RepositoryItemLookUpEdit();
        MasterClass.InvoicesType invoicesType { get => MasterClass.InvoicesType.Sales; }
        DAL.Sales_Detail InvoDInsta = new DAL.Sales_Detail();

        void GetData()
        {
          //  if (DetailsDataContexst == null)
                DetailsDataContexst = new DBDataContext(Properties.Settings.Default.Connection_String);

            IQueryable<DAL.Sales_Detail> InvoiceDetails =
                DetailsDataContexst.Sales_Details.Select<DAL.Sales_Detail, DAL.Sales_Detail>((Expression<Func<DAL.Sales_Detail, DAL.Sales_Detail>>)
                (x => x)).Where(x => x.InvoiceID == Convert.ToInt64(txtMainID.Text) && x.InvoiceType == (int)invoicesType );
            GridControl_Items.DataSource = InvoiceDetails;
            //GridView_Items_RowCountChanged(null, null);
            if (IsGridIntialized == false)
            {
                repoStore.NullText = repoItems.NullText = repoUOM.NullText = repoColor.NullText = repoSize.NullText = Revenuerepo.NullText = "";
                repoStore.ValueMember = "ID";
                repoStore.DisplayMember = "Name";
                GridView_Items.Columns[nameof(InvoDInsta.StoreID)].ColumnEdit = repoStore;

                // repoItems.CloseUp += glkp_Customer_CloseUp;
                repoItems.ValidateOnEnterKey = true;
                repoItems.AllowNullInput = DefaultBoolean.False;
                repoItems.BestFitMode = BestFitMode.BestFitResizePopup;
                repoItems.ImmediatePopup = true;
                repoItems.TextEditStyle = TextEditStyles.DisableTextEditor;
                repoItems.ValueMember = "ID";
                repoItems.DisplayMember = "Name";
                repoItems.Buttons.Add(new EditorButton("Add", ButtonPredefines.Plus));
                repoItems.ButtonClick += RepoItems_ButtonClick;
                GridView_Items.Columns[nameof(InvoDInsta.ItemID)].ColumnEdit = repoItems;







                var repositoryItemGridLookUpEdit1View = repoItems.View as  GridView; 
                repositoryItemGridLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
                repositoryItemGridLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = HorzAlignment.Far;
                repositoryItemGridLookUpEdit1View.Appearance.Row.Options.UseTextOptions = true;
                repositoryItemGridLookUpEdit1View.Appearance.Row.TextOptions.HAlignment = HorzAlignment.Near;
                repositoryItemGridLookUpEdit1View.FocusRectStyle = DrawFocusRectStyle.RowFocus;
                repositoryItemGridLookUpEdit1View.OptionsBehavior.AllowAddRows = DefaultBoolean.False;
                repositoryItemGridLookUpEdit1View.OptionsBehavior.AllowDeleteRows = DefaultBoolean.False;
                repositoryItemGridLookUpEdit1View.OptionsBehavior.AutoSelectAllInEditor = false;
                repositoryItemGridLookUpEdit1View.OptionsBehavior.AutoUpdateTotalSummary = false;
                repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
                repositoryItemGridLookUpEdit1View.OptionsSelection.UseIndicatorForSelection = true;
                repositoryItemGridLookUpEdit1View.OptionsView.BestFitMaxRowCount = 10;
                repositoryItemGridLookUpEdit1View.OptionsView.ShowAutoFilterRow = true;
                repositoryItemGridLookUpEdit1View.OptionsView.ShowDetailButtons = false;
                repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
                repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;


                GridView_Items.Columns[nameof(InvoDInsta.ItemUnitID)].ColumnEdit = repoUOM;




                RepositoryItemSpinEdit repospin = new RepositoryItemSpinEdit();
                RepositoryItemSpinEdit repospinPrecintage = new RepositoryItemSpinEdit();

                repospinPrecintage.Increment = 0.01M;
                repospinPrecintage.Mask.EditMask = "p";
                repospinPrecintage.Mask.UseMaskAsDisplayFormat = true;
                repospinPrecintage.MaxValue = 1;
                GridView_Items.Columns[nameof(InvoDInsta.Price)].ColumnEdit = repospin;
                GridView_Items.Columns[nameof(InvoDInsta.Discount)].ColumnEdit = repospinPrecintage;
                GridView_Items.Columns[nameof(InvoDInsta.DiscountValue)].ColumnEdit = repospin;

                GridView_Items.Columns[nameof(InvoDInsta.Vat)].ColumnEdit = repospinPrecintage;
                GridView_Items.Columns[nameof(InvoDInsta.VatValue)].ColumnEdit = repospin;

                GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].ColumnEdit = repospin;

                GridView_Items.Columns[nameof(InvoDInsta.Color)].Visible =
                      GridView_Items.Columns[nameof(InvoDInsta.Color)].OptionsColumn.ShowInCustomizationForm =
                GridView_Items.Columns[nameof(InvoDInsta.Size)].Visible =
                      GridView_Items.Columns[nameof(InvoDInsta.Size)].OptionsColumn.ShowInCustomizationForm =
                       GridView_Items.Columns[nameof(InvoDInsta.InvoiceType)].OptionsColumn.ShowInCustomizationForm =
                GridView_Items.Columns[nameof(InvoDInsta.InvoiceType)].Visible =
                  GridView_Items.Columns[nameof(InvoDInsta.CurrencyID)].Visible =
                    GridView_Items.Columns[nameof(InvoDInsta.CurrencyID)].OptionsColumn.ShowInCustomizationForm =
                  GridView_Items.Columns[nameof(InvoDInsta.CurrencyRate)].Visible =
                  GridView_Items.Columns[nameof(InvoDInsta.PurchasePrice)].Visible =
                    GridView_Items.Columns[nameof(InvoDInsta.CurrencyRate)].OptionsColumn.ShowInCustomizationForm = false;


                GridView_Items.Columns[nameof(InvoDInsta.ExpDate)].ColumnEdit = repoDate;


                GridControl_Items.RepositoryItems.AddRange(new RepositoryItem[]
                {
                    repoStore,repoItems , repoUOM, repospin, /*repoColor,*/  /*repoSize, */repoDate,repospinPrecintage
                });

                //GridView_Items.Columns.Add(new GridColumn() { VisibleIndex = 0, Name = "clmIndex", FieldName = "Index", UnboundType = DevExpress.Data.UnboundColumnType.Integer, MaxWidth = 25 });
                //GridView_Items.Columns["Index"].OptionsColumn.AllowFocus = false;
                //GridView_Items.Columns.Add(new GridColumn() { VisibleIndex = 1, Name = "clmCode", FieldName = "Code", UnboundType = DevExpress.Data.UnboundColumnType.String });

                GridView_Items.Columns["ID"].OptionsColumn.AllowShowHide = GridView_Items.Columns["InvoiceID"].OptionsColumn.AllowShowHide =
                GridView_Items.Columns["ID"].Visible = GridView_Items.Columns["InvoiceID"].Visible = false;

                this.GridView_Items.ColumnPanelRowHeight = 35;
                this.GridView_Items.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
                this.GridView_Items.GridControl = this.GridControl_Items;
                this.GridView_Items.Name = "GridView_Items";
                this.GridView_Items.OptionsBehavior.FocusLeaveOnTab = true;
                this.GridView_Items.OptionsBehavior.KeepFocusedRowOnUpdate = false;
                this.GridView_Items.OptionsMenu.ShowConditionalFormattingItem = true;
                this.GridView_Items.OptionsNavigation.AutoFocusNewRow = true;
                this.GridView_Items.OptionsPrint.ExpandAllDetails = true;
                this.GridView_Items.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
                this.GridView_Items.OptionsView.ShowFooter = true;
                this.GridView_Items.OptionsView.ShowGroupPanel = false;

                GridView_Items.CustomRowCellEditForEditing +=GridView_Items_CustomRowCellEditForEditing;
                GridView_Items.FocusedRowChanged +=GridView_Items_FocusedRowChanged;
                GridView_Items.FocusedColumnChanged +=GridView_Items_FocusedColumnChanged;
                GridView_Items.CellValueChanged += GridView_Items_CellValueChanged;
                GridView_Items.InvalidRowException +=GridView_Items_InvalidRowException;
                GridView_Items.ValidateRow += GridView_Items_ValidateRow;
                GridView_Items.RowUpdated +=GridView_Items_RowUpdated;
                GridView_Items.RowCountChanged +=GridView_Items_RowCountChanged;
                GridView_Items.CustomColumnDisplayText += GridView_Items_CustomColumnDisplayText;
                GridView_Items.CustomDrawCell += GridView_Items_CustomDrawCell;
                GridView_Items.CustomUnboundColumnData += GridView_Items_CustomUnboundColumnData;
                GridView_Items.InitNewRow += GridView_Items_InitNewRow;
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyRate)].Caption = "";
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyID)].Caption = "العمله";
                GridView_Items.Columns[nameof(InvoDInsta.Color)].Caption = "اللون";
                GridView_Items.Columns[nameof(InvoDInsta.Size)].Caption = "الحجم";



                GridView_Items.Columns[nameof(InvoDInsta.ItemID)].Caption = "الصنف";
                GridView_Items.Columns[nameof(InvoDInsta.ItemUnitID)].Caption = "الوحده";
                GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].Caption = "الكميه";
                GridView_Items.Columns[nameof(InvoDInsta.Price)].Caption = "السعر";
                GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].Caption = "الاجمالي";
                GridView_Items.Columns[nameof(InvoDInsta.Discount)].Caption = "ن خصم";
                GridView_Items.Columns[nameof(InvoDInsta.DiscountValue)].Caption = "ق خصم";
                GridView_Items.Columns[nameof(InvoDInsta.Vat)].Caption = "VAT %";
                GridView_Items.Columns[nameof(InvoDInsta.VatValue)].Caption = "VAT";
                GridView_Items.Columns[nameof(InvoDInsta.NetPrice)].Caption = "الصافي";
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyNet)].Caption = "الصافي بسعر العمله";
                GridView_Items.Columns[nameof(InvoDInsta.ExpDate)].Caption = "الصلاحيه";
                GridView_Items.Columns[nameof(InvoDInsta.StoreID)].Caption = "المخزن";
                GridView_Items.Columns[nameof(InvoDInsta.PurchasePrice)].Caption = "سعر الشراء";
                GridView_Items.Columns[nameof(InvoDInsta.TotalPurcasePrice)].Caption = "الاجمالي بسعر الشراء";

                GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].OptionsColumn.AllowFocus =
                GridView_Items.Columns[nameof(InvoDInsta.NetPrice)].OptionsColumn.AllowFocus =
                GridView_Items.Columns[nameof(InvoDInsta.PurchasePrice)].OptionsColumn.AllowFocus =
                GridView_Items.Columns[nameof(InvoDInsta.TotalPurcasePrice)].OptionsColumn.AllowFocus =
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyNet)].OptionsColumn.AllowFocus = false;


                GridView_Items.Columns.Add(new GridColumn() {   Name = "clmIndex", FieldName = "Index",Caption ="م", UnboundType = DevExpress.Data.UnboundColumnType.Integer, MaxWidth = 25 });
                GridView_Items.Columns["Index"].OptionsColumn.AllowFocus = false;
                GridView_Items.Columns.Add(new GridColumn() {   Name = "clmCode", FieldName = "Code", Caption = "باركود", UnboundType = DevExpress.Data.UnboundColumnType.String });

                RepositoryItemButtonEdit buttonEdit = new RepositoryItemButtonEdit();
                GridControl_Items .RepositoryItems.Add(buttonEdit);
                buttonEdit.Buttons.Clear();
                buttonEdit.Buttons.Add(new EditorButton(ButtonPredefines.Delete));
                buttonEdit.ButtonClick += ButtonEdit_ButtonClick;
                GridColumn clmnDelete = new GridColumn() { Name = "Delete", Caption =" ",FieldName = "Delete", ColumnEdit = buttonEdit, VisibleIndex = 25, Width = 15 };
                buttonEdit.TextEditStyle = TextEditStyles.HideTextEditor;
                GridView_Items.Columns.Add(clmnDelete);

                GridView_Items.Columns["Index"].VisibleIndex = 0;
                GridView_Items.Columns["Code"].VisibleIndex = 1;
                GridView_Items.Columns[nameof(InvoDInsta.ItemID)].VisibleIndex = 2;
                GridView_Items.Columns[nameof(InvoDInsta.ItemUnitID)].VisibleIndex = 3;
                GridView_Items.Columns[nameof(InvoDInsta.ItemQty )].VisibleIndex = 4;
                GridView_Items.Columns[nameof(InvoDInsta.Price)].VisibleIndex = 5;
                GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].VisibleIndex = 6;
                GridView_Items.Columns[nameof(InvoDInsta.Discount)].VisibleIndex = 7;
                GridView_Items.Columns[nameof(InvoDInsta.DiscountValue)].VisibleIndex = 8;
                GridView_Items.Columns[nameof(InvoDInsta.Vat)].VisibleIndex = 9;
                GridView_Items.Columns[nameof(InvoDInsta.VatValue)].VisibleIndex = 10;
                GridView_Items.Columns[nameof(InvoDInsta.NetPrice)].VisibleIndex = 11;
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyNet)].VisibleIndex = 12;
                GridView_Items.Columns[nameof(InvoDInsta.ExpDate)].VisibleIndex = 13;
                GridView_Items.Columns[nameof(InvoDInsta.StoreID)].VisibleIndex = 14;

                //GridView_Items.Columns[nameof(InvoDInsta.Vat)].Summary.Clear();
                GridView_Items.Columns[nameof(InvoDInsta.ItemID)].Summary.Add(DevExpress.Data.SummaryItemType.Count , nameof(InvoDInsta.ItemID), "{0}عدد ");
                GridView_Items.Columns[nameof(InvoDInsta.ItemQty)].Summary.Add(DevExpress.Data.SummaryItemType.Count, nameof(InvoDInsta.ItemQty), "{0} قطع");

                GridView_Items.Columns[nameof(InvoDInsta.VatValue)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(InvoDInsta.VatValue), "{0}");
                GridView_Items.Columns[nameof(InvoDInsta.CurrencyNet)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(InvoDInsta.CurrencyNet), "{0}");
                GridView_Items.Columns[nameof(InvoDInsta.NetPrice)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(InvoDInsta.NetPrice), "{0}");
                GridView_Items.Columns[nameof(InvoDInsta.TotalPrice)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(InvoDInsta.TotalPrice), "{0}");


                IsGridIntialized = true;
            }
            RefreshData();
        }

        private void GridView_Items_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            GridView view = GridView_Items as GridView;
            int index = e.RowHandle ;
            DAL.Sales_Detail detail = view.GetRow(index) as DAL.Sales_Detail;
            detail.CurrencyID = combCurrency.SelectedValue.ToInt();
            detail.CurrencyRate = Convert.ToDouble(txtCurrencyRate.Text);
            if (BillSetting_cls.UseVat == true)
            {
            detail.Vat  =Convert.ToDouble(BillSetting_cls.Vat) /100;
            }
         
        }

        private void ButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = ((GridControl)((ButtonEdit)sender).Parent).MainView as GridView;

            if (view.FocusedRowHandle >= 0)
            {
                view.DeleteSelectedRows();
            }

        }
        Dictionary<string, string> CustomUOMNamesForItems = new Dictionary<string, string>();
        List<DAL.Prodecut> prodecuts;
        private void GridView_Items_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == nameof(InvoDInsta.ItemUnitID))
            {
                GridView view = GridView_Items as GridView;
                int index = view.GetRowHandle(e.ListSourceRowIndex);
                DAL.Sales_Detail detail = view.GetRow(index) as DAL.Sales_Detail;
                if (detail == null) return;
                var db = new DBDataContext(Properties.Settings.Default.Connection_String);
                if (prodecuts == null)
                    prodecuts = db.Prodecuts.ToList();
                var product = prodecuts.SingleOrDefault(x => x.ProdecutID == detail.ItemID);
                if (prodecuts == null) return;
                if (detail.ItemUnitID == 1)
                    e.DisplayText = product.FiestUnit;
                else if (detail.ItemUnitID == 2)
                    e.DisplayText = product.SecoundUnit;
                else if (detail.ItemUnitID == 3)
                    e.DisplayText = product.ThreeUnit ;
            }
        }

        void SaveDetails()
        {
            var db = new DBDataContext(Properties.Settings.Default.Connection_String); 
            if (GridView_Items.FocusedRowHandle < 0)
            {
                GridView_Items_ValidateRow(GridView_Items, new ValidateRowEventArgs(GridView_Items.FocusedRowHandle, GridView_Items.GetRow(GridView_Items.FocusedRowHandle)));

            }
            var invoiceDetails = ((Collection<DAL.Sales_Detail >)GridView_Items.DataSource);
            foreach (var item in invoiceDetails)
            {
                item.InvoiceID = Convert.ToInt64(txtMainID.Text);
                item.InvoiceType = (byte)invoicesType;
            }
            DetailsDataContexst.SubmitChanges();
            var invoice = db.Sales_Mains.SingleOrDefault(x => x.MainID == Convert.ToInt64(txtMainID.Text));

            db.Inv_StoreLogs.DeleteAllOnSubmit(db.Inv_StoreLogs.Where(x => x.Type == (int)invoicesType  && invoiceDetails.Select(d => d.ID).Contains(x.TypeID ?? 0)));
            db.SubmitChanges();
            foreach (var item in invoiceDetails)
            {
                var product = db.Prodecuts.Single(x => x.ProdecutID == item.ItemID);
                bool IsStockOut;
                double Factor = 1;
                var MainMessage = "";

                switch (item.ItemUnitID)
                {
                    case 1:
                        Factor =Convert.ToDouble( product.FiestUnitFactor);
                        break;
                    case 2:
                        Factor = Convert.ToDouble(product.SecoundUnitFactor );

                        break;
                    case 3:
                        Factor = Convert.ToDouble(product.ThreeUnitFactor);

                        break;
                }
                switch (invoicesType)
                {
                    case MasterClass.InvoicesType.Sales:
                        IsStockOut = true;
                        MainMessage = string.Format("فاتوره مبيعات رقم {0}", invoice.MainID);

                        break;
                    case MasterClass.InvoicesType.Purchase:
                        IsStockOut = false ;
                        MainMessage = string.Format("فاتوره مشتريات رقم {0}", invoice.MainID);

                        break;
                    default:
                       throw new NotImplementedException();
                }

                db.Inv_StoreLogs.InsertOnSubmit(new Inv_StoreLog()
                {
                    Type =item.InvoiceType ,
                    TypeID = item.ID,
                    Batch ="",
                    BuyPrice = item.NetPrice  / item.ItemQty,
                    Color = item.Color,
                    date = invoice.MyDate,
                    ExpDate = item.ExpDate,
                    ItemID = item.ItemID,
                    ItemQuIN = IsStockOut ? 0 : item.ItemQty * Factor,
                    ItemQuOut = IsStockOut ? item.ItemQty * Factor : 0,
                    Note = MainMessage,
                    SellPrice = item.NetPrice / item.ItemQty,
                    Serial = "",
                    Size = item.Size,
                    StoreID = item.StoreID 
                });
            }
            db.SubmitChanges();
        }

    void RefreshData()
        {
             var db = new DBDataContext(Properties.Settings.Default.Connection_String);
             repoStore.DataSource = db.Stores.Select(x => new { ID = x.StoreID, Name = x.StoreName }).ToList() ;
            repoItems.DataSource =db.Prodecuts .Select(x=> new { ID =x.ProdecutID , Name = x.ProdecutName ,
            Category = db.Categories.SingleOrDefault(c=>c.CategoryID == x.CategoryID ).CategoryName ??"" }).ToList();
            repoUOM.DataSource = db.Units.Select(x => new { ID = x.UnitID, Name = x.UnitName }).ToList();
        }
        public static void GridView_Items_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            ColumnView columnView = sender as ColumnView;
            int h = e.RowHandle;
            GridView view = sender as GridView;
            DAL.Sales_Detail detail = view.GetRow(h) as DAL.Sales_Detail;
            if (e.RowHandle < 0) return;
            if (detail.ExpDate.HasValue )
            {
                if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpDate")) <= DateTime.Now)
                    e.Appearance.BackColor = System.Drawing.Color.Red;
                else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpDate")).AddDays(-15) <= DateTime.Now)
                    e.Appearance.BackColor = System.Drawing.Color.DarkOrange;
                else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpDate")).AddDays(-30) <= DateTime.Now)
                    e.Appearance.BackColor = System.Drawing.Color.Orange;
                else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpDate")).AddDays(-45) <= DateTime.Now)
                    e.Appearance.BackColor = System.Drawing.Color.Yellow;
                else if (Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "ExpDate")).AddDays(-60) <= DateTime.Now)
                    e.Appearance.BackColor = System.Drawing.Color.LightYellow;
            }

        }
        private void GridView_Items_RowUpdated(object sender, RowObjectEventArgs e)
        {
            var items = GridView_Items.DataSource as Collection<DAL.Sales_Detail>;
            if (items == null)
                txtTotalInvoice.Text = txtTotalBayPrice.Text = txtVatValue.Text = "0";
            else
            {
                txtTotalInvoice .Text = items.Sum(x => x.NetPrice).ToString();
                txtVatValue.Text = items.Sum(x => x.VatValue ).ToString();
                txtTotalBayPrice.Text  = items.Sum(x => x.TotalPurcasePrice ).ToString();

            }
        
        }

        private void GridView_Items_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            var dbc = new DBDataContext(Properties.Settings.Default.Connection_String);

            GridView Senderview = sender as GridView;

            if (e.Column.FieldName == "ItemUnitID")
            {
                RepositoryItemGridLookUpEdit UOMRepoEXP = new RepositoryItemGridLookUpEdit();
                GridView view = new GridView();
                UOMRepoEXP.View = view;
                UOMRepoEXP.NullText = "";

                DAL.Sales_Detail detail = Senderview.GetRow(e.RowHandle ) as DAL.Sales_Detail;

                if (detail.ItemID .ValidAsIntNonZero() == false )
                { e.RepositoryItem = new RepositoryItem(); return; }
                var item = dbc.Prodecuts.Where(x => x.ProdecutID == Convert.ToInt32(Senderview.GetRowCellValue(e.RowHandle, "ItemID"))).Single();
                var datasource = new[] { new { Name = item.FiestUnit, ID = 1 }, new { Name = item.SecoundUnit, ID = 2 } , new { Name = item.ThreeUnit, ID = 3 } };


                var itemunit = datasource.Where(x => string.IsNullOrEmpty(x.Name) == false).ToList();

                UOMRepoEXP.DataSource = itemunit;
                UOMRepoEXP.DisplayMember = "Name";
                UOMRepoEXP.ValueMember = "ID";
                UOMRepoEXP.View.PopulateColumns(itemunit.ToList());
                view.Columns["ID"].Visible = false;
                e.RepositoryItem = UOMRepoEXP;
            }

             
        }
        MasterClass.ItemLog SharedLog;
        private void GridView_Items_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.RowHandle > GridView_Items.RowCount - 1) return;
            GridControl gridControl = sender as GridControl;
            GridView View = GridView_Items as GridView;
            var dbc = new DBDataContext(Properties.Settings.Default.Connection_String);

            int index = e.RowHandle;
            DAL.Sales_Detail  detail = View.GetRow(index) as DAL.Sales_Detail ;
            if (detail == null)
                return;
            var item = dbc.Prodecuts.SingleOrDefault(x => x.ProdecutID  == detail.ItemID);
            if (e.Column == null) return;
            switch (e.Column.FieldName)
            {
                case "Code":
                    if (e.Value == null || e.Value == DBNull.Value || e.Value.ToString().Trim() == string.Empty) View.DeleteRow(index);
                    if (CurrentSession.SystemSettings.UseScalBarcode && e.Value.ToString().Length == CurrentSession.SystemSettings.WeightCodeLength + CurrentSession.SystemSettings.ItemCodeLength)
                    {
                        var itemCode = e.Value.ToString().Substring(0, CurrentSession.SystemSettings.ItemCodeLength);
                        var weightText = e.Value.ToString().Substring(CurrentSession.SystemSettings.ItemCodeLength, CurrentSession.SystemSettings.WeightCodeLength);

                        double weight = 0;
                        double.TryParse(weightText, out weight);
                        SharedLog = MasterClass.SearchForItem(itemCode, (string.IsNullOrEmpty(txtStoreID.Text)) ? 0 : Convert.ToInt32(txtStoreID.Text));
                        if (weight > 0)
                        {
                            weight = weight / (double)CurrentSession.SystemSettings.DivideWeightBy; 
                            detail.ItemQty = weight;
                        }
                    }
                    else
                    {
                        SharedLog = MasterClass.SearchForItem(e.Value.ToString(), (string.IsNullOrEmpty(txtStoreID.Text)) ? 0 : Convert.ToInt32(txtStoreID.Text));
                    }
                    if (SharedLog.ItemID == 0)
                    {
                        View.DeleteRow(index);
                        break;
                    }
                    detail.ItemID = SharedLog.ItemID;
                    if(CodesDictionary.ContainsKey(index))
                       CodesDictionary.Remove(index);
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemID)], detail.ItemID));

                    break;
                case nameof(InvoDInsta.ItemID):
                    if (detail.ItemID == 0)   //e.Value == null || e.Value == DBNull.Value || e.Value.ToString().Trim() == string.Empty)
                    {
                        View.DeleteRow(index);
                        break;
                    }
                    if (item == null) return;
                    if (SharedLog.ItemID == 0)
                        SharedLog = MasterClass.GetNextItemForSell(detail.ItemID, Convert.ToInt32(txtStoreID.Text));
                    
                    detail.ItemUnitID = SharedLog.UnitID;

                    if (detail.ItemUnitID.ValidAsIntNonZero() ==false)
                        detail.ItemUnitID = item.UnitDefoult ?? 1 ;
                    
                    detail.StoreID = SharedLog.StoreID == 0 ? Convert.ToInt32(txtStoreID.Text) : SharedLog.StoreID;
                    if (detail.ItemQty <= 0)
                        detail.ItemQty = 1;
                    detail.Color = SharedLog.Color;
                    detail.ExpDate = SharedLog.ExpirDate;
                    detail.Size = SharedLog.Size; 
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemUnitID)], detail.ItemUnitID));
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemQty)], detail.ItemQty));
                    break;
                case nameof(InvoDInsta.ItemUnitID):
                    if (detail.ItemUnitID <= 0) break;
                    if (item == null) return;
                    if (detail.ItemUnitID == 1)
                    {
                        if (SalesLevel1.Checked)
                            detail.Price = item.FiestUnitPrice1??0;
                        else if (SalesLevel2.Checked)
                            detail.Price = item.FiestUnitPrice2 ?? 0;
                        else
                            detail.Price = item.FiestUnitPrice3 ?? 0;
                        detail.PurchasePrice =( item.ProdecutBayPrice ?? 0/Convert.ToDouble( item.FiestUnitFactor)) * Convert.ToDouble(item.FiestUnitFactor);
                    }
                    else if (detail.ItemUnitID == 2)
                    {
                        if (SalesLevel1.Checked)
                            detail.Price = item.SecoundUnitPrice1 ?? 0;
                        else if (SalesLevel2.Checked)
                            detail.Price = item.SecoundUnitPrice2 ?? 0;
                        else
                            detail.Price = item.SecoundUnitPrice3 ?? 0;
                        detail.PurchasePrice = (item.ProdecutBayPrice ?? 0 / Convert.ToDouble(item.FiestUnitFactor)) * Convert.ToDouble(item.SecoundUnitFactor);


                    }
                    else if (detail.ItemUnitID == 3)
                    {
                        if (SalesLevel1.Checked)
                            detail.Price = item.ThreeUnitPrice1 ?? 0;
                        else if (SalesLevel2.Checked)
                            detail.Price = item.ThreeUnitPrice2 ?? 0;
                        else
                            detail.Price = item.ThreeUnitPrice3 ?? 0;
                        detail.PurchasePrice = (item.ProdecutBayPrice ?? 0 / Convert.ToDouble(item.FiestUnitFactor)) * Convert.ToDouble(item.ThreeUnitFactor);


                    }
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.Price)], detail.Price));
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.ItemQty)], detail.ItemQty));
                    break;
                case nameof(InvoDInsta.ItemQty):
                    if (detail.ItemID == 0) break;
                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.Price)], View.GetRowCellValue(index, nameof(InvoDInsta.Price))));
                    break;
                case nameof(InvoDInsta.Price):
                case nameof(InvoDInsta.Discount):
                case nameof(InvoDInsta.Vat ):
                    // if (View.FocusedColumn.FieldName == "DiscountVal") break;
                    detail.DiscountValue = detail.Discount * (detail.ItemQty * detail.Price);
                    detail.VatValue = detail.Vat * (detail.ItemQty * detail.Price);

                    GridView_Items_CellValueChanged(sender, new CellValueChangedEventArgs(index, View.Columns[nameof(InvoDInsta.DiscountValue)], detail.DiscountValue));
                    break;
                case nameof(InvoDInsta.DiscountValue):
                case nameof(InvoDInsta.VatValue):
                    detail.TotalPrice = (detail.ItemQty * detail.Price);
                    
                    detail.TotalPurcasePrice = (detail.ItemQty * detail.PurchasePrice);
                    if (View.FocusedColumn.FieldName == nameof(InvoDInsta.DiscountValue))
                    {
                        detail.Discount = detail.DiscountValue / detail.TotalPrice ;
                    }
                    if (View.FocusedColumn.FieldName == nameof(InvoDInsta.VatValue ))
                    {
                        detail.Vat  = detail.VatValue  / detail.TotalPrice ;
                    }
                    detail.NetPrice = detail.TotalPrice + detail.VatValue  - detail.DiscountValue;
                    detail.CurrencyNet  = detail.NetPrice * detail.CurrencyRate ;

                    break;
            }
            SharedLog.ItemID = 0;

        }
        Dictionary<int, string> CodesDictionary = new Dictionary<int, string>();
        private void GridView_Items_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            
            if (e.Column.FieldName == "Index")
            {
                if (e.IsGetData)
                    e.Value = e.ListSourceRowIndex + 1;
            }
            else if (e.Column.FieldName == "Code")
            {
                if (e.IsGetData)
                {

                    var code = "";
                    CodesDictionary.TryGetValue(GridView_Items.GetRowHandle(e.ListSourceRowIndex), out code);
                    e.Value = code;
                    
                }
                else
                {
                    CodesDictionary.Remove(GridView_Items.GetRowHandle(e.ListSourceRowIndex));
                    if (e.Value != null)
                        CodesDictionary.Add(GridView_Items.GetRowHandle(e.ListSourceRowIndex), e.Value.ToString());
                }
            }
          

        }
        private void GridView_Items_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            GridView view = sender as GridView;
            if (view == null || view.Columns.Count == 0) return;
            DAL.Sales_Detail detail = view.GetRow(e.FocusedRowHandle) as DAL.Sales_Detail; 
            //if (detail == null)
            //    return;
            //Inv_Item item = CurrentSession.Products.Where(x => x.ID == detail.ItemID).SingleOrDefault();
            //if (item == null) return;

            //int h = e.FocusedRowHandle;
            //view.Columns[nameof(InvoDInsta.Size)].OptionsColumn.AllowEdit = item.Size;
            //view.Columns[nameof(InvoDInsta.ExpDate)].OptionsColumn.AllowEdit = item.Expier;
            //view.Columns[nameof(InvoDInsta.Color)].OptionsColumn.AllowEdit = item.Color;
            //view.Columns[nameof(InvoDInsta.Serial)].OptionsColumn.AllowEdit = item.Serial;
            //view.Columns[nameof(InvoDInsta.ItemQty)].OptionsColumn.AllowEdit = !item.Serial;
            //if (!view.Columns[nameof(InvoDInsta.ItemQty)].OptionsColumn.AllowEdit)
            //    view.SetRowCellValue(h, nameof(InvoDInsta.ItemQty), 1);


        }


        private void GridView_Items_FocusedColumnChanged(object sender, FocusedColumnChangedEventArgs e)
        {
            GridView_Items_FocusedRowChanged(sender, new FocusedRowChangedEventArgs(0, GridView_Items.FocusedRowHandle));

        }
        private void GridView_Items_ValidateRow(object sender, ValidateRowEventArgs e)
        {
            GridView view = sender as GridView;
            ColumnView columnView = sender as ColumnView;
            int index = e.RowHandle;
            DAL.Sales_Detail detail = e.Row as DAL.Sales_Detail;
            if (detail == null || detail.ItemID <= 0)
            {
                e.Valid = false;
                return;
            }
            if (detail.ItemID.ValidAsIntNonZero() == false)
            {
                e.Valid = false;
                view.SetColumnError(view.Columns[nameof(detail.ItemID)], "يجب اختيار الصنف ");
            }
            // if (columnView.get)
            if (detail.ItemQty <= 0)
            {
                e.Valid = false;
                view.SetColumnError(view.Columns[nameof(detail.ItemQty)], "يجب ان تكون الكميه اكبر من صفر ");
            }
            if (detail.PurchasePrice > detail.Price )
            {
                if (MessageBox.Show("سعر البيـع اقل من سعر الشـراء بمقدار :  " + (detail.Price - detail.PurchasePrice).ToString() + Environment.NewLine + "هل تريد الاستمــرار ؟؟ ", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    e.Valid = false;
                    view.SetColumnError(view.Columns[nameof(detail.Price)], "سعر البيع غير مقبول ");
                     
                }

            }
            else if (detail.PurchasePrice == detail.Price)
            {
                if (MessageBox.Show("سعر البيع يساوي سعر الشراء : هل تريد البيع بسعر التكلفة", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    e.Valid = false;
                    view.SetColumnError(view.Columns[nameof(detail.Price)], "سعر البيع غير مقبول ");
            
                }
            }

            //if (detail.TotalPrice < detail.TotalCostValue)
            //{
            //    if (CurrentSession.user.SellLowerPriceThanBuy == 1)
            //    {
            //        e.Valid = false;
            //        view.SetColumnError(view.Columns[nameof(detail.Price)], LangResource.ErrorCantSellLowerPriceThanBuy);

            //    }
            //    else if (CurrentSession.user.SellLowerPriceThanBuy == 0)
            //    {

            //        view.SetColumnError(view.Columns[nameof(detail.Price)], LangResource.ItemPriceIsLowerThanCostPrice + "-" + LangResource.PressESCToDismess, DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning);
            //        //var dialogResult  = XtraMessageBox.Show(text: LangResource.DoYouWantToSellWithPriceLowerThanCost, caption: LangResource.Warning, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //        //if(dialogResult == DialogResult.No)
            //        //{
            //        //    e.Valid = false;
            //        //    view.SetColumnError(view.Columns[nameof(detail.Price)], LangResource.ErrorCantSellLowerPriceThanBuy, DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning );
            //        //}
            //    }
            //}


            //if (item.Color && detail.Color.ValidAsIntNonZero() == false)
            //{
            //    e.Valid = false;
            //    view.SetColumnError(view.Columns[nameof(detail.Color)], LangResource.ErrorCantBeEmpry);
            //    view.Columns[nameof(detail.Color)].Visible = true;

            //}
            //if (item.Size && detail.Size.ValidAsIntNonZero() == false)
            //{
            //    e.Valid = false;
            //    view.SetColumnError(view.Columns[nameof(detail.Size)], LangResource.ErrorCantBeEmpry);
            //    view.Columns[nameof(detail.Size)].Visible = true;

            //}
            //if (item.Expier && detail.ExpDate.HasValue == false)
            //{
            //    view.Columns[nameof(detail.ExpDate)].Visible = true;
            //    e.Valid = false;
            //    view.SetColumnError(view.Columns[nameof(detail.ExpDate)], LangResource.ErrorCantBeEmpry);
            //}

            //if (detail.ExpDate.HasValue)
            //{
            //    int relationship = DateTime.Compare(detail.ExpDate.Value, DateTime.Now.Date);
            //    if (relationship <= 0)
            //    {
            //        //if (CurrentSession.user.WhenSelllingItemHasExpired == 1)
            //        //{
            //        //    if (XtraMessageBox.Show(LangResource.WarningItemHasExpired, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            //        //    {
            //        //        view.DeleteRow(e.RowHandle);
            //        //    }
            //        //}
            //        //if (CurrentSession.user.WhenSelllingItemHasExpired == 2)
            //        //{
            //            view.Columns[nameof(detail.ExpDate)].Visible = true;
            //            e.Valid = false;
            //          //  view.SetColumnError(view.Columns[nameof(detail.ExpDate)], LangResource.ErrorCantBeEmpry);
            //        //} 
            //    }
            //}

            //if (detail.ItemQty > 0 && IsNew && Master.IsItemReachedReorderLevel(item.ID, detail.StoreID.ToInt()))
            //{
            //    if (CurrentSession.user.WhenSellingQtyLessThanReorder == 1)
            //    {
            //        if (XtraMessageBox.Show(LangResource.WarningItemTakeRechedReorderLevel, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            //        {
            //            view.DeleteRow(e.RowHandle);
            //        }
            //    }
            //    if (CurrentSession.user.WhenSellingQtyLessThanReorder == 2)
            //    {
            //        XtraMessageBox.Show(LangResource.ErorrCantTakeSellItemReachedReorderLevel, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        view.DeleteRow(e.RowHandle);
            //    }


            //}



        }

        private void GridView_Items_InvalidRowException(object sender, InvalidRowExceptionEventArgs e)
        {
            if ((e.Row as DAL.Sales_Detail).ItemID == 0)
                e.ExceptionMode = ExceptionMode.Ignore;
            else
                e.ExceptionMode = ExceptionMode.NoAction;
            
        }

        private void GridView_Items_RowCountChanged(object sender, EventArgs e)
        {

            GridView_Items_RowUpdated(sender, new RowObjectEventArgs(0, "Index"));

        }
        private void RepoItems_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.GetType() != typeof(EditorButton) || e.Button.Tag == null)
                return;

            string btnName = e.Button.Tag.ToString();
            if (btnName == "Add")
            {
                // frm_Main.OpenForm(new frm_Item());
                RefreshData();
            }
        }
        private void gridControl1_ProcessGridKey(object sender, KeyEventArgs e)
        {
            try
            {

                GridControl gridControl = sender as GridControl;
                GridView focusedView = gridControl.FocusedView as GridView;
                if (e.KeyCode == Keys.Return)
                {
                    GridColumn focusedColumn = (gridControl.FocusedView as ColumnView).FocusedColumn;
                    int focusedRowHandle = (gridControl.FocusedView as ColumnView).FocusedRowHandle;

                    var FocusedColumnName = "";
                    if (focusedView.FocusedColumn == focusedView.Columns["Code"] || focusedView.FocusedColumn ==  focusedView.Columns["ItemID"])
                    {
                        FocusedColumnName = focusedView.FocusedColumn.FieldName ;
                        gridControl1_ProcessGridKey(sender, new KeyEventArgs(Keys.Tab));
                    }
                    if (focusedView.FocusedRowHandle < 0)
                    {
                        focusedView.AddNewRow();
                        if (FocusedColumnName != "")
                            focusedView.FocusedColumn = focusedView.Columns[FocusedColumnName];
                        else 
                        focusedView.FocusedColumn = focusedView.Columns["Code"];
                    }
                    else
                    {
                        focusedView.FocusedRowHandle = focusedRowHandle + 1;
                        if (FocusedColumnName != "")
                            focusedView.FocusedColumn = focusedView.Columns[FocusedColumnName];
                        else
                            focusedView.FocusedColumn = focusedView.Columns["Code"];
                    }
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Tab && e.Modifiers != Keys.Shift)
                {
                    if (focusedView.FocusedColumn.VisibleIndex == 0)
                        focusedView.FocusedColumn = focusedView.VisibleColumns[focusedView.VisibleColumns.Count - 1];
                    else
                        focusedView.FocusedColumn = focusedView.VisibleColumns[focusedView.FocusedColumn.VisibleIndex - 1];
                    e.Handled = true;
                }
                else if (e.KeyCode == Keys.Delete && e.Modifiers == Keys.Control)
                    focusedView.DeleteSelectedRows();
                //else if (e.KeyCode == Keys.Tab && e.Modifiers == Keys.Shift)
                //{
                //    if (focusedView.FocusedColumn.VisibleIndex == focusedView.VisibleColumns.Count)
                //        focusedView.FocusedColumn = focusedView.VisibleColumns[0];
                //    else
                //        focusedView.FocusedColumn = focusedView.VisibleColumns[focusedView.FocusedColumn.VisibleIndex + 1];
                //    e.Handled = true;
                //}
                //else
                //{
                //    //if (e.KeyCode != Keys.Up || focusedView.GetFocusedRow() == null || !(focusedView.GetFocusedRow() as DataRowView).IsNew || focusedView.GetFocusedRowCellValue("ItemId") != null && !(focusedView.GetFocusedRowCellValue("ItemId").ToString() == string.Empty))
                //    //    return;
                //    //focusedView.DeleteRow(focusedView.FocusedRowHandle);
                //}
            }
            catch
            {
            }
        }

        private void View_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if(e.Column.FieldName == "State")
            {
                e.DisplayText = (e.Value is bool d && d == true) ? "مشغول" : "متاح";
            }
        }

        private void View_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
             if(e.Column.FieldName == "State")
            {
                if(e.CellValue is bool d && d == true)
                {
                    e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Warning ;
                }
                else
                {
                    e.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;

                }
            }
        }

        public void FillCurrency()
        {
            string cusName = combCurrency.Text;
            Currency_cls Currency_cls = new Currency_cls();
            combCurrency.DataSource = Currency_cls.Search__Currency("");
            combCurrency.DisplayMember = "CurrencyName";
            combCurrency.ValueMember = "CurrencyID";
            combCurrency.Text = cusName;

        }
        //public void FillProdecut()
        //{
        //    loadaccount_CLS.ItemsLoadtxt(txtSearch);

        //}
        public void FillCombAccount()
        {
            string cusName = combAccount.Text;
            string cusID = txtAccountID.Text;
            Customers_cls CUS = new Customers_cls();
            combAccount.DataSource = CUS.Search_Customers("");
            combAccount.DisplayMember = "CustomerName";
            combAccount.ValueMember = "CustomerID";
            combAccount.Text = cusName;
            txtAccountID.Text = cusID;

        }

        public void FillPayType()
        {
            string cusName = combPayType.Text;
            PayType_cls PayType_cls = new PayType_cls();
            combPayType.DataSource = PayType_cls.Search__PayType("");
            combPayType.DisplayMember = "PayTypeName";
            combPayType.ValueMember = "PayTypeID";
            combPayType.Text = cusName;

        }




        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                StoreSearch_frm frm = new StoreSearch_frm();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtStoreName.BackColor = Color.WhiteSmoke;
                    txtStoreID.Text = frm.DGV1.CurrentRow.Cells["StoreID"].Value.ToString();
                    txtStoreName.Text = frm.DGV1.CurrentRow.Cells["StoreName"].Value.ToString();
                 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        Prodecut_cls Prodecut_CLS = new Prodecut_cls();
        DataTable dt_Prodecut = new DataTable();
        public Boolean ProductService = false;


        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveBill(false);
        }


        private void SaveBill(Boolean print)
        {

            

                if (GridView_Items.RowCount <= 0)
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (combAccount.Text.Trim() == "" || txtAccountID.Text.Trim() == "")
                {
                    combAccount.BackColor = Color.Pink;
                    combAccount.Focus();
                    return;
                }

               
                if (combPayKind.SelectedIndex == 0)
                {
                    if (btnSave.Enabled == true)
                    {
                        if (DGVPayType.Rows.Count == 0)
                        {
                            AddDgv_PayType(DGVPayType, combPayType.SelectedValue.ToString(), combPayType.Text, (double.Parse(txtNetInvoice.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), txtStatement.Text, combCurrency.SelectedValue.ToString(), combCurrency.Text, txtCurrencyRate.Text, txtNetInvoice.Text);
                            ClearPayType();
                            SumPayType();
                        }
                    }
                    if (double.Parse(txtpaid_Invoice.Text) < double.Parse(txtNetInvoice.Text))
                    {
                        MessageBox.Show(string.Format("{0}\n{1}\n", "المبلغ المدفوع اقل من صافي الفاتورة ", "في حالة نوع الدفع كاش لا يمكنك دفع اقل من صافي الفاتورة"), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                }



                txtMainID.Text = cls.MaxID_Sales_Main();
                //===InsertCustomers_trans============================================================================================================================================================================================================================================================
                ////===Treasury Movement_============================================================================================================================================================================================================================================================

                double paidInvoice = Convert.ToDouble(txtNetInvoice.Text);
                if (combPayKind.SelectedIndex == 1)
                {
                    paidInvoice = Convert.ToDouble(txtpaid_Invoice.Text);
                }
                TreasuryID = TreasuryMovement_CLS.MaxID_TreasuryMovement();
                TreasuryMovement_CLS.InsertTreasuryMovement(TreasuryID, txtAccountID.Text, "Cus", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value, "فاتورة مبيعات " + " : " + combAccount.Text, paidInvoice.ToString(), "0", this.Text + " : " + combPayKind.Text, txtNetInvoice.Text, paidInvoice.ToString(), UserID);
                //  TreasuryMovement_CLS.InsertTreasuryMovement(TreasuryID, txtAccountID.Text, "Cus", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value,  combAccount.Text,txtCurrencyID.Text,txtCurrencyRate.Text, paidInvoice.ToString(),(paidInvoice*Convert.ToDouble(txtCurrencyRate.Text)).ToString(), "0","0", this.Text + " : " + combPayKind.Text, txtNetInvoice.Text, (Convert.ToDouble(txtNetInvoice.Text) * Convert.ToDouble(txtCurrencyRate.Text)).ToString(), paidInvoice.ToString(), (Convert.ToDouble(paidInvoice) * Convert.ToDouble(txtCurrencyRate.Text)).ToString(), UserID,true);

                //====Bay_Main===========================================================================================================================================================================================================================================================


                cls.InsertSales_Main(txtMainID.Text, D1.Value, txtAccountID.Text, txtNote.Text, combPayKind.Text, Convert.ToDouble(txtTotalInvoice.Text), Convert.ToDouble(txtTotalDescound.Text), Convert.ToDouble(txtNetInvoice.Text), paidInvoice, Convert.ToDouble(txtRestInvoise.Text), "0", txtTotalBayPrice.Text, "0", (Convert.ToDouble(txtNetInvoice.Text) - Convert.ToDouble(txtTotalBayPrice.Text)).ToString(), TransID, TreasuryID, 0/* Vat % */, double.Parse(txtVatValue.Text), txtExtra.Text, Properties.Settings.Default.UserID);


                //====InsertSales_Details===========================================================================================================================================================================================================================================================


                //if (OpenNewBill == false)
                //{
                //    // dt_Prodecut.Clear();
                //    for (int i = 0; i < DGV1.Rows.Count; i++)
                //    {
                //        cls.UpdateSales_Details(DGV1.Rows[i].Cells["ID"].Value.ToString(), txtMainID.Text, true);
                //        AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(), false);
                //    }

                //}
                //else
                //{
                    //dt_Prodecut.Clear();
                    //for (int i = 0; i < DGV1.Rows.Count; i++)
                    //{
                    //    //DGV1.Rows[i].Cells["CurrencyID"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyVat"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyTotal"].Value.ToString()
                    //    cls.InsertSales_Details(txtMainID.Text, DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["Unit"].Value.ToString(), DGV1.Rows[i].Cells["UnitFactor"].Value.ToString(), DGV1.Rows[i].Cells["UnitOperating"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), "0", DGV1.Rows[i].Cells["BayPrice"].Value.ToString(), DGV1.Rows[i].Cells["TotalBayPrice"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["Vat"].Value.ToString(), DGV1.Rows[i].Cells["MainVat"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), "0", DGV1.Rows[i].Cells["StoreID"].Value.ToString(), true, DGV1.Rows[i].Cells["CurrencyID"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyVat"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyTotal"].Value.ToString());
                    //    AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(), false);
                    //}

                //}


                #region // Pay
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    string Statement = "عميل" + " / " + combAccount.Text;
                    if (!string.IsNullOrEmpty(DGVPayType.Rows[i].Cells["Statement"].Value.ToString()))
                    {
                        Statement = DGVPayType.Rows[i].Cells["Statement"].Value.ToString();
                    }
                    PayType_trans_cls.Insert_PayType_trans(txtMainID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtMainID.Text, this.Text, txtBillTypeID.Text, double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()), 0, Statement, Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
                }
                #endregion

                DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
                var invoice = db.Sales_Mains.SingleOrDefault(x => x.MainID == Convert.ToInt64(txtMainID.Text));
                invoice.DriverID = gridLookUpEdit1.EditValue.ToInt();
                db.SubmitChanges();
                SaveDetails();






                if (print == true)
                {
                    //Task.Run(() => PrintBill()); 
                    PrintBill(Convert.ToInt32(txtMainID.Text));

                }

                if (BillSetting_cls.ShowMessageSave == false)
                {
                    Mass.Saved();
                }

                if (combPayKind .SelectedIndex == 1 &&  XtraMessageBox.Show(text: "هل تريد انشاء ملف قسط لهذه الفاتوره", caption:"",MessageBoxButtons.YesNo )== DialogResult.Yes)
                {
                    var frm = new Forms.frm_Instalment(invoice.CustomerID.Value, Convert.ToInt32(invoice.MainID));
                    frm.Show();
                }
                btnNew_Click(null, null);

           
        }
        public static  void PrintBill(int ID)
        {

            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            var Invoice = (from h in db.Sales_Mains  
                           from p in db.Customers .Where(x => x.CustomerID  == h.CustomerID  ).DefaultIfEmpty()
                           from user in db.UserPermissions .Where(x => x.ID == h.UserAdd ).DefaultIfEmpty()
                           where h.MainID  == ID
                           select new
                           {
                               ID =h.MainID ,  
                               Customer = p.CustomerName ,
                               Date= h.MyDate ,
                               Notes= h.Note ,
                               DiscountValue=  h.Discount ,
                               Total=  h.TotalInvoice ,  
                               Net =  h.NetInvoice ,
                               NetText = "",
                               Paid = h.PaidInvoice ,
                               Remains = h.RestInvoise , 
                               p.Address,
                               Mobile=  p.Phone2 ,
                               p.Phone,
                               user.UserName,
                               Products = db.Sales_Details .Where(x => x.InvoiceID == h.MainID && x.InvoiceType == (int)MasterClass.InvoicesType.Sales).Select(x => new
                               {
                                   x.InvoiceID  ,
                                   Index = 0,
                                   x.ItemID, 
                                   Item ="["+ db.Prodecuts.Where(u => u.ProdecutID == x.ItemID ).SingleOrDefault().ProdecutID +"] "+ db.Prodecuts .Where(u => u.ProdecutID  == x.ItemID  ).SingleOrDefault().ProdecutName ,
                                   Unit ="" ,
                                   UnitID = x.ItemUnitID ,
                                   x.ItemQty ,
                                   x.Price,
                                   Discount  =x.Vat,
                                   x.TotalPrice ,
                               }).ToList()
                           }).ToList();

          


            Invoice.ForEach(x =>
            {
                x.Set(i => i.NetText, MasterClass.ConvertMoneyToText(x.Net.ToString()));
                int Index = 1;
                x.Products.ForEach(p =>
                {
                    p.Set(i => i.Index, Index++);
                    var product = db.Prodecuts.Single(pro => pro.ProdecutID == p.ItemID);
                    switch (p.UnitID)
                    {
                        case 1:   p.Set(i => i.Unit , product.FiestUnit);   break;
                        case 2: p.Set(i => i.Unit, product.SecoundUnit); break;
                        case 3: p.Set(i => i.Unit, product.ThreeUnit); break;
                    }
                });

            });
            RPT.rpt_SalesInvoice .Print(Invoice);

           
        }

       

        

        private void btnUpdate_Click(object sender, EventArgs e)
        {
         

                DataTable dt_Rsales = cls.Details_Sales_Main(txtMainID.Text);
                if (DataAccessLayer.CS_12 == false)
                {
                    MessageBox.Show("ليس لديك صلاحية", "صلاحيات ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (dt_Rsales.Rows.Count == 0)
                {
                    MessageBox.Show("رقم الفاتورة  غير موجود يرجي التأكد من رقم الفاتورة المدخل", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (Convert.ToDouble(dt_Rsales.Rows[0]["ReturnInvoise"].ToString()) > 0)
                {
                    MessageBox.Show("لا يمكنك التعديل  بسبب تم  عمل مرتجع علي الفاتورة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (GridView_Items.RowCount <= 0)
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (combAccount.Text.Trim() == "" || txtAccountID.Text.Trim() == "")
                {
                    combAccount.BackColor = Color.Pink;
                    combAccount.Focus();
                    return;
                }
               
                    if (GridView_Items.HasColumnErrors)
                    {
                        MessageBox.Show("من فضلك يرجي مراجعة الكميات المدخلة في الفاتورة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); 
                        return;
                    }

               

                if (combPayKind.SelectedIndex == 0)
                {
                    if (double.Parse(txtpaid_Invoice.Text) < double.Parse(txtNetInvoice.Text))
                    {
                        MessageBox.Show(string.Format("{0}\n{1}\n", "المبلغ المدفوع اقل من صافي الفاتورة ", "في حالة نوع الدفع كاش لا يمكنك دفع اقل من صافي الفاتورة"), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                }


                if (MessageBox.Show("سوف يتم التعديل ! : هل تريد الاستمرار", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    UpdateBill();
                    DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
                    var invoice = db.Sales_Mains.SingleOrDefault(x => x.MainID == Convert.ToInt64(txtMainID.Text));
                    invoice.DriverID = gridLookUpEdit1.EditValue.ToInt();
                    db.SubmitChanges();
                    SaveDetails();
                    btnNew_Click(null, null);
                }

 
        }



        private void UpdateBill()
        {
            //===Treasury Movement_============================================================================================================================================================================================================================================================


            Double paid = Convert.ToDouble(txtNetInvoice.Text);
            if (combPayKind.SelectedIndex != 0)
            {
                paid = Convert.ToDouble(txtpaid_Invoice.Text);
            }
            TreasuryMovement_CLS.UpdateTreasuryMovement(TreasuryID, txtAccountID.Text, "Cus", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value, "فاتورة مبيعات " + " : " + combAccount.Text, paid.ToString(), "0", this.Text + " : " + combPayKind.Text, txtNetInvoice.Text, paid.ToString());

            //====Update Suppliers_trans========================================================================================================================================================================================================================================================

            double paidInvoice = Convert.ToDouble(txtNetInvoice.Text);
            if (combPayKind.SelectedIndex == 1)
            {
                paidInvoice = Convert.ToDouble(txtpaid_Invoice.Text);
            }

            cls.UpdateSales_Main(txtMainID.Text, D1.Value, txtAccountID.Text, txtNote.Text, combPayKind.Text, Convert.ToDouble(txtTotalInvoice.Text), Convert.ToDouble(txtTotalDescound.Text), Convert.ToDouble(txtNetInvoice.Text), paidInvoice, Convert.ToDouble(txtRestInvoise.Text), txtTotalBayPrice.Text, (Convert.ToDouble(txtNetInvoice.Text) - Convert.ToDouble(txtTotalBayPrice.Text)).ToString(), TreasuryID,0/* vat %*/, double.Parse(txtVatValue.Text), txtExtra.Text);
            //====Delete Quentity ========================================================================================================================================================================================================================================================
            DataTable dt = cls.Details_Sales_Details(txtMainID.Text);


       
            var items = GridView_Items.DataSource as Collection<DAL.Sales_Detail>;
            items.ToList().ForEach(i =>
            {
                AVG_cls.QuantityNow_Avg(i.ItemID.ToString(), i.StoreID.ToString(), false);

            });




            #region // Pay
            PayType_trans_cls.Delete_PayType_trans(txtMainID.Text, txtBillTypeID.Text);
            for (int i = 0; i < DGVPayType.Rows.Count; i++)
            {
                string Statement = "عميل" + " / " + combAccount.Text;
                if (!string.IsNullOrEmpty(DGVPayType.Rows[i].Cells["Statement"].Value.ToString()))
                {
                    Statement = DGVPayType.Rows[i].Cells["Statement"].Value.ToString();
                }
                PayType_trans_cls.Insert_PayType_trans(txtMainID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtMainID.Text, this.Text, txtBillTypeID.Text, double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()), 0, Statement, Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
            }
            #endregion

            if (BillSetting_cls.ShowMessageSave == false)
            {
                Mass.Update();
            }
        }
        //======================================================================


       




       
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                BillSales_Search_frm frm = new BillSales_Search_frm();
                frm.Text = this.Text;
                frm.ShowDialog(this);
                if (frm.LoadData == false)
                {
                    return;
                }
                btnSave.Enabled = false;
                btnSave_Print.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
                DataTable dt = cls.Details_Sales_Main(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtMainID.Text = Dr["MainID"].ToString();
                DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
                var invoice = db.Sales_Mains.SingleOrDefault(x => x.MainID == Convert.ToInt64(Dr["MainID"]));
                if(invoice != null)
                {
                    gridLookUpEdit1.EditValue = invoice.DriverID;
                }
                D1.Value = Convert.ToDateTime(Dr["MyDate"]);
                txtAccountID.Text = Dr["CustomerID"].ToString();
                combAccount.Text = Dr["CustomerName"].ToString();
                txtPhone.Text = Dr["Phone"].ToString();
                txtNote.Text = Dr["Note"].ToString();
                combPayKind.Text = Dr["TypeKind"].ToString();
                //txtMainVat.Text = Dr["Vat"].ToString();
                txtTotalInvoice.Text = Dr["TotalInvoice"].ToString();
                txtTotalDescound.Text = Dr["Discount"].ToString();
                txtNetInvoice.Text = Dr["NetInvoice"].ToString();
                txtpaid_Invoice.Text = Dr["PaidInvoice"].ToString();
                txtRestInvoise.Text = Dr["RestInvoise"].ToString();

                TransID = Dr["TransID"].ToString();
                TreasuryID = Dr["TreasuryID"].ToString();

                txtExtra.Text = Dr["Extra"].ToString();


              
                DGVPayType.DataSource = null;
                DGVPayType.Rows.Clear();
                DataTable dt_paytype_trans = PayType_trans_cls.Details_PayType_trans(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString(), txtBillTypeID.Text);
                for (int i = 0; i < dt_paytype_trans.Rows.Count; i++)
                {
                    AddDgv_PayType(DGVPayType, dt_paytype_trans.Rows[i]["PayTypeID"].ToString(), dt_paytype_trans.Rows[i]["PayTypeName"].ToString(), dt_paytype_trans.Rows[i]["Debit"].ToString(), dt_paytype_trans.Rows[i]["Statement"].ToString(), dt_paytype_trans.Rows[i]["CurrencyID"].ToString(), dt_paytype_trans.Rows[i]["CurrencyName"].ToString(), dt_paytype_trans.Rows[i]["CurrencyRate"].ToString(), dt_paytype_trans.Rows[i]["CurrencyPrice"].ToString());
                }
                SumPayType();

                 



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

         
        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

      

        private void OrderProudect_frm_FormClosing(object sender, FormClosingEventArgs e)
        {
        
        }





        private void button3_Click(object sender, EventArgs e)
        {
            Customers_frm cus = new Customers_frm();
            cus.ShowDialog();

        }



        private void BalenceCustomer(string CustomerID)
        {
            try
            {
                txtCustomersBlanse.Text = "0";
                if (CustomerID.Trim() != "" || CustomerID != "System.Data.DataRowView")
                {
                    DataTable dt = TreasuryMovement_CLS.Balence_ISCusSupp(txtAccountID.Text, "Cus");//Cus_Trans_cls.Cus_Balence(dt_cus.Rows[i]["CustomerID"].ToString());
                    //  = Cus_Trans_cls.Cus_Balence(txtAccountID.Text);
                    txtCustomersBlanse.Text = "0";
                    if (dt.Rows[0]["balence"] == DBNull.Value == false)
                    {
                        txtCustomersBlanse.Text = dt.Rows[0]["balence"].ToString();
                    }
                    //=========================================================================================================
                    Customers_cls Customers_cls = new Customers_cls();
                    DataTable dt_cus = Customers_cls.Details_Customers(CustomerID);
                    txtPhone.Text = dt_cus.Rows[0]["Phone"].ToString();
                    if (dt_cus.Rows[0]["SalesLavel"].ToString() == "1")
                    {
                        SalesLevel1.Checked = true;
                        SalesLevel2.Checked = false;
                        SalesLevel3.Checked = false;
                    }
                    else if (dt_cus.Rows[0]["SalesLavel"].ToString() == "2")
                    {
                        SalesLevel1.Checked = false;
                        SalesLevel2.Checked = true;
                        SalesLevel3.Checked = false;
                    }
                    else if (dt_cus.Rows[0]["SalesLavel"].ToString() == "3")
                    {
                        SalesLevel1.Checked = false;
                        SalesLevel2.Checked = false;
                        SalesLevel3.Checked = true;
                    }

                }
            }
            catch
            {

            }

        }

        private void SalesLevel1_Click(object sender, EventArgs e)
        {
            SalesLevel2.Checked = false;
            SalesLevel3.Checked = false;
        }

        private void SalesLevel2_Click(object sender, EventArgs e)
        {
            SalesLevel1.Checked = false;
            SalesLevel3.Checked = false;
        }

        private void SalesLevel3_Click(object sender, EventArgs e)
        {
            SalesLevel1.Checked = false;
            SalesLevel2.Checked = false;
        }

     

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
           
        }

        private void txtChange(object sender, EventArgs e)
        {
            SumTotalTXT();
        }

        private void txtpaid_Invoice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Double paid = Convert.ToDouble(txtSumPayType.Text);
                txtRestInvoise.Text =Math.Abs( (Convert.ToDouble(txtNetInvoice.Text) - paid)).ToString();
            }
            catch
            {
                return;
            }
        }

        private void combPayKind_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }


       

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            //try
            //{
            if (DataAccessLayer.CS_13 == false)
            {
                MessageBox.Show("ليس لديك صلاحية", "صلاحيات ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (Mass.Delete() == true)
            {
                //====Delete Quentity ========================================================================================================================================================================================================================================================
                DataTable dt = cls.Details_Sales_Details(txtMainID.Text);


                // cls.Delete_Sales_Details(txtMainID.Text);
                TreasuryMovement_CLS.DeleteTreasuryMovement(TreasuryID);
                // Pay
                PayType_trans_cls.Delete_PayType_trans(txtMainID.Text, txtBillTypeID.Text);

                cls.Delete_BillStoreMain(txtMainID.Text);
             

                var items = GridView_Items.DataSource as Collection<DAL.Sales_Detail>;
                items.ToList().ForEach(i =>
                {
                    AVG_cls.QuantityNow_Avg(i.ItemID.ToString() ,i.StoreID.ToString(), false);

                });

                var db = new DBDataContext(Properties.Settings.Default.Connection_String);
                var invoiceDetails = ((Collection<DAL.Sales_Detail>)GridView_Items.DataSource);
                db.Inv_StoreLogs.DeleteAllOnSubmit(db.Inv_StoreLogs.Where(x => x.Type == (int)invoicesType && invoiceDetails.Select(d => d.ID).Contains(x.TypeID ?? 0)));
                db.Sales_Details.DeleteAllOnSubmit(db.Sales_Details.Where(x => x.InvoiceType == (int)invoicesType && x.InvoiceID == Convert.ToInt32(txtMainID.Text)));
                db.SubmitChanges();
                btnNew_Click(null, null);
            }
      
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            // Use Fast Inpout

            //===============================================================================================================================================================
            txtMainID.Text = cls.MaxID_Sales_Main();
            combPayKind.SelectedIndex = BillSetting_cls.kindPay;
            txtNote.Text = "";
            txtProdecutID.Text = "";
            txtTotalInvoice.Text = "0";

            txtExtra.Text = "0";
            D1.Value = DateTime.Now;
            
            gridLookUpEdit1.EditValue = null;
            if (BillSetting_cls.UseCustomerDefult == true)
            {
                Customers_cls customers_cls = new Customers_cls();
                DataTable dt_customer = customers_cls.Details_Customers(BillSetting_cls.CustomerID);
                if (dt_customer.Rows.Count > 0)
                {
                    txtAccountID.Text = dt_customer.Rows[0]["CustomerID"].ToString();
                    combAccount.Text = dt_customer.Rows[0]["CustomerName"].ToString();
                }
            }
            else
            {
                txtAccountID.Text = "";
                combAccount.Text = "";
            }


            // Customer Defult

            if (BillSetting_cls.UseCrrencyDefault == true)
            {
                Currency_cls Currency = new Currency_cls();
                DataTable DtCurrency = Currency.Details_Currency(BillSetting_cls.CurrencyID.ToString());
                combCurrency.Text = DtCurrency.Rows[0]["CurrencyName"].ToString();
                txtCurrencyRate.Text = DtCurrency.Rows[0]["CurrencyRate"].ToString();
            }
            else
            {
                combCurrency.Text = null;
                txtCurrencyRate.Text = "";

            }



            //=================================================================================

            // txtpaid_Invoice.Text = "0";
            TransID = "0";
            TreasuryID = "0";
            txtTotalDescound.Text = "0";
            // store defult ===================================================================================



            if (BillSetting_cls.UseStoreDefult == true)
            {
                Store_cls Store_cls = new Store_cls();
                DataTable dt_store = Store_cls.Details_Stores(int.Parse(BillSetting_cls.StoreID));
                txtStoreID.Text = dt_store.Rows[0]["StoreID"].ToString();
                txtStoreName.Text = dt_store.Rows[0]["StoreName"].ToString();
            }
            else
            {
                txtStoreID.Text = "";
                txtStoreName.Text = "";
            }

            btnSave.Enabled = true;
            btnSave_Print.Enabled = true;
            btnUpdate.Enabled = false;
            btnDelete.Enabled = false;
            ClearPayType();

            #region UsekindPay

            //===============================================================================================================================================================


            if (BillSetting_cls.UsekindPay == true)
            {
                PayType_cls PayType_cls = new PayType_cls();
                DataTable dtPayType = PayType_cls.Details_PayType(BillSetting_cls.PayTypeID);
                if (dtPayType.Rows.Count > 0)
                {
                    combPayType.Text = dtPayType.Rows[0]["PayTypeName"].ToString();
                }
            }
            else
            {
                combPayType.Text = null;
            }



            //===============================================================================================================================================================

            #endregion



            // txtRestInvoise.Text = "0";
            DGVPayType.DataSource = null;
            DGVPayType.Rows.Clear();
            GetData();
            txtSumPayType.Text = "0";
            txtpaid_Invoice.Text = "0";
            txtRestInvoise.Text = "0";

           

        }


        private void BillSales_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    //Close();
                }
                else if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                else if (e.KeyCode == Keys.F2)
                {
                    if (btnSave_Print.Enabled == true)
                    {
                        btnSave_Print_Click(null, null);
                    }
                }
                else if (e.KeyCode == Keys.F10)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                else if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                else if (e.Control == true && e.KeyCode == Keys.P)
                {
                    PrintBill(Convert.ToInt32(txtMainID.Text));

                }


                else if (e.KeyCode == Keys.F5)
                {
                    button5_Click(null, null);
                }
                else if (e.Shift == true && e.KeyCode == Keys.A)
                {
                    bunViewQuentityStore_frm_Click(null, null);
                }

 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }



        private void اعادةتحميلالاصنافToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void اضافةفاتورةفوريةToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BillSales_frm frm = new BillSales_frm();
            frm.BackColor = Color.Tomato;
            frm.Show();
        }

        private void CustomerTrsnse_Click(object sender, EventArgs e)
        {
            try
            {
                if (combAccount.Text.Trim() == "" || txtAccountID.Text.Trim() == "")
                {
                    combAccount.BackColor = Color.Pink;
                    button3_Click(null, null);
                    return;
                }

                CustomersTrans_Form frm = new CustomersTrans_Form();
                frm.txtAccountID.Text = txtAccountID.Text;
                frm.txtAccountName.Text = combAccount.Text;
                frm.AddEdit = "Bay";
                frm.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }




        private void CS_44_Click(object sender, EventArgs e)
        {

            MessageBox.Show((Convert.ToDouble(txtNetInvoice.Text) - Convert.ToDouble(txtTotalBayPrice.Text)).ToString(), "", MessageBoxButtons.OK);
        }

        private void combAccount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                txtAccountID.Text = "";
                combAccount.Text = "";
            }
        }

        private void txtStoreName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                txtStoreID.Text = "";
                txtStoreName.Text = "";
            }
        }

        private void combAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combAccount.BackColor = Color.White;
                txtAccountID.Text = combAccount.SelectedValue.ToString();

            }
            catch
            {
                txtAccountID.Text = "";
            }
        }

        private void combAccount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combAccount.BackColor = Color.White;
                if (combAccount.Text.Trim() == "")
                {
                    txtAccountID.Text = "";
                }
                else
                {
                    txtAccountID.Text = combAccount.SelectedValue.ToString();
                }
            }
            catch
            {
                txtAccountID.Text = "";
            }
        }

        private void txtAccountID_TextChanged(object sender, EventArgs e)
        {
            BalenceCustomer(txtAccountID.Text);
        }


        private void btnSave_Print_Click(object sender, EventArgs e)
        {
            SaveBill(true);
        }

        private void bunViewQuentityStore_frm_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtProdecutID.Text.Trim() == "")
                {
                    return;
                }
                ViewQuentityStore_frm frm = new ViewQuentityStore_frm();
                DataTable dt_Store = Prodecut_CLS.Prodecut_Store(txtProdecutID.Text);
                frm.DGV1.DataSource = null;
                frm.DGV1.Rows.Clear();
                for (int i = 0; i < dt_Store.Rows.Count; i++)
                {
                    string Change_All_Unit = ChangeUnit(dt_Store.Rows[i]["Balence"].ToString(), dt_Prodecut.Rows[0]["FiestUnit"].ToString(), dt_Prodecut.Rows[0]["FiestUnitFactor"].ToString(), dt_Prodecut.Rows[0]["SecoundUnit"].ToString(), dt_Prodecut.Rows[0]["SecoundUnitOperating"].ToString(), dt_Prodecut.Rows[0]["SecoundUnitFactor"].ToString(), dt_Prodecut.Rows[0]["ThreeUnit"].ToString(), dt_Prodecut.Rows[0]["ThreeUnitOperating"].ToString(), dt_Prodecut.Rows[0]["ThreeUnitFactor"].ToString());
                    AddDgv_QuentityStore(frm.DGV1, dt_Store.Rows[i]["ProdecutID"].ToString(), dt_Store.Rows[i]["StoreName"].ToString().ToString(), dt_Store.Rows[i]["Balence"].ToString(), Change_All_Unit);
                }

                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }


        private void AddDgv_QuentityStore(DataGridView dgv, string ProdecutID, string StoreName, string Balence, string ChangeAllUnit)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
                dgv.Rows[lastRows].Cells["StoreName"].Value = StoreName;
                dgv.Rows[lastRows].Cells["Balence"].Value = Balence;
                dgv.Rows[lastRows].Cells["Change_Unit"].Value = ChangeAllUnit;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private string ChangeUnit(string Quentity, string FiestUnit, string FiestUnitFactor, string SecoundUnit, string SecoundUnitOperating, string SecoundUnitFactor, string ThreeUnit, string ThreeUnitOperating, string ThreeUnitFactor)
        {
            if (Quentity == "" || Quentity == "0")
            {
                return "";
            }
            string Resalet = "";
            if (ThreeUnitOperating != "")
            {
                decimal n = Convert.ToDecimal(Quentity) / Convert.ToDecimal(ThreeUnitOperating);
                decimal H = Math.Floor(n);
                decimal b = Convert.ToDecimal(H) * Convert.ToDecimal(ThreeUnitOperating);
                decimal namber3 = Convert.ToDecimal(Quentity) - Convert.ToDecimal(b);
                decimal F = Convert.ToDecimal(H) / Convert.ToDecimal(SecoundUnitOperating);
                decimal namber1 = Math.Floor(F);
                decimal Fd = Convert.ToDecimal(namber1) * Convert.ToDecimal(SecoundUnitOperating);
                decimal namer2 = Convert.ToDecimal(H) - Convert.ToDecimal(Fd);
                Resalet = namber1 + "  " + FiestUnit + "  // " + namer2 + "  " + SecoundUnit + "  // " + namber3 + " " + ThreeUnit;
                //======================================================================================================================================================================================================================================================

            }

            else if (SecoundUnitOperating != "")
            {

                var y = Convert.ToDouble(Quentity) / Convert.ToDouble(SecoundUnitOperating);
                var Hnamber1 = Math.Floor(y);
                var dFd = Convert.ToDouble(Hnamber1) * Convert.ToDouble(SecoundUnitOperating);
                var hnamer2 = Convert.ToDouble(Quentity) - Convert.ToDouble(dFd);
                Resalet = Hnamber1 + " " + FiestUnit + "  // " + hnamer2 + " " + SecoundUnit;
                //======================================================================================================================================================================================================================================================

            }
            else if (FiestUnit != "")
            {
                Resalet = Quentity + "  " + FiestUnit;
                //======================================================================================================================================================================================================================================================
            }

            return Resalet;
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void combPayType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (combPayType.SelectedIndex >= 0)
                {
                    txtPayValue.Focus();
                }

            }
        }

        private void txtValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(txtPayValue.Text.Trim()))
                {
                    txtStatement.Focus();
                }

            }
        }

        private void ClearPayType()
        {
            combPayType.Text = null;
            txtPayValue.Text = "";
            txtStatement.Text = "";
            combPayType.Focus();
        }

        private void SumPayType()
        {
            try
            {

                double Sum = 0;
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    Sum += Convert.ToDouble(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString());
                }
                txtSumPayType.Text = Sum.ToString();
                txtpaid_Invoice.Text = txtSumPayType.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtStatement_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                if (e.KeyCode == Keys.Enter)
                {


                    if (combCurrency.SelectedIndex < 0)
                    {
                        MessageBox.Show("يرجي تحديد العملة", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        combCurrency.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                    {
                        MessageBox.Show("يرجي تحديد سعر التعادل", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCurrencyRate.Focus();
                        return;
                    }

                    if (combPayType.SelectedIndex < 0)
                    {
                        combPayType.BackColor = Color.Pink;
                        combPayType.Focus();
                        return;
                    }
                    if (txtPayValue.Text.Trim() == "")
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    if (double.Parse(txtPayValue.Text) == 0)
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    for (int i = 0; i < DGVPayType.Rows.Count; i++)
                    {
                        if (DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString() == combPayType.SelectedValue.ToString())
                        {
                            combPayType.BackColor = Color.Pink;
                            combPayType.Focus();
                            return;
                        }
                    }
                    AddDgv_PayType(DGVPayType, combPayType.SelectedValue.ToString(), combPayType.Text, (double.Parse(txtPayValue.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), txtStatement.Text, combCurrency.SelectedValue.ToString(), combCurrency.Text, txtCurrencyRate.Text, txtPayValue.Text);
                    ClearPayType();
                    SumPayType();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void AddDgv_PayType(DataGridView dgv, string PayTypeID, string PayTypeName, string PayValue, string Statement, string CurrencyID2, string CurrencyName2, string CurrencyRate2, string CurrencyPrice2)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["PayTypeID"].Value = PayTypeID;
                dgv.Rows[lastRows].Cells["PayTypeName"].Value = PayTypeName;
                dgv.Rows[lastRows].Cells["PayValue"].Value = PayValue;
                dgv.Rows[lastRows].Cells["Statement"].Value = Statement;

                dgv.Rows[lastRows].Cells["CurrencyID2"].Value = CurrencyID2;
                dgv.Rows[lastRows].Cells["CurrencyName2"].Value = CurrencyName2;
                dgv.Rows[lastRows].Cells["CurrencyRate2"].Value = CurrencyRate2;
                dgv.Rows[lastRows].Cells["CurrencyPrice2"].Value = CurrencyPrice2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void DGVPayType_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGVPayType.Rows.Count == 0)
                {
                    return;
                }
                double pay = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value.ToString());
                double Rat = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyRate2"].Value.ToString());
                DGVPayType.CurrentRow.Cells["PayValue"].Value = pay * Rat;
                SumPayType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGVPayType.SelectedRows.Count == 0)
                {
                    return;
                }
                if (Mass.Delete() == true)
                {
                    foreach (DataGridViewRow r in DGVPayType.SelectedRows)
                    {

                        DGVPayType.Rows.Remove(r);

                    }
                    SumPayType();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void combPayType_TextChanged(object sender, EventArgs e)
        {
            combPayType.BackColor = Color.White;

        }

        private void txtPayValue_TextChanged(object sender, EventArgs e)
        {
            txtPayValue.BackColor = Color.White;
        }

        private void txtPayValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            PrintBill(Convert.ToInt32(txtMainID.Text));

        }

        private void SumTotalTXT()
        {
            try
            {

                Double total = Convert.ToDouble(txtTotalInvoice.Text);
                Double Discount = 0;
                if (!string.IsNullOrEmpty(txtTotalDescound.Text))
                {
                    Discount = Convert.ToDouble(txtTotalDescound.Text);
                }

                Double Extra = 0;
                if (!string.IsNullOrEmpty(txtExtra.Text))
                {
                    Extra = Convert.ToDouble(txtExtra.Text);
                }

                txtNetInvoice.Text = (Convert.ToDouble(txtTotalInvoice.Text) + Extra - Discount).ToString();
                Double paid = Convert.ToDouble(txtSumPayType.Text);
                txtpaid_Invoice.Text = paid.ToString();
                txtRestInvoise.Text =Math.Abs( (Convert.ToDouble(txtNetInvoice.Text) - paid)).ToString();

                //if (btnSave.Enabled)
                //{
                //    if (!string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                //    {
                //        txtPayValue.Text = (double.Parse(txtNetInvoice.Text) / double.Parse(txtCurrencyRate.Text)).ToString();

                //    }
                //    else
                //    {
                //        txtPayValue.Text = txtNetInvoice.Text;
                //    }
                //}


            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message.ToString());
                // return;
            }

        }

        private void combAccount_KeyDown_1(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (string.IsNullOrEmpty(combAccount.Text.Trim()))
                    {
                        return;
                    }
                    Customers_cls cls = new Customers_cls();
                    DataTable dt = cls.Search_Customers(combAccount.Text);
                    if (dt.Rows.Count > 0)
                    {
                        combAccount.Text = dt.Rows[0]["CustomerName"].ToString();
                    }
                    else
                    {

                        Customers_frm cus = new Customers_frm();
                        cus.txtPhone.Text = combAccount.Text; //txtCustomerName.Text = "";
                        cus.ClearData = false;
                        cus.ShowDialog(this);
                        if (cus.SaveData == true)
                        {
                            combAccount.Text = cus.txtCustomerName.Text;
                        }

                    }
                }
            }
            catch
            {


            }
        }


        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void combCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (combCurrency.SelectedIndex > -1)
                {
                    Currency_cls crrcls = new Currency_cls();
                    DataTable dt = crrcls.Details_Currency(combCurrency.SelectedValue.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        txtCurrencyRate.Text = dt.Rows[0]["CurrencyRate"].ToString();
                    }
                }
            }
            catch
            {

                return;
            }
        }

        private void txtNetInvoice_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCurrencyRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }
        OfferPrice_cls offerPrice_cls = new OfferPrice_cls();
        private void تحميلالاصنافمنفاتورةعرضسعرToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {

                BillOfferPricer_Search_frm frm = new BillOfferPricer_Search_frm();
                frm.Icon = this.Icon;
                frm.ShowDialog(this);
                if (frm.LoadData == false)
                {
                    return;
                }

                #region MyRegion








                TransID = "0";
                TreasuryID = "0";
                txtTotalDescound.Text = "0";
                //DGV1.DataSource = null;
                //DGV1.Rows.Clear();
                //lblCount.Text = DGV1.Rows.Count.ToString();
                //===============================================================================================================================================================
                btnSave.Enabled = true;
                btnSave_Print.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                ClearPayType();

                txtSumPayType.Text = "0";
                DGVPayType.DataSource = null;
                DGVPayType.Rows.Clear();
               // cleartxt();
                #endregion


                ////=========================================================================================================================================================================
                //dt.Clear();
                //DGV1.DataSource = null;
                //DGV1.Rows.Clear();
                //=========================================================================================================================================================================
                DataTable dt = offerPrice_cls.Details_OfferPrice_Detalis(long.Parse(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString()));
                //DGV1.AutoGenerateColumns = false;
                //DGV1.DataSource = dt;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataTable dt_Prod = Prodecut_CLS.SearchBill_Prodecuts(dt.Rows[i]["ProdecutName"].ToString(), txtStoreID.Text);
                    string bayprice = (Convert.ToDouble(dt_Prod.Rows[0]["ProdecutAvg"].ToString()) * Convert.ToDouble(dt.Rows[i]["UnitFactor"].ToString())).ToString();
                    //AddRowDgv(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["UnitFactor"].ToString(), dt.Rows[i]["UnitOperating"].ToString(), dt.Rows[i]["Quantity"].ToString(), "0", bayprice, (double.Parse(bayprice) * double.Parse(dt.Rows[i]["Quantity"].ToString())).ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["MainVat"].ToString(), dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreID"].ToString(), dt.Rows[i]["StoreName"].ToString(), combCurrency.SelectedValue.ToString(), txtCurrencyRate.Text, combCurrency.Text);

                    //AddRowDgv(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["UnitFactor"].ToString(), dt.Rows[i]["UnitOperating"].ToString(), dt.Rows[i]["Quantity"].ToString(), combCurrency.SelectedValue.ToString(), txtCurrencyRate.Text, (double.Parse(dt.Rows[i]["Price"].ToString()) * double.Parse(txtCurrencyRate.Text)).ToString(), (double.Parse(dt.Rows[i]["TotalPrice"].ToString()) * double.Parse(txtCurrencyRate.Text)).ToString(), combCurrency.Text, txtCurrencyRate.Text, dt.Rows[i]["Price"].ToString(), dt.Rows[i]["Vat"].ToString(), dt.Rows[i]["MainVat"].ToString(), dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreID"].ToString(), dt.Rows[i]["StoreName"].ToString());


                }
                OpenNewBill = true; 










            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void gridLookUpEdit1_Properties_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if(e.Button. Kind == ButtonPredefines.Delete)
            {
                (sender as GridLookUpEdit).EditValue = null;
            }
        }

        private void gridLookUpEdit1_EditValueChanged(object sender, EventArgs e)
        {
            var glkp = sender as GridLookUpEdit;
            if(glkp.EditValue.ValidAsIntNonZero())
            {
                combPayKind.SelectedIndex = 1;
                combPayKind.Enabled = false;
            }else
            { 
                combPayKind.Enabled = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DGVPayType.Rows.Clear();
            if (combPayType.SelectedIndex < 0)
                combPayType.SelectedIndex = 0;
            txtPayValue.Text = txtNetInvoice.Text;
            txtStatement_KeyDown(txtStatement, new KeyEventArgs(Keys.Enter));

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (combPayType.SelectedIndex < 0)
                combPayType.SelectedIndex = 0;
            txtStatement_KeyDown(txtStatement, new KeyEventArgs(Keys.Enter));

        }

        private void txtMainID_TextChanged(object sender, EventArgs e)
        {
            GetData();
        }
    }
}
