﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class Category_frm : Form
    {
        public Category_frm()
        {
            InitializeComponent();
        }
        Category_cls cls = new Category_cls();
        public Boolean LoadData = false;
        private void AccountEndAdd_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);

        }


        private void AccountEndAdd_Form_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
          
        }

        private void txtAccountName_TextChanged(object sender, EventArgs e)
        {
            txtGroupName.BackColor = Color.White;
        }

        private void txtCurrencyVal_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

  

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

              
                //"Conditional"
                if (txtGroupName.Text.Trim() == "")
                {
                    txtGroupName.BackColor = Color.Pink;
                    txtGroupName.Focus();
                    return;
                }

                // " Search TextBox "
                DataTable DtSearch = cls.NameSearch_Category(txtGroupName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtGroupName.BackColor = Color.Pink;
                    txtGroupName.Focus();
                    return;
                }

                txtID.Text = cls.MaxID_Category();
                 cls.Insert_Category(txtID.Text, txtGroupName.Text.Trim(),txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // "Conditional"
                if (txtGroupName.Text.Trim() == "")
                {
                    txtGroupName.BackColor = Color.Pink;
                    txtGroupName.Focus();
                    return;
                }

                //"Conditional"
             


                cls.Update_Category(txtID.Text, txtGroupName.Text.Trim(), txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                                // fill combBox combParent
            
                //=============================================================================================

                txtID.Text = cls.MaxID_Category();
                txtGroupName.Text = "";
                txtRemark.Text ="";
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                txtGroupName.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {





                #region " Search TextBox "


                if (cls.NoDelete_Prodecuts(txtID.Text).Rows.Count > 0)
                {
                    Mass.NoDelete();
                    return;
                }

                DataTable DtItem = cls.Search_Category_Delete( txtID.Text);
                if (DtItem.Rows.Count > 0)
                {
                    MessageBox.Show("لا يمكنك حذف مجموعة يوجد بداخلها اصناف ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                #endregion

                if (Mass.Delete() == true)
                {
                    cls.Delete_Category( txtID.Text);
                    btnNew_Click(null, null); 
                    LoadData = true;
                }  
             
            
            



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
           

            try
            {
                CategorySearch_frm frm = new CategorySearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata==false)
                {
                    return;
                }

                DataTable dt = cls.Details_Category( frm.DGV1.CurrentRow.Cells["CategoryID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtID.Text = Dr["CategoryID"].ToString();
                txtGroupName.Text = Dr["CategoryName"].ToString();

                txtRemark.Text =Dr["Remark"].ToString();
     
                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

     

      



  

       












    }
}
