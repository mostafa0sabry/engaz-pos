﻿namespace ByStro.PL
{
    partial class BillSalesR_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BillSalesR_frm));
            this.txtMainID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.D1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.DGV1 = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProdecutID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProdecutName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnCurrencyVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnCurrencyTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReturnTotalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StoreName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StoreID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitOperating = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BayPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalBayPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MainVat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtBillTypeID = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnNew = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.txtAccountID = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtRestInvoise = new System.Windows.Forms.TextBox();
            this.txtpaid_Invoice = new System.Windows.Forms.TextBox();
            this.txtNetInvoice = new System.Windows.Forms.TextBox();
            this.txtTotalDescound = new System.Windows.Forms.TextBox();
            this.txtTotalInvoice = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.combPayKind = new System.Windows.Forms.ComboBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblQTY = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtReturnTotalBayPrice = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button5 = new System.Windows.Forms.Button();
            this.txtVatValue = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtSumPayType = new System.Windows.Forms.ToolStripStatusLabel();
            this.DGVPayType = new System.Windows.Forms.DataGridView();
            this.PayTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayTypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyPrice2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Statement = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyID2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrencyRate2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PayValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtStatement = new System.Windows.Forms.TextBox();
            this.txtPayValue = new System.Windows.Forms.TextBox();
            this.combPayType = new System.Windows.Forms.ComboBox();
            this.txtCustomerName = new System.Windows.Forms.TextBox();
            this.txtTotalProfits = new System.Windows.Forms.TextBox();
            this.txtTotalBayPrice = new System.Windows.Forms.TextBox();
            this.txtMainNetInvoice = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCurrencyRate = new System.Windows.Forms.TextBox();
            this.combCurrency = new System.Windows.Forms.ComboBox();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPayType)).BeginInit();
            this.SuspendLayout();
            // 
            // txtMainID
            // 
            this.txtMainID.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtMainID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMainID.Location = new System.Drawing.Point(92, 55);
            this.txtMainID.Name = "txtMainID";
            this.txtMainID.ReadOnly = true;
            this.txtMainID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtMainID.Size = new System.Drawing.Size(310, 24);
            this.txtMainID.TabIndex = 606;
            this.txtMainID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMainID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllKeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 17);
            this.label4.TabIndex = 605;
            this.label4.Text = "رقم السند :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 604;
            this.label2.Text = "البيـــان :";
            // 
            // txtNote
            // 
            this.txtNote.BackColor = System.Drawing.Color.White;
            this.txtNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNote.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNote.Location = new System.Drawing.Point(92, 110);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(839, 23);
            this.txtNote.TabIndex = 601;
            // 
            // D1
            // 
            this.D1.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.D1.CustomFormat = "dd/MM/yyyy";
            this.D1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.D1.Location = new System.Drawing.Point(92, 28);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(154, 24);
            this.D1.TabIndex = 603;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 602;
            this.label1.Text = "تاريخ السند :";
            // 
            // DGV1
            // 
            this.DGV1.AllowUserToAddRows = false;
            this.DGV1.AllowUserToDeleteRows = false;
            this.DGV1.AllowUserToResizeColumns = false;
            this.DGV1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FloralWhite;
            this.DGV1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(146)))), ((int)(((byte)(222)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGV1.ColumnHeadersHeight = 24;
            this.DGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGV1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.ProdecutID,
            this.ProdecutName,
            this.Unit,
            this.Quantity,
            this.ReturnQuantity,
            this.CurrencyPrice,
            this.ReturnCurrencyVat,
            this.ReturnCurrencyTotal,
            this.ReturnVat,
            this.ReturnTotalPrice,
            this.StoreName,
            this.StoreID,
            this.UnitFactor,
            this.UnitOperating,
            this.BayPrice,
            this.TotalBayPrice,
            this.MainVat,
            this.CurrencyName,
            this.CurrencyRate});
            this.DGV1.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.DefaultCellStyle = dataGridViewCellStyle4;
            this.DGV1.EnableHeadersVisualStyles = false;
            this.DGV1.GridColor = System.Drawing.Color.Silver;
            this.DGV1.Location = new System.Drawing.Point(2, 6);
            this.DGV1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DGV1.MultiSelect = false;
            this.DGV1.Name = "DGV1";
            this.DGV1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DGV1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DGV1.RowHeadersVisible = false;
            this.DGV1.RowHeadersWidth = 55;
            this.DGV1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.DGV1.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.DGV1.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGV1.RowTemplate.Height = 23;
            this.DGV1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV1.Size = new System.Drawing.Size(779, 313);
            this.DGV1.TabIndex = 609;
            this.DGV1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV1_CellEndEdit);
            this.DGV1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.DGV1_EditingControlShowing);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // ProdecutID
            // 
            this.ProdecutID.DataPropertyName = "ProdecutID";
            this.ProdecutID.HeaderText = "كود الصنف";
            this.ProdecutID.Name = "ProdecutID";
            this.ProdecutID.ReadOnly = true;
            this.ProdecutID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ProdecutID.Visible = false;
            // 
            // ProdecutName
            // 
            this.ProdecutName.DataPropertyName = "ProdecutName";
            this.ProdecutName.FillWeight = 256.7848F;
            this.ProdecutName.HeaderText = "اسم الصنف";
            this.ProdecutName.Name = "ProdecutName";
            this.ProdecutName.ReadOnly = true;
            this.ProdecutName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ProdecutName.Width = 303;
            // 
            // Unit
            // 
            this.Unit.DataPropertyName = "Unit";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Unit.DefaultCellStyle = dataGridViewCellStyle3;
            this.Unit.FillWeight = 96.3198F;
            this.Unit.HeaderText = "الوحدة";
            this.Unit.Name = "Unit";
            this.Unit.ReadOnly = true;
            this.Unit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Unit.Width = 116;
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "Quantity";
            this.Quantity.FillWeight = 79.34152F;
            this.Quantity.HeaderText = "الكمية";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Quantity.Width = 90;
            // 
            // ReturnQuantity
            // 
            this.ReturnQuantity.DataPropertyName = "ReturnQuantity";
            this.ReturnQuantity.HeaderText = "كمية المرتجع";
            this.ReturnQuantity.Name = "ReturnQuantity";
            // 
            // CurrencyPrice
            // 
            this.CurrencyPrice.DataPropertyName = "CurrencyPrice";
            this.CurrencyPrice.FillWeight = 116.319F;
            this.CurrencyPrice.HeaderText = "سعر الوحدة";
            this.CurrencyPrice.Name = "CurrencyPrice";
            this.CurrencyPrice.ReadOnly = true;
            this.CurrencyPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CurrencyPrice.Width = 101;
            // 
            // ReturnCurrencyVat
            // 
            this.ReturnCurrencyVat.DataPropertyName = "ReturnCurrencyVat";
            this.ReturnCurrencyVat.HeaderText = "Vat %";
            this.ReturnCurrencyVat.Name = "ReturnCurrencyVat";
            this.ReturnCurrencyVat.ReadOnly = true;
            this.ReturnCurrencyVat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ReturnCurrencyTotal
            // 
            this.ReturnCurrencyTotal.DataPropertyName = "ReturnCurrencyTotal";
            this.ReturnCurrencyTotal.HeaderText = "اجمالي المرتجع بالعملة";
            this.ReturnCurrencyTotal.Name = "ReturnCurrencyTotal";
            this.ReturnCurrencyTotal.ReadOnly = true;
            this.ReturnCurrencyTotal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ReturnCurrencyTotal.Width = 150;
            // 
            // ReturnVat
            // 
            this.ReturnVat.DataPropertyName = "ReturnVat";
            this.ReturnVat.HeaderText = "VAT";
            this.ReturnVat.Name = "ReturnVat";
            this.ReturnVat.ReadOnly = true;
            this.ReturnVat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ReturnVat.Width = 80;
            // 
            // ReturnTotalPrice
            // 
            this.ReturnTotalPrice.DataPropertyName = "ReturnTotalPrice";
            this.ReturnTotalPrice.HeaderText = "أجمالي المرتجع";
            this.ReturnTotalPrice.Name = "ReturnTotalPrice";
            this.ReturnTotalPrice.ReadOnly = true;
            this.ReturnTotalPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ReturnTotalPrice.Visible = false;
            this.ReturnTotalPrice.Width = 200;
            // 
            // StoreName
            // 
            this.StoreName.DataPropertyName = "StoreName";
            this.StoreName.HeaderText = "المخزن";
            this.StoreName.Name = "StoreName";
            this.StoreName.ReadOnly = true;
            this.StoreName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.StoreName.Width = 150;
            // 
            // StoreID
            // 
            this.StoreID.DataPropertyName = "StoreID";
            this.StoreID.HeaderText = "StoreID";
            this.StoreID.Name = "StoreID";
            this.StoreID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.StoreID.Visible = false;
            // 
            // UnitFactor
            // 
            this.UnitFactor.DataPropertyName = "UnitFactor";
            this.UnitFactor.HeaderText = "UnitFactor";
            this.UnitFactor.Name = "UnitFactor";
            this.UnitFactor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UnitFactor.Visible = false;
            // 
            // UnitOperating
            // 
            this.UnitOperating.DataPropertyName = "UnitOperating";
            this.UnitOperating.HeaderText = "UnitOperating";
            this.UnitOperating.Name = "UnitOperating";
            this.UnitOperating.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UnitOperating.Visible = false;
            // 
            // BayPrice
            // 
            this.BayPrice.DataPropertyName = "BayPrice";
            this.BayPrice.HeaderText = "سعر التكلفة";
            this.BayPrice.Name = "BayPrice";
            this.BayPrice.ReadOnly = true;
            this.BayPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.BayPrice.Visible = false;
            // 
            // TotalBayPrice
            // 
            this.TotalBayPrice.DataPropertyName = "TotalBayPrice";
            this.TotalBayPrice.HeaderText = "اجمالي سعر التكلفة";
            this.TotalBayPrice.Name = "TotalBayPrice";
            this.TotalBayPrice.ReadOnly = true;
            this.TotalBayPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TotalBayPrice.Visible = false;
            // 
            // MainVat
            // 
            this.MainVat.DataPropertyName = "MainVat";
            this.MainVat.HeaderText = "الضريبة %";
            this.MainVat.Name = "MainVat";
            this.MainVat.ReadOnly = true;
            this.MainVat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MainVat.Width = 80;
            // 
            // CurrencyName
            // 
            this.CurrencyName.DataPropertyName = "CurrencyName";
            this.CurrencyName.HeaderText = "العملة";
            this.CurrencyName.Name = "CurrencyName";
            this.CurrencyName.ReadOnly = true;
            this.CurrencyName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CurrencyRate
            // 
            this.CurrencyRate.DataPropertyName = "CurrencyRate";
            this.CurrencyRate.HeaderText = "سعر التعادل";
            this.CurrencyRate.Name = "CurrencyRate";
            this.CurrencyRate.ReadOnly = true;
            this.CurrencyRate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // txtBillTypeID
            // 
            this.txtBillTypeID.Location = new System.Drawing.Point(874, 79);
            this.txtBillTypeID.Name = "txtBillTypeID";
            this.txtBillTypeID.ReadOnly = true;
            this.txtBillTypeID.Size = new System.Drawing.Size(10, 24);
            this.txtBillTypeID.TabIndex = 659;
            this.txtBillTypeID.Text = "RSales";
            this.txtBillTypeID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBillTypeID.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 17);
            this.label9.TabIndex = 687;
            this.label9.Text = "العميل :";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNew,
            this.btnSave,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(935, 27);
            this.toolStrip1.TabIndex = 690;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnNew
            // 
            this.btnNew.Image = global::ByStro.Properties.Resources.New;
            this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(65, 24);
            this.btnNew.Text = "جديد ";
            this.btnNew.ToolTipText = "جديد Ctrl + N ";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = global::ByStro.Properties.Resources.Save_and_New;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(77, 24);
            this.btnSave.Text = "حفظ F2";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtAccountID
            // 
            this.txtAccountID.Location = new System.Drawing.Point(874, 79);
            this.txtAccountID.Name = "txtAccountID";
            this.txtAccountID.Size = new System.Drawing.Size(10, 24);
            this.txtAccountID.TabIndex = 691;
            this.txtAccountID.Visible = false;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.BackColor = System.Drawing.SystemColors.Control;
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label19.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(793, 203);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(137, 24);
            this.label19.TabIndex = 767;
            this.label19.Text = "خصم";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRestInvoise
            // 
            this.txtRestInvoise.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRestInvoise.BackColor = System.Drawing.Color.White;
            this.txtRestInvoise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRestInvoise.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtRestInvoise.ForeColor = System.Drawing.Color.Blue;
            this.txtRestInvoise.Location = new System.Drawing.Point(793, 478);
            this.txtRestInvoise.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtRestInvoise.Name = "txtRestInvoise";
            this.txtRestInvoise.ReadOnly = true;
            this.txtRestInvoise.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtRestInvoise.Size = new System.Drawing.Size(137, 24);
            this.txtRestInvoise.TabIndex = 765;
            this.txtRestInvoise.Text = "0.00";
            this.txtRestInvoise.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtpaid_Invoice
            // 
            this.txtpaid_Invoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtpaid_Invoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtpaid_Invoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtpaid_Invoice.ForeColor = System.Drawing.Color.Green;
            this.txtpaid_Invoice.Location = new System.Drawing.Point(793, 428);
            this.txtpaid_Invoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtpaid_Invoice.Name = "txtpaid_Invoice";
            this.txtpaid_Invoice.ReadOnly = true;
            this.txtpaid_Invoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtpaid_Invoice.Size = new System.Drawing.Size(137, 24);
            this.txtpaid_Invoice.TabIndex = 766;
            this.txtpaid_Invoice.Text = "0";
            this.txtpaid_Invoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtpaid_Invoice.TextChanged += new System.EventHandler(this.txtpaid_Invoice_TextChanged);
            // 
            // txtNetInvoice
            // 
            this.txtNetInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNetInvoice.BackColor = System.Drawing.Color.White;
            this.txtNetInvoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNetInvoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtNetInvoice.ForeColor = System.Drawing.Color.Red;
            this.txtNetInvoice.Location = new System.Drawing.Point(793, 328);
            this.txtNetInvoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNetInvoice.Name = "txtNetInvoice";
            this.txtNetInvoice.ReadOnly = true;
            this.txtNetInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtNetInvoice.Size = new System.Drawing.Size(137, 24);
            this.txtNetInvoice.TabIndex = 764;
            this.txtNetInvoice.Text = "0.00";
            this.txtNetInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTotalDescound
            // 
            this.txtTotalDescound.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalDescound.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalDescound.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtTotalDescound.Location = new System.Drawing.Point(793, 228);
            this.txtTotalDescound.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalDescound.Name = "txtTotalDescound";
            this.txtTotalDescound.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTotalDescound.Size = new System.Drawing.Size(137, 24);
            this.txtTotalDescound.TabIndex = 760;
            this.txtTotalDescound.Text = "0";
            this.txtTotalDescound.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip1.SetToolTip(this.txtTotalDescound, "خصم مسموح بة");
            this.txtTotalDescound.TextChanged += new System.EventHandler(this.txtChange);
            // 
            // txtTotalInvoice
            // 
            this.txtTotalInvoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalInvoice.BackColor = System.Drawing.Color.White;
            this.txtTotalInvoice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalInvoice.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalInvoice.Location = new System.Drawing.Point(793, 178);
            this.txtTotalInvoice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtTotalInvoice.Name = "txtTotalInvoice";
            this.txtTotalInvoice.ReadOnly = true;
            this.txtTotalInvoice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTotalInvoice.Size = new System.Drawing.Size(137, 24);
            this.txtTotalInvoice.TabIndex = 759;
            this.txtTotalInvoice.Text = "0.00";
            this.txtTotalInvoice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTotalInvoice.TextChanged += new System.EventHandler(this.txtChange);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(793, 353);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(137, 24);
            this.label7.TabIndex = 762;
            this.label7.Text = "نوع الدفع";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.BackColor = System.Drawing.SystemColors.Control;
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label24.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(793, 453);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label24.Size = new System.Drawing.Size(137, 24);
            this.label24.TabIndex = 755;
            this.label24.Text = "المبلغ الباقي";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.BackColor = System.Drawing.SystemColors.Control;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(793, 303);
            this.label15.Name = "label15";
            this.label15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label15.Size = new System.Drawing.Size(137, 24);
            this.label15.TabIndex = 758;
            this.label15.Text = "صافي المرتجع";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.BackColor = System.Drawing.SystemColors.Control;
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label18.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(793, 403);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(137, 24);
            this.label18.TabIndex = 757;
            this.label18.Text = "المبلغ المسدد";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.BackColor = System.Drawing.SystemColors.Control;
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label20.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(793, 153);
            this.label20.Name = "label20";
            this.label20.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label20.Size = new System.Drawing.Size(137, 24);
            this.label20.TabIndex = 754;
            this.label20.Text = "اجمالي المرتجع";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // combPayKind
            // 
            this.combPayKind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.combPayKind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combPayKind.FormattingEnabled = true;
            this.combPayKind.Items.AddRange(new object[] {
            "نقدي",
            "أجل"});
            this.combPayKind.Location = new System.Drawing.Point(793, 378);
            this.combPayKind.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.combPayKind.Name = "combPayKind";
            this.combPayKind.Size = new System.Drawing.Size(137, 24);
            this.combPayKind.TabIndex = 763;
            this.combPayKind.SelectedIndexChanged += new System.EventHandler(this.combPayKind_SelectedIndexChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblCount,
            this.toolStripStatusLabel3,
            this.lblQTY});
            this.statusStrip1.Location = new System.Drawing.Point(3, 320);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(777, 22);
            this.statusStrip1.TabIndex = 768;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(88, 17);
            this.toolStripStatusLabel1.Text = "عدد الاصناف :";
            // 
            // lblCount
            // 
            this.lblCount.BackColor = System.Drawing.Color.Transparent;
            this.lblCount.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.ForeColor = System.Drawing.Color.Red;
            this.lblCount.Name = "lblCount";
            this.lblCount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCount.Size = new System.Drawing.Size(17, 17);
            this.lblCount.Text = "0";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(87, 17);
            this.toolStripStatusLabel3.Text = "عدد الكميات :";
            // 
            // lblQTY
            // 
            this.lblQTY.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQTY.ForeColor = System.Drawing.Color.Red;
            this.lblQTY.Name = "lblQTY";
            this.lblQTY.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblQTY.Size = new System.Drawing.Size(17, 17);
            this.lblQTY.Text = "0";
            // 
            // txtReturnTotalBayPrice
            // 
            this.txtReturnTotalBayPrice.Location = new System.Drawing.Point(874, 79);
            this.txtReturnTotalBayPrice.Name = "txtReturnTotalBayPrice";
            this.txtReturnTotalBayPrice.ReadOnly = true;
            this.txtReturnTotalBayPrice.Size = new System.Drawing.Size(10, 24);
            this.txtReturnTotalBayPrice.TabIndex = 776;
            this.txtReturnTotalBayPrice.Visible = false;
            // 
            // button5
            // 
            this.button5.BackgroundImage = global::ByStro.Properties.Resources.Search_20;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(408, 54);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(29, 25);
            this.button5.TabIndex = 607;
            this.toolTip1.SetToolTip(this.button5, "البحث في الفواتير السابقة F5");
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // txtVatValue
            // 
            this.txtVatValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVatValue.BackColor = System.Drawing.Color.White;
            this.txtVatValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVatValue.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtVatValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.txtVatValue.Location = new System.Drawing.Point(793, 278);
            this.txtVatValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVatValue.Name = "txtVatValue";
            this.txtVatValue.ReadOnly = true;
            this.txtVatValue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtVatValue.Size = new System.Drawing.Size(137, 24);
            this.txtVatValue.TabIndex = 780;
            this.txtVatValue.Text = "0";
            this.txtVatValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.BackColor = System.Drawing.SystemColors.Control;
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label16.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(793, 253);
            this.label16.Name = "label16";
            this.label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label16.Size = new System.Drawing.Size(137, 24);
            this.label16.TabIndex = 779;
            this.label16.Text = "اجمالي الضريبة";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 133);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(791, 374);
            this.tabControl1.TabIndex = 782;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.DGV1);
            this.tabPage1.Controls.Add(this.statusStrip1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(783, 345);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "الاصناف";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(783, 345);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "دفع للعميل F4";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.statusStrip2);
            this.groupBox2.Controls.Add(this.DGVPayType);
            this.groupBox2.Controls.Add(this.txtStatement);
            this.groupBox2.Controls.Add(this.txtPayValue);
            this.groupBox2.Controls.Add(this.combPayType);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(3, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(777, 336);
            this.groupBox2.TabIndex = 781;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "انواع الدفع ";
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label23.Location = new System.Drawing.Point(519, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(255, 24);
            this.label23.TabIndex = 788;
            this.label23.Text = "نوع الدفع";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label22.Location = new System.Drawing.Point(386, 21);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(132, 24);
            this.label22.TabIndex = 787;
            this.label22.Text = "المبلغ";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label25.Location = new System.Drawing.Point(3, 21);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(382, 24);
            this.label25.TabIndex = 786;
            this.label25.Text = "البيان";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip2
            // 
            this.statusStrip2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripStatusLabel2,
            this.txtSumPayType});
            this.statusStrip2.Location = new System.Drawing.Point(3, 307);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(771, 26);
            this.statusStrip2.TabIndex = 785;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::ByStro.Properties.Resources.Deletex;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(118, 24);
            this.toolStripButton1.Text = "حذف  المحدد  ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(69, 21);
            this.toolStripStatusLabel2.Text = "الاجمالي :";
            // 
            // txtSumPayType
            // 
            this.txtSumPayType.BackColor = System.Drawing.Color.Transparent;
            this.txtSumPayType.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSumPayType.ForeColor = System.Drawing.Color.Red;
            this.txtSumPayType.Name = "txtSumPayType";
            this.txtSumPayType.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSumPayType.Size = new System.Drawing.Size(17, 21);
            this.txtSumPayType.Text = "0";
            // 
            // DGVPayType
            // 
            this.DGVPayType.AllowUserToAddRows = false;
            this.DGVPayType.AllowUserToDeleteRows = false;
            this.DGVPayType.AllowUserToResizeColumns = false;
            this.DGVPayType.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FloralWhite;
            this.DGVPayType.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.DGVPayType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGVPayType.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(146)))), ((int)(((byte)(222)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVPayType.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.DGVPayType.ColumnHeadersHeight = 24;
            this.DGVPayType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGVPayType.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PayTypeName,
            this.PayTypeID,
            this.CurrencyPrice2,
            this.Statement,
            this.CurrencyID2,
            this.CurrencyName2,
            this.CurrencyRate2,
            this.PayValue});
            this.DGVPayType.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.DefaultCellStyle = dataGridViewCellStyle9;
            this.DGVPayType.EnableHeadersVisualStyles = false;
            this.DGVPayType.GridColor = System.Drawing.Color.Silver;
            this.DGVPayType.Location = new System.Drawing.Point(3, 72);
            this.DGVPayType.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DGVPayType.MultiSelect = false;
            this.DGVPayType.Name = "DGVPayType";
            this.DGVPayType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DGVPayType.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.DGVPayType.RowHeadersVisible = false;
            this.DGVPayType.RowHeadersWidth = 55;
            this.DGVPayType.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.DGVPayType.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.DGVPayType.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGVPayType.RowTemplate.Height = 23;
            this.DGVPayType.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVPayType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVPayType.Size = new System.Drawing.Size(771, 235);
            this.DGVPayType.TabIndex = 784;
            this.DGVPayType.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVPayType_CellEndEdit);
            // 
            // PayTypeName
            // 
            this.PayTypeName.DataPropertyName = "PayTypeName";
            this.PayTypeName.HeaderText = "نوع الدفع";
            this.PayTypeName.Name = "PayTypeName";
            this.PayTypeName.ReadOnly = true;
            this.PayTypeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayTypeName.Width = 252;
            // 
            // PayTypeID
            // 
            this.PayTypeID.DataPropertyName = "PayTypeID";
            this.PayTypeID.HeaderText = "PayTypeID";
            this.PayTypeID.Name = "PayTypeID";
            this.PayTypeID.ReadOnly = true;
            this.PayTypeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayTypeID.Visible = false;
            // 
            // CurrencyPrice2
            // 
            this.CurrencyPrice2.HeaderText = "المبلغ بالعملة";
            this.CurrencyPrice2.Name = "CurrencyPrice2";
            this.CurrencyPrice2.Width = 135;
            // 
            // Statement
            // 
            this.Statement.DataPropertyName = "Statement";
            this.Statement.HeaderText = "البيان";
            this.Statement.Name = "Statement";
            this.Statement.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Statement.Width = 382;
            // 
            // CurrencyID2
            // 
            this.CurrencyID2.HeaderText = "CurrencyID2";
            this.CurrencyID2.Name = "CurrencyID2";
            this.CurrencyID2.Visible = false;
            // 
            // CurrencyName2
            // 
            this.CurrencyName2.HeaderText = "العملة";
            this.CurrencyName2.Name = "CurrencyName2";
            // 
            // CurrencyRate2
            // 
            this.CurrencyRate2.HeaderText = "سعر التعادل";
            this.CurrencyRate2.Name = "CurrencyRate2";
            // 
            // PayValue
            // 
            this.PayValue.DataPropertyName = "Debit";
            this.PayValue.HeaderText = "المبلغ بعد التحويل";
            this.PayValue.Name = "PayValue";
            this.PayValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PayValue.Width = 135;
            // 
            // txtStatement
            // 
            this.txtStatement.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStatement.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtStatement.ForeColor = System.Drawing.Color.Black;
            this.txtStatement.Location = new System.Drawing.Point(3, 46);
            this.txtStatement.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStatement.Name = "txtStatement";
            this.txtStatement.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStatement.Size = new System.Drawing.Size(382, 24);
            this.txtStatement.TabIndex = 783;
            this.txtStatement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStatement.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStatement_KeyDown);
            // 
            // txtPayValue
            // 
            this.txtPayValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPayValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPayValue.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtPayValue.ForeColor = System.Drawing.Color.Black;
            this.txtPayValue.Location = new System.Drawing.Point(386, 46);
            this.txtPayValue.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPayValue.Name = "txtPayValue";
            this.txtPayValue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPayValue.Size = new System.Drawing.Size(132, 24);
            this.txtPayValue.TabIndex = 780;
            this.txtPayValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPayValue.TextChanged += new System.EventHandler(this.txtPayValue_TextChanged);
            this.txtPayValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValue_KeyDown);
            this.txtPayValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPayValue_KeyPress);
            // 
            // combPayType
            // 
            this.combPayType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combPayType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combPayType.FormattingEnabled = true;
            this.combPayType.Location = new System.Drawing.Point(519, 46);
            this.combPayType.Name = "combPayType";
            this.combPayType.Size = new System.Drawing.Size(254, 24);
            this.combPayType.TabIndex = 779;
            this.combPayType.TextChanged += new System.EventHandler(this.combPayType_TextChanged);
            this.combPayType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.combPayType_KeyDown);
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtCustomerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCustomerName.Location = new System.Drawing.Point(92, 83);
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.ReadOnly = true;
            this.txtCustomerName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCustomerName.Size = new System.Drawing.Size(310, 24);
            this.txtCustomerName.TabIndex = 783;
            this.txtCustomerName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalProfits
            // 
            this.txtTotalProfits.Location = new System.Drawing.Point(874, 79);
            this.txtTotalProfits.Name = "txtTotalProfits";
            this.txtTotalProfits.ReadOnly = true;
            this.txtTotalProfits.Size = new System.Drawing.Size(10, 24);
            this.txtTotalProfits.TabIndex = 784;
            this.txtTotalProfits.Visible = false;
            // 
            // txtTotalBayPrice
            // 
            this.txtTotalBayPrice.Location = new System.Drawing.Point(874, 79);
            this.txtTotalBayPrice.Name = "txtTotalBayPrice";
            this.txtTotalBayPrice.ReadOnly = true;
            this.txtTotalBayPrice.Size = new System.Drawing.Size(10, 24);
            this.txtTotalBayPrice.TabIndex = 785;
            this.txtTotalBayPrice.Visible = false;
            // 
            // txtMainNetInvoice
            // 
            this.txtMainNetInvoice.Location = new System.Drawing.Point(874, 79);
            this.txtMainNetInvoice.Name = "txtMainNetInvoice";
            this.txtMainNetInvoice.ReadOnly = true;
            this.txtMainNetInvoice.Size = new System.Drawing.Size(10, 24);
            this.txtMainNetInvoice.TabIndex = 786;
            this.txtMainNetInvoice.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(555, 86);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 17);
            this.label21.TabIndex = 799;
            this.label21.Text = "سعر الصرف :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(588, 58);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 17);
            this.label17.TabIndex = 798;
            this.label17.Text = "العملة :";
            // 
            // txtCurrencyRate
            // 
            this.txtCurrencyRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCurrencyRate.BackColor = System.Drawing.Color.White;
            this.txtCurrencyRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrencyRate.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrencyRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtCurrencyRate.Location = new System.Drawing.Point(645, 82);
            this.txtCurrencyRate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCurrencyRate.Name = "txtCurrencyRate";
            this.txtCurrencyRate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCurrencyRate.Size = new System.Drawing.Size(286, 24);
            this.txtCurrencyRate.TabIndex = 797;
            this.txtCurrencyRate.Text = "1";
            this.txtCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrencyRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCurrencyRate_KeyPress);
            // 
            // combCurrency
            // 
            this.combCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combCurrency.FormattingEnabled = true;
            this.combCurrency.Location = new System.Drawing.Point(645, 54);
            this.combCurrency.Name = "combCurrency";
            this.combCurrency.Size = new System.Drawing.Size(286, 24);
            this.combCurrency.TabIndex = 809;
            this.combCurrency.SelectedIndexChanged += new System.EventHandler(this.combCurrency_SelectedIndexChanged);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::ByStro.Properties.Resources.SearchRead_25;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(107, 24);
            this.toolStripButton2.Text = "بحث الاصناف";
            // 
            // BillSalesR_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(935, 507);
            this.Controls.Add(this.combCurrency);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtCurrencyRate);
            this.Controls.Add(this.txtMainNetInvoice);
            this.Controls.Add(this.txtTotalBayPrice);
            this.Controls.Add(this.txtTotalProfits);
            this.Controls.Add(this.txtCustomerName);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtVatValue);
            this.Controls.Add(this.txtReturnTotalBayPrice);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtRestInvoise);
            this.Controls.Add(this.txtpaid_Invoice);
            this.Controls.Add(this.txtNetInvoice);
            this.Controls.Add(this.txtTotalDescound);
            this.Controls.Add(this.txtTotalInvoice);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.combPayKind);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.txtAccountID);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtBillTypeID);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.txtMainID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label16);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "BillSalesR_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "مرتجع المبيعات";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OrderProudect_frm_FormClosing);
            this.Load += new System.EventHandler(this.PureItemToStoreForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BillSales_frm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVPayType)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox txtMainID;
        private System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.DateTimePicker D1;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.DataGridView DGV1;
        private System.Windows.Forms.TextBox txtBillTypeID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAccountID;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtRestInvoise;
        public System.Windows.Forms.TextBox txtpaid_Invoice;
        public System.Windows.Forms.TextBox txtNetInvoice;
        public System.Windows.Forms.TextBox txtTotalDescound;
        public System.Windows.Forms.TextBox txtTotalInvoice;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.ComboBox combPayKind;
        private System.Windows.Forms.ToolStripButton btnNew;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblCount;
        public System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.TextBox txtReturnTotalBayPrice;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.TextBox txtVatValue;
        public System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox combPayType;
        public System.Windows.Forms.TextBox txtStatement;
        public System.Windows.Forms.TextBox txtPayValue;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel txtSumPayType;
        public System.Windows.Forms.DataGridView DGVPayType;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtCustomerName;
        private System.Windows.Forms.TextBox txtTotalProfits;
        private System.Windows.Forms.TextBox txtTotalBayPrice;
        private System.Windows.Forms.TextBox txtMainNetInvoice;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayTypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyPrice2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Statement;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyID2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyRate2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PayValue;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtCurrencyRate;
        private System.Windows.Forms.ComboBox combCurrency;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel lblQTY;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProdecutID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProdecutName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnCurrencyVat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnCurrencyTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnVat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnTotalPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn StoreName;
        private System.Windows.Forms.DataGridViewTextBoxColumn StoreID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitOperating;
        private System.Windows.Forms.DataGridViewTextBoxColumn BayPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalBayPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn MainVat;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrencyRate;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
    }
}