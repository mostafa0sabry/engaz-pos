﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
using System.Data.SqlClient;
namespace ByStro.PL
{
    public partial class User_Permetion_frm : Form
    {
        public User_Permetion_frm()
        {
            InitializeComponent();
        }
        public String AddEdit = "Add";
        UserPermissions_CLS cls = new UserPermissions_CLS();
        public Boolean Add = true;
        private void Permetion_FormAdd_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);


        }




        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            txtUserName.BackColor = Color.White;
        }

        private void txtUserPassword_TextChanged(object sender, EventArgs e)
        {
            txtUserPassword.BackColor = Color.White;
        }





        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            btnSave.Text = "حفظ";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            btnSave.Text = "حفظ";
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            btnSave.Text = "التالي";
        }

        private void txtEmployee_TextChanged(object sender, EventArgs e)
        {
            txtEmployee.BackColor = Color.White;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {

            try
            {
                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    checkedListBox1.SetItemChecked(i, false);
                }
                Add = true;
                chStatus.Checked = true;
                txtEmployee.Text = "";
                txtUserName.Text = "";
                txtUserPassword.Text = "";
                combTypeUser.SelectedIndex = 1;
                txtEmployee.Focus();
            }
            catch
            {


            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmployee.Text.Trim() == "")
                {
                    txtEmployee.BackColor = Color.Pink;
                    txtEmployee.Focus();
                    return;
                }

                if (txtUserName.Text.Trim() == "")
                {
                    txtUserName.BackColor = Color.Pink;
                    txtUserName.Focus();

                    return;
                }
                if (txtUserPassword.Text.Trim() == "")
                {
                    txtUserPassword.BackColor = Color.Pink;
                    txtUserPassword.Focus();

                    return;
                }

                if (combTypeUser.SelectedIndex == 0)
                {
                    for (int i = 0; i < checkedListBox1.Items.Count; i++)
                    {
                        checkedListBox1.SetItemChecked(i, true);
                    }
                }



                string sql = "select * from UserPermissions where ID = N'" + txtID.Text + "'";
                SqlDataAdapter da2 = new SqlDataAdapter(sql, DataAccess_Layer.connSQLServer);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);

                if (Add == true)
                {
                    DataTable dt_search = cls.NameSearch_UserPermissions(txtUserName.Text);
                    if (dt_search.Rows.Count > 0)
                    {
                        txtUserName.BackColor = Color.Pink;
                        txtUserName.Focus();
                        MessageBox.Show("الاسم المستخدم المدخل موجود مسبقا", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;

                    }
                }


                DataRow dr;
                if (Add == true)
                {
                    dr = dt2.NewRow();
                    dr["ID"] = cls.MaxID_UserPermissions();

                }
                else
                {

                    dr = dt2.Rows[0];
                    dr["ID"] = txtID.Text;

                }

                dr["EmpName"] = txtEmployee.Text;
                dr["UserName"] = txtUserName.Text;
                dr["UserPassword"] = txtUserPassword.Text;
                bool TypeUser = false;
                if (combTypeUser.SelectedIndex == 0)
                {
                    TypeUser = true;
                }
                dr["TypeUser"] = TypeUser;

                dr["Status"] = chStatus.Checked;
                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    dr[i + 6] = checkedListBox1.GetItemChecked(i);
                }
                if (Add == true)
                {
                    dt2.Rows.Add(dr);
                    SqlCommandBuilder cmd = new SqlCommandBuilder(da2);
                    da2.Update(dt2);
                }
                else
                {
                    // dt2.Rows.Add(dr);
                    SqlCommandBuilder cmd = new SqlCommandBuilder(da2);
                    da2.Update(dt2);
                }


                if (Add == true)
                {
                    MessageBox.Show("تم الحفظ بنجاح", "حفظ", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    MessageBox.Show("تم التعديل بنجاح", "تعديل", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

                btnNew_Click(null, null);



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    checkedListBox1.SetItemChecked(i, false);
                }
            }
            catch
            {


            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    checkedListBox1.SetItemChecked(i, true);
                }
            }
            catch
            {


            }
        }

        private void txtUserPassword_TextChanged_1(object sender, EventArgs e)
        {
            txtUserPassword.BackColor = Color.White;
        }
        DataAccessLayer DataAccess_Layer = new DataAccessLayer();
        private void btnGetData_Click(object sender, EventArgs e)
        {
            try
            {

                UserSearch_frm frm = new UserSearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == false)
                {
                    return;
                }

                DataTable dt = cls.Details_UserPermissions(frm.DGV1.CurrentRow.Cells["ID"].Value.ToString());

                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("User Name is not Found", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;

                }


                DataRow Dr = dt.Rows[0];
                txtID.Text = Dr["ID"].ToString();
                txtEmployee.Text = Dr["EmpName"].ToString();
                txtUserName.Text = Dr["UserName"].ToString();
                txtUserPassword.Text = Dr["UserPassword"].ToString();

                if (Convert.ToBoolean(Dr["TypeUser"].ToString()) == true)
                {
                    combTypeUser.SelectedIndex = 0;
                }
                else
                {
                    combTypeUser.SelectedIndex = 1;
                }
                chStatus.Checked = Convert.ToBoolean(Dr["Status"]);
                Add = false;

                for (int i = 0; i < checkedListBox1.Items.Count; i++)
                {
                    checkedListBox1.SetItemChecked(i, Convert.ToBoolean(Dr[i + 6]));
                }







            }
            catch
            {

            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void User_Permetion_frm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                btnSave_Click(null, null);
            }
            else if (e.Control == true && e.KeyCode == Keys.A)
            {
                btnUpdate_Click(null, null);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void combTypeUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combTypeUser.SelectedIndex == 0)
            {
                checkedListBox1.Enabled = false;
            }
            else
            {
                checkedListBox1.Enabled = true;
            }
        }
    }
}
