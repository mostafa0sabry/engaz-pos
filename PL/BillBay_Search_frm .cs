﻿using System;
using System.Data;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class BillBay_Search_frm : Form
    {
        public BillBay_Search_frm()
        {
            InitializeComponent();
        }
        Bay_cls cls = new Bay_cls();
        public Boolean LoadData = false;
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                DataTable dt = cls.Search_Bay_Main(D1.Value, D2.Value, Properties.Settings.Default.UserID, txtSearch.Text);
                DGV2.AutoGenerateColumns = false;
                DGV2.DataSource = dt;
                sumbills();
            }
            catch
            {
                return;
            }
        }

        private void SalesReturn_Form_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);
                ERB_Setting.SettingDGV(DGV2);
                DataTable dt = cls.Load_Bay_Main(D1.Value, D2.Value, Properties.Settings.Default.UserID);
                DGV2.AutoGenerateColumns = false;
                DGV2.DataSource = dt;
                sumbills();
                txtSearch.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }





        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            try
            {

                DataTable dt = cls.Load_Bay_Main(D1.Value, D2.Value, Properties.Settings.Default.UserID);
                DGV2.AutoGenerateColumns = false;
                DGV2.DataSource = dt;
                sumbills();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = cls.Load_Bay_Main(D1.Value, D2.Value, Properties.Settings.Default.UserID);
                DGV2.AutoGenerateColumns = false;
                DGV2.DataSource = dt;
                sumbills();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void sumbills()
        {


            try
            {


                Decimal NetInvoice = 0;
                Decimal ReturnInvoise = 0;
                Decimal paid_Invoice = 0;
                Decimal RestInvoise = 0;
                lblCount.Text = "0";

                for (int i = 0; i < DGV2.Rows.Count; i++)
                {
                    ReturnInvoise += Convert.ToDecimal(DGV2.Rows[i].Cells["ReturnInvoise"].Value);
                    NetInvoice += Convert.ToDecimal(DGV2.Rows[i].Cells["NetInvoice"].Value);
                    paid_Invoice += Convert.ToDecimal(DGV2.Rows[i].Cells["PaidInvoice"].Value);
                    RestInvoise += Convert.ToDecimal(DGV2.Rows[i].Cells["RestInvoise"].Value);
                }
                lblRetrunInvoice.Text = ReturnInvoise.ToString();
                lblSum.Text = NetInvoice.ToString();
                lblPay.Text = paid_Invoice.ToString();
                lblRest.Text = RestInvoise.ToString();
                lblCount.Text = DGV2.Rows.Count.ToString(); ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }




        }



        private void DGV2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGV2.Rows.Count == 0)
                {
                    return;
                }
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                DataTable dt = cls.Details_Bay_Details(DGV2.CurrentRow.Cells["MainID"].Value.ToString());
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void DGV2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                String Header = DGV2.Columns[e.ColumnIndex].Name;
                if (Header == "C1")
                {



                    //RPT.CRSales report = new RPT.CRSales();
                    //Reportes_Form frm = new Reportes_Form();
                    //Search_Sales_Details(DGV2.CurrentRow.Cells["MainID"].Value.ToString());
                    //DGV1.AutoGenerateColumns = false;
                    //DGV1.DataSource = Dt_Sales_Details;
                    //report.Database.Tables["CombanyData"].SetDataSource(DataAccessLayer.Dt_CombanyData);
                    //report.Database.Tables["Sales_Details"].SetDataSource(Dt_Sales_Details);
                    //frm.crystalReportViewer1.ReportSource = report;
                    //frm.crystalReportViewer1.Refresh();
                    //report.SetParameterValue(0, DGV2.CurrentRow.Cells["CustomerName"].Value.ToString());
                    //report.SetParameterValue(1, "");
                    //report.SetParameterValue(2, DGV2.CurrentRow.Cells["InvoiceCode"].Value.ToString());
                    //report.SetParameterValue(3, DGV2.CurrentRow.Cells["BillType"].Value.ToString());
                    //report.SetParameterValue(4, DGV2.CurrentRow.Cells["Note"].Value.ToString());
                    //report.SetParameterValue(5, "");
                    //report.SetParameterValue(6, DGV2.CurrentRow.Cells["TotalInvoice"].Value.ToString());
                    //report.SetParameterValue(7, DGV2.CurrentRow.Cells["TotalDescound"].Value.ToString());
                    //report.SetParameterValue(8, DGV2.CurrentRow.Cells["NetInvoice"].Value.ToString());
                    //report.SetParameterValue(9, DGV2.CurrentRow.Cells["paid_Invoice"].Value.ToString());
                    //report.SetParameterValue(10, DGV2.CurrentRow.Cells["RestInvoise"].Value.ToString());
                    //report.SetParameterValue(11, "");


                    //frm.ShowDialog();



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
        public Boolean ReturnInvoice = false;
        private void btnView_Click(object sender, EventArgs e)
        {
            if (DGV2.Rows.Count == 0)
            {
                return;
            }
            if (ReturnInvoice == false)
            {

                if (Convert.ToDouble(DGV2.CurrentRow.Cells["ReturnInvoise"].Value) != 0)
                {
                    MessageBox.Show("لا يمكنك عرض الفاتورةبسبب تم  عمل مرتجع علي الفاتورة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }
            LoadData = true;
            Close();
        }

        private void BillBay_Search_frm_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.WindowState == FormWindowState.Maximized)
                {
                    DGV2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                }
                else
                {
                    DGV2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                    DGV2.Columns[0].Width = 150;
                    DGV2.Columns[1].Width = 150;
                    DGV2.Columns[2].Width = 250;
                    DGV2.Columns[3].Width = 150;
                    DGV2.Columns[4].Width = 150;
                    DGV2.Columns[5].Width = 100;
                    DGV2.Columns[6].Width = 150;
                    DGV2.Columns[7].Width = 100;
                    DGV2.Columns[8].Width = 150;
                    DGV2.Columns[9].Width = 150;
                    DGV2.Columns[10].Width = 300;
                    DGV2.Columns[11].Width = 200;
                }

                panel3.Height = this.Height / 2;
            }
            catch
            {

            }

        }
        public  bool isForSelect;
        public static int  SelectedInvoice;

        public static void SelectInvoiceItems(out  int InvoiceID)
        {
            SelectedInvoice = 0;
            BillBay_Search_frm frm = new BillBay_Search_frm();
            frm.isForSelect = true;
            frm.ShowDialog();
            InvoiceID =  SelectedInvoice;


        }
        private void DGV2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
            if (isForSelect)
            {
                int.TryParse(DGV2["MainID", e.RowIndex].Value .ToString(), out SelectedInvoice);
                if (SelectedInvoice != 0)
                    this.Close();
            }
        }
    }
}