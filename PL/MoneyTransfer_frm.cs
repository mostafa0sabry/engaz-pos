﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class MoneyTransfer_frm : Form
    {
        public MoneyTransfer_frm()
        {
            InitializeComponent();
        }

        PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();
        MonyTransfer_cls cls = new MonyTransfer_cls();
      
        private void CustomersPayment_FormAdd_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);

            FillCurrency();
            Search_cls.FillcombPayType(combPayTypeFrom);
            Search_cls.FillcombPayType(combPayTypeto);
            btnNew_Click_1(null, null);

        }



        public void FillCurrency()
        {
            string cusName = combCurrency.Text;
            Currency_cls Currency_cls = new Currency_cls();
            combCurrency.DataSource = Currency_cls.Search__Currency("");
            combCurrency.DisplayMember = "CurrencyName";
            combCurrency.ValueMember = "CurrencyID";
            combCurrency.Text = cusName;

        }



        private void txtPaymentValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }






        TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();


   










        private void LoadPaymentCustomer()
        {
            try
            {

                MoneyTransfer_Search_frm frm = new MoneyTransfer_Search_frm();
                frm.ShowDialog(this);
                if (frm.LoadData == true)
                {
                    btnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                    DataTable dt = cls.Details_MoneyTransfer(int.Parse(frm.DGV1.CurrentRow.Cells["TransferID"].Value.ToString()));
                    DataRow Dr = dt.Rows[0];
                    txtTransferID.Text = Dr["TransferID"].ToString();
                    D1.Value = Convert.ToDateTime(Dr["MyDate"]);

                    combPayTypeFrom.Text = Dr["PayTypeNameFrom"].ToString();

                    combPayTypeto.Text = Dr["PayTypeNameTo"].ToString();
                    txtAmount.Text = Dr["Amount"].ToString();
                    txtNote.Text = Dr["Note"].ToString();

                    combCurrency.Text = Dr["CurrencyName"].ToString();
                    txtCurrencyRate.Text = Dr["CurrencyRate"].ToString();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnNew_Click_1(object sender, EventArgs e)
        {
            try
            {
                txtTransferID.Text = Convert.ToString(cls.MaxID_MoneyTransfer());
                D1.Value = DateTime.Now;
                txtAmount.Text = "";
                combPayTypeFrom.Text = null;
                combPayTypeto.Text = null;
               // txtCurrencyRate.Text = "";
                txtAmount.BackColor = System.Drawing.Color.White;
                txtNote.Text = "";
         
                if (BillSetting_cls.UseCrrencyDefault == true)
                {
                    Currency_cls Currency = new Currency_cls();
                    DataTable DtCurrency = Currency.Details_Currency(BillSetting_cls.CurrencyID.ToString());
                    combCurrency.Text = DtCurrency.Rows[0]["CurrencyName"].ToString();
                    txtCurrencyRate.Text = DtCurrency.Rows[0]["CurrencyRate"].ToString();
                }
                else
                {
                    combCurrency.Text = null;
                    txtCurrencyRate.Text = "";

                }

                combPayTypeFrom.Focus();

                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtTransferID.Text == "")
                {
                    MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }



                if (combPayTypeFrom.SelectedItem == null)
                {
                    combPayTypeFrom.BackColor = Color.Pink;
                    combPayTypeFrom.Focus();
                    return;
                }


                if (combPayTypeto.SelectedItem == null)
                {
                    combPayTypeto.BackColor = Color.Pink;
                    combPayTypeto.Focus();
                    return;
                }



                if (combPayTypeto.SelectedValue == combPayTypeFrom.SelectedValue)
                {
                    MessageBox.Show(" لا يمكنك التحويل لنفس الحساب يرجي تحديد حساب مختلف ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    combPayTypeFrom.BackColor = Color.Pink;
                    combPayTypeFrom.Focus();
                    return;
                }



                if (txtAmount.Text.Trim() == "" || txtAmount.Text.Trim() == "0")
                {
                    txtAmount.BackColor = Color.Pink;
                    txtAmount.Focus();
                    return;
                }


                if (combCurrency.SelectedItem == null)
                {
                    combCurrency.BackColor = Color.Pink;
                    combCurrency.Focus();
                    return;
                }

                if (txtCurrencyRate.Text.Trim() == "" || txtCurrencyRate.Text.Trim() == "0")
                {
                    txtCurrencyRate.BackColor = Color.Pink;
                    txtCurrencyRate.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(txtNote.Text.Trim()))
                {
                    txtNote.BackColor = Color.Pink;
                    txtNote.Focus();
                    return;
                }


                // TreasuryID = cls_Treasury_Movement.MaxID_TreasuryMovement();
                txtTransferID.Text = cls.MaxID_MoneyTransfer();

                // cls_Treasury_Movement.InsertTreasuryMovement(TreasuryID, combPayTypeFrom.SelectedValue.ToString(), "truns", txtTransferID.Text, txtTypeID.Text, this.Text, D1.Value, txtNote.Text, txtAmount.Text, "0", int.Parse(combCurrency.SelectedValue.ToString()), double.Parse(txtCurrencyRate.Text), Properties.Settings.Default.UserID, true);
                PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();
                cls.Insert_MoneyTransfer(int.Parse(txtTransferID.Text), D1.Value, int.Parse(combPayTypeFrom.SelectedValue.ToString()), int.Parse(combPayTypeto.SelectedValue.ToString()), double.Parse(txtAmount.Text), int.Parse(combCurrency.SelectedValue.ToString()), double.Parse(txtCurrencyRate.Text), txtNote.Text);
                PayType_trans_cls.Insert_PayType_trans(txtTransferID.Text, D1.Value, combPayTypeFrom.SelectedValue.ToString(), txtTransferID.Text, this.Text, txtTypeID.Text,0, double.Parse(txtAmount.Text), txtNote.Text, Properties.Settings.Default.UserID, combCurrency.SelectedValue.ToString(), txtCurrencyRate.Text, (double.Parse(txtCurrencyRate.Text) * double.Parse(txtAmount.Text)).ToString());
                PayType_trans_cls.Insert_PayType_trans(txtTransferID.Text, D1.Value, combPayTypeto.SelectedValue.ToString(), txtTransferID.Text, this.Text, txtTypeID.Text, double.Parse(txtAmount.Text),0, txtNote.Text, Properties.Settings.Default.UserID, combCurrency.SelectedValue.ToString(), txtCurrencyRate.Text, (double.Parse(txtCurrencyRate.Text) * double.Parse(txtAmount.Text)).ToString());


                Mass.Saved();
                btnNew_Click_1(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnUpdate_Click_1(object sender, EventArgs e)
        {
            try
            {

                if (txtTransferID.Text == "")
                {
                    MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }



                if (combPayTypeFrom.SelectedItem == null)
                {
                    combPayTypeFrom.BackColor = Color.Pink;
                    combPayTypeFrom.Focus();
                    return;
                }


                if (combPayTypeto.SelectedItem == null)
                {
                    combPayTypeto.BackColor = Color.Pink;
                    combPayTypeto.Focus();
                    return;
                }

                if (txtAmount.Text.Trim() == "" || txtAmount.Text.Trim() == "0")
                {
                    txtAmount.BackColor = Color.Pink;
                    txtAmount.Focus();
                    return;
                }


                if (combCurrency.SelectedItem == null)
                {
                    combCurrency.BackColor = Color.Pink;
                    combCurrency.Focus();
                    return;
                }

                if (txtCurrencyRate.Text.Trim() == "" || txtCurrencyRate.Text.Trim() == "0")
                {
                    txtCurrencyRate.BackColor = Color.Pink;
                    txtCurrencyRate.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(txtNote.Text.Trim()))
                {
                    txtNote.BackColor = Color.Pink;
                    txtNote.Focus();
                    return;
                }


                // TreasuryID = cls_Treasury_Movement.MaxID_TreasuryMovement();
                txtTransferID.Text = cls.MaxID_MoneyTransfer();
                PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();
                cls.Update_MoneyTransfer(int.Parse(txtTransferID.Text), D1.Value, int.Parse(combPayTypeFrom.SelectedValue.ToString()), int.Parse(combPayTypeto.SelectedValue.ToString()), double.Parse(txtAmount.Text), int.Parse(combCurrency.SelectedValue.ToString()), double.Parse(txtCurrencyRate.Text), txtNote.Text);


                PayType_trans_cls.Delete_PayType_trans(txtTransferID.Text,txtTypeID.Text);
                PayType_trans_cls.Insert_PayType_trans(txtTransferID.Text, D1.Value, combPayTypeFrom.SelectedValue.ToString(), txtTransferID.Text, this.Text, txtTypeID.Text, 0, double.Parse(txtAmount.Text), txtNote.Text, Properties.Settings.Default.UserID, combCurrency.SelectedValue.ToString(), txtCurrencyRate.Text, (double.Parse(txtCurrencyRate.Text) * double.Parse(txtAmount.Text)).ToString());
                PayType_trans_cls.Insert_PayType_trans(txtTransferID.Text, D1.Value, combPayTypeto.SelectedValue.ToString(), txtTransferID.Text, this.Text, txtTypeID.Text, double.Parse(txtAmount.Text), 0, txtNote.Text, Properties.Settings.Default.UserID, combCurrency.SelectedValue.ToString(), txtCurrencyRate.Text, (double.Parse(txtCurrencyRate.Text) * double.Parse(txtAmount.Text)).ToString());


                Mass.Update();
                btnNew_Click_1(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    cls.Delete_MoneyTransfer(int.Parse(txtTransferID.Text));
                    PayType_trans_cls.Delete_PayType_trans(txtTransferID.Text, txtTypeID.Text);

                    btnNew_Click_1(null, null);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                LoadPaymentCustomer();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }




        private void combPayType_DropDown(object sender, EventArgs e)
        {

        }

        private void combPayType_TextChanged(object sender, EventArgs e)
        {
            combPayTypeto.BackColor = Color.White;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void combPayType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (combPayTypeto.SelectedIndex >= 0)
                {
                    txtAmount.Focus();
                }

            }
        }

        private void txtStatement_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                if (e.KeyCode == Keys.Enter)
                {
                    if (combCurrency.SelectedIndex < 0)
                    {
                        MessageBox.Show("يرجي تحديد العملة", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        combCurrency.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                    {
                        MessageBox.Show("يرجي تحديد سعر التعادل", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCurrencyRate.Focus();
                        return;
                    }
                    if (combPayTypeto.SelectedIndex < 0)
                    {
                        combPayTypeto.BackColor = Color.Pink;
                        combPayTypeto.Focus();
                        return;
                    }
                    if (txtAmount.Text.Trim() == "")
                    {
                        txtAmount.BackColor = Color.Pink;
                        txtAmount.Focus();
                        return;
                    }
                    if (double.Parse(txtAmount.Text) == 0)
                    {
                        txtAmount.BackColor = Color.Pink;
                        txtAmount.Focus();
                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }











        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            txtAmount.BackColor = Color.White;
        }

        private void txtPayValue_KeyDown(object sender, KeyEventArgs e)
        {

        }



        private void CustomersRecived_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click_1(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        toolStripButton3_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click_1(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click_1(null, null);
                    }
                }
                //if (e.KeyCode == Keys.F5)
                //{
                //    btnGetData_Click(null, null);
                //}

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
        }

        private void combCurrenvy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (combCurrency.SelectedIndex > -1)
                {
                    Currency_cls crrcls = new Currency_cls();
                    DataTable dt = crrcls.Details_Currency(combCurrency.SelectedValue.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        txtCurrencyRate.Text = dt.Rows[0]["CurrencyRate"].ToString();
                    }
                }
            }
            catch
            {

                return;
            }

        }

        private void txtCurrencyRate_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCurrencyRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtSumPayType_Click(object sender, EventArgs e)
        {

        }

        private void combPayTypeto_SelectedIndexChanged(object sender, EventArgs e)
        {
            combPayTypeto.BackColor = Color.White;
        }

        private void combPayTypeFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            combPayTypeFrom.BackColor = Color.White;
        }


    }
}
