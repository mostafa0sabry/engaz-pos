﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class SerilFull_FORM : Form
    {
        public SerilFull_FORM()
        {
            InitializeComponent();
        }
        public Boolean close = true;

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void SerilFull_FORM_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            txtProductID.Text = BL.ComputerInfo.GetComputerId();
            this.Text = DataAccessLayer.Version;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            var sql = txtProductKey.Text.Length;
            if (sql!=39)
            {
                txtProductKey.Focus();
                txtProductKey.BackColor = Color.Pink;
                return;
            }
              
            //txtProductKey.Text = GetHash(txtProductID.Text);

            try
            {
                var p = Properties.Settings.Default;
                p.computerID = txtProductID.Text.Trim();
                p.Finull_Sarial = txtProductKey.Text.Trim();
                p.Save();
                createregedit();
                this.Close();
                Application.Exit();
            }
            catch (Exception ex)
            {
           MessageBox.Show(ex.Message, "Message",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }


        }


        private static string GetHash(string s)
        {
            return GetHexString(new MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(s)));
        }

        private static string GetHexString(byte[] bt)
        {
            string str1 = string.Empty;
            for (int index = 0; index < bt.Length; ++index)
            {
                int num1 = (int)bt[index];
                int num2 = 15;
                int num3 = num1 & num2;
                int num4 = 4;
                int num5 = num1 >> num4 & 15;
                string str2 = num5 <= 8 ? str1 + num5.ToString() : str1 + ((char)(num5 - 8 + 65)).ToString();
                str1 = num3 <= 8 ? str2 + num3.ToString() : str2 + ((char)(num3 - 8 + 65)).ToString();
                if (index + 1 != bt.Length && (index + 1) % 2 == 0)
                    str1 += "-";
            }
            return str1;
        }



        private void pictureBox5_Click(object sender, EventArgs e)
        {
            Process.Start("https://plus.google.com/u/0/113608438800827724392");
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.facebook.com/startech40");
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Process.Start("https://twitter.com/StarTec40");
        }

        private void SerilFull_FORM_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void SerilFull_FORM_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (close==true)
            {
                Application.Exit();
            }
           
        }

        private void SerilFull_FORM_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (close == true)
            {
                Application.Exit();
            }
        }


        public void createregedit()
        {
            RegistryKey regkey = default(RegistryKey);

            try
            {
               
                regkey = Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
                regkey.CreateSubKey("ByStor");
                regkey.Close();

                writvalue();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public bool writvalue()
        {
            RegistryKey regkey = default(RegistryKey);

            try
            {
                regkey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\ByStor", true);

                regkey.SetValue("System_Activation", EncryptData(txtProductKey.Text.Trim()));
                regkey.SetValue("AppName", "ByStor");
                regkey.Close();



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return true;
        }



        public string EncryptData(string cLeartext)
        {
            //MAKV2SPBNI99212

            try
            {

                string encryptionkey = "ENG11MOHAMMED55519";
                byte[] Clearebyet = Encoding.Unicode.GetBytes(cLeartext);

                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionkey, new byte[] {
				0x1,
				0x11,
				0x36,
				0x97,
				0x69,
				0x8d,
				0x11,
				0x12,
				0x13,
				0x14,
				0x15,
				0x16,
				0x17
			});

                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);

                    using (MemoryStream MS = new MemoryStream())
                    {

                        using (CryptoStream CS = new CryptoStream(MS, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            CS.Write(Clearebyet, 0, Clearebyet.Length);
                            CS.Close();


                        }
                        cLeartext = Convert.ToBase64String(MS.ToArray());
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return cLeartext;
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txtProductID.SelectAll();
            txtProductID.Copy();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           
                Connection_Form frm = new Connection_Form();
                frm.ShowDialog();
        
        }

        private void txtProductKey_TextChanged(object sender, EventArgs e)
        {
            txtProductKey.BackColor = Color.White;
            if (FinullSarial()== txtProductKey.Text)
            {
                pictureBox1.Visible = true;
            }
            else
            {
                pictureBox1.Visible = false;
            }
        }
        

        private String FinullSarial()
        {
            String str = BL.Final_key.GetHash(Properties.Settings.Default.computerID);
            return str;
        }

    }
}
