﻿
using System;
using System.Data;
class CashExport_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Cash_Export()
    {
        return Execute_SQL("select ISNULL (MAX(ExportID)+1,1) from Cash_Export", CommandType.Text);
    }



    // Insert
    public void Insert_Cash_Export(string ExportID, DateTime MayDate, string Remarks, string Value, string TreasuryID, int UserAdd)
    {
        Execute_SQL("insert into Cash_Export(ExportID ,MayDate ,Remarks ,Value ,TreasuryID,UserAdd )Values (@ExportID ,@MayDate ,@Remarks ,@Value,@TreasuryID ,@UserAdd )", CommandType.Text,
        Parameter("@ExportID", SqlDbType.NVarChar, ExportID),
        Parameter("@MayDate", SqlDbType.Date, MayDate),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@Value", SqlDbType.Float, Value),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void Update_Cash_Export(string ExportID, DateTime MayDate, string Remarks, string Value)
    {
        Execute_SQL("Update Cash_Export Set MayDate=@MayDate ,Remarks=@Remarks ,Value=@Value  where ExportID=@ExportID", CommandType.Text,
        Parameter("@ExportID", SqlDbType.NVarChar, ExportID),
        Parameter("@MayDate", SqlDbType.Date, MayDate),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@Value", SqlDbType.Float, Value));
    }

    //Delete
    public void Delete_Cash_Export(string ExportID)
    {
        Execute_SQL("Delete  From Cash_Export where ExportID=@ExportID", CommandType.Text,
        Parameter("@ExportID", SqlDbType.NVarChar, ExportID));
    }

        //Delete
    public DataTable Search_Cash_Export()
    {
       return ExecteRader("Select *  from Cash_Export ", CommandType.Text);
    }

    //Delete
    public DataTable Search_Cash_Export(string ExportID)
    {
        return ExecteRader("Select *  from Cash_Export where ExportID=@ExportID", CommandType.Text,
         Parameter("@ExportID", SqlDbType.NVarChar, ExportID));
    }

}

