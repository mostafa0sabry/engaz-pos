﻿using System;
using System.Data;



    class UserPermissions_CLS: DataAccessLayer
    {


      public void Update_Password(String ID, String UserPassword)
      {
         Execute_SQL("update UserPermissions set UserPassword=@UserPassword  where ID=@ID", CommandType.Text);
        Parameter("@ID", SqlDbType.NVarChar, ID);
        Parameter("@UserPassword", SqlDbType.NVarChar, UserPassword);
      }

    //Search
      public DataTable Search_UserPermissions()
      {
          return ExecteRader("Select * from UserPermissions Where Status='1'", CommandType.Text);
      }

      public DataTable Search_NewLogIn_UserPermissions()
      {
          return ExecteRader("Select * from UserPermissions", CommandType.Text);
      }

      public DataTable Search_LogIn_UserPermissions(string UserName, string UserPassword, Boolean Status)
      {
          return ExecteRader("select * from UserPermissions where UserName =@UserName and UserPassword=@UserPassword and Status=@Status", CommandType.Text,
              Parameter("@UserName", SqlDbType.NVarChar, UserName),
           Parameter("@UserPassword", SqlDbType.NVarChar, UserPassword),
            Parameter("@Status", SqlDbType.Bit, Status));
      }

        

    //MaxID
    public String MaxID_UserPermissions()
    {
        return Execute_SQL("select ISNULL (MAX(ID)+1,1) from UserPermissions", CommandType.Text);
    }


    //Search 
    public DataTable Search_UserPermissions(string Search)
    {
        return ExecteRader("Select *  from UserPermissions Where EmpName like '%'+@Search+ '%' ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }


    //Details ID
    public DataTable Details_UserPermissions(string ID)
    {
        return ExecteRader("Select *  from UserPermissions Where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.Int, ID));
    }


    //Details ID
    public DataTable NameSearch_UserPermissions(String EmpName)
    {
        return ExecteRader("Select EmpName  from UserPermissions Where EmpName=@EmpName", CommandType.Text,
        Parameter("@EmpName", SqlDbType.NVarChar, EmpName));
    }





}
