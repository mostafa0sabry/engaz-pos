﻿using System;
using System.Data;

class Prodecut_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Prodecuts()
    {
        return Execute_SQL("select ISNULL (MAX(ProdecutID)+1,1) from Prodecuts", CommandType.Text);
    }

    // Insert
    public void InsertProdecuts(string ProdecutID, String ProdecutName, string CategoryID, String ProdecutLocation, String Company, DateTime LastUpdate, Double RequestLimit, String FiestUnit, String FiestUnitOperating, String FiestUnitFactor, String FiestUnitBarcode, Double FiestUnitPrice1, Double FiestUnitPrice2, Double FiestUnitPrice3, String SecoundUnit, String SecoundUnitOperating, String SecoundUnitFactor, String SecoundUnitBarcode, Double SecoundUnitPrice1, Double SecoundUnitPrice2, Double SecoundUnitPrice3, String ThreeUnit, String ThreeUnitOperating, String ThreeUnitFactor, String ThreeUnitBarcode, Double ThreeUnitPrice1, Double ThreeUnitPrice2, Double ThreeUnitPrice3, Double ProdecutBayPrice, Double DiscoundBay, Double DiscoundSale, int UnitDefoult, Boolean Status, Boolean ProductService,bool HasColor, bool HasSize, bool HasSerial, bool HasExpire)
    {
        Execute_SQL("insert into Prodecuts(ProdecutID ,ProdecutName ,CategoryID ,ProdecutLocation ,Company ,LastUpdate ,RequestLimit ,FiestUnit ,FiestUnitOperating ,FiestUnitFactor ,FiestUnitBarcode ,FiestUnitPrice1 ,FiestUnitPrice2 ,FiestUnitPrice3 ,SecoundUnit ,SecoundUnitOperating ,SecoundUnitFactor ,SecoundUnitBarcode ,SecoundUnitPrice1 ,SecoundUnitPrice2 ,SecoundUnitPrice3 ,ThreeUnit ,ThreeUnitOperating ,ThreeUnitFactor ,ThreeUnitBarcode ,ThreeUnitPrice1 ,ThreeUnitPrice2 ,ThreeUnitPrice3 ,ProdecutBayPrice  ,DiscoundBay ,DiscoundSale  ,UnitDefoult ,Status ,ProductService,HasColor,HasSize,HasSerial,HasExpire )Values (@ProdecutID ,@ProdecutName ,@CategoryID ,@ProdecutLocation ,@Company ,@LastUpdate ,@RequestLimit ,@FiestUnit ,@FiestUnitOperating ,@FiestUnitFactor ,@FiestUnitBarcode ,@FiestUnitPrice1 ,@FiestUnitPrice2 ,@FiestUnitPrice3 ,@SecoundUnit ,@SecoundUnitOperating ,@SecoundUnitFactor ,@SecoundUnitBarcode ,@SecoundUnitPrice1 ,@SecoundUnitPrice2 ,@SecoundUnitPrice3 ,@ThreeUnit ,@ThreeUnitOperating ,@ThreeUnitFactor ,@ThreeUnitBarcode ,@ThreeUnitPrice1 ,@ThreeUnitPrice2 ,@ThreeUnitPrice3 ,@ProdecutBayPrice ,@DiscoundBay ,@DiscoundSale ,@UnitDefoult ,@Status ,@ProductService ,@HasColor,@HasSize,@HasSerial,@HasExpire)", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@ProdecutName", SqlDbType.NVarChar, ProdecutName),
        Parameter("@CategoryID", SqlDbType.Int, CategoryID),
        Parameter("@ProdecutLocation", SqlDbType.NVarChar, ProdecutLocation),
        Parameter("@Company", SqlDbType.NVarChar, Company),
        Parameter("@LastUpdate", SqlDbType.DateTime, LastUpdate),
        Parameter("@RequestLimit", SqlDbType.Float, RequestLimit),
        Parameter("@FiestUnit", SqlDbType.NVarChar, FiestUnit),
        Parameter("@FiestUnitOperating", SqlDbType.NVarChar, FiestUnitOperating),
        Parameter("@FiestUnitFactor", SqlDbType.NVarChar, FiestUnitFactor),
        Parameter("@FiestUnitBarcode", SqlDbType.NVarChar, FiestUnitBarcode),
        Parameter("@FiestUnitPrice1", SqlDbType.Float, FiestUnitPrice1),
        Parameter("@FiestUnitPrice2", SqlDbType.Float, FiestUnitPrice2),
        Parameter("@FiestUnitPrice3", SqlDbType.Float, FiestUnitPrice3),
        Parameter("@SecoundUnit", SqlDbType.NVarChar, SecoundUnit),
        Parameter("@SecoundUnitOperating", SqlDbType.NVarChar, SecoundUnitOperating),
        Parameter("@SecoundUnitFactor", SqlDbType.NVarChar, SecoundUnitFactor),
        Parameter("@SecoundUnitBarcode", SqlDbType.NVarChar, SecoundUnitBarcode),
        Parameter("@SecoundUnitPrice1", SqlDbType.Float, SecoundUnitPrice1),
        Parameter("@SecoundUnitPrice2", SqlDbType.Float, SecoundUnitPrice2),
        Parameter("@SecoundUnitPrice3", SqlDbType.Float, SecoundUnitPrice3),
        Parameter("@ThreeUnit", SqlDbType.NVarChar, ThreeUnit),
        Parameter("@ThreeUnitOperating", SqlDbType.NVarChar, ThreeUnitOperating),
        Parameter("@ThreeUnitFactor", SqlDbType.NVarChar, ThreeUnitFactor),
        Parameter("@ThreeUnitBarcode", SqlDbType.NVarChar, ThreeUnitBarcode),
        Parameter("@ThreeUnitPrice1", SqlDbType.Float, ThreeUnitPrice1),
        Parameter("@ThreeUnitPrice2", SqlDbType.Float, ThreeUnitPrice2),
        Parameter("@ThreeUnitPrice3", SqlDbType.Float, ThreeUnitPrice3),
        Parameter("@ProdecutBayPrice", SqlDbType.Float, ProdecutBayPrice),
        //Parameter("@ProdecutAvg", SqlDbType.Float, ProdecutAvg),
        Parameter("@DiscoundBay", SqlDbType.Float, DiscoundBay),
        Parameter("@DiscoundSale", SqlDbType.Float, DiscoundSale),

        Parameter("@UnitDefoult", SqlDbType.Int, UnitDefoult),
        Parameter("@Status", SqlDbType.Bit, Status),
        Parameter("@ProductService", SqlDbType.Bit, ProductService),
        Parameter("@HasColor", SqlDbType.Bit, HasColor),
        Parameter("@HasSize", SqlDbType.Bit, HasSize),
        Parameter("@HasSerial", SqlDbType.Bit, HasSerial),
        Parameter("@HasExpire", SqlDbType.Bit, HasExpire));
    }





    //Update
    public void UpdateProdecuts(string ProdecutID, String ProdecutName, string CategoryID, String ProdecutLocation, String Company, DateTime LastUpdate, Double RequestLimit, String FiestUnit, String FiestUnitOperating, String FiestUnitFactor, String FiestUnitBarcode, Double FiestUnitPrice1, Double FiestUnitPrice2, Double FiestUnitPrice3, String SecoundUnit, String SecoundUnitOperating, String SecoundUnitFactor, String SecoundUnitBarcode, Double SecoundUnitPrice1, Double SecoundUnitPrice2, Double SecoundUnitPrice3, String ThreeUnit, String ThreeUnitOperating, String ThreeUnitFactor, String ThreeUnitBarcode, Double ThreeUnitPrice1, Double ThreeUnitPrice2, Double ThreeUnitPrice3, Double ProdecutBayPrice, Double DiscoundBay, Double DiscoundSale, int UnitDefoult, Boolean Status, Boolean ProductService)
    {
        Execute_SQL("Update Prodecuts Set ProdecutName=@ProdecutName ,CategoryID=@CategoryID ,ProdecutLocation=@ProdecutLocation ,Company=@Company ,LastUpdate=@LastUpdate ,RequestLimit=@RequestLimit ,FiestUnit=@FiestUnit ,FiestUnitOperating=@FiestUnitOperating ,FiestUnitFactor=@FiestUnitFactor ,FiestUnitBarcode=@FiestUnitBarcode ,FiestUnitPrice1=@FiestUnitPrice1 ,FiestUnitPrice2=@FiestUnitPrice2 ,FiestUnitPrice3=@FiestUnitPrice3 ,SecoundUnit=@SecoundUnit ,SecoundUnitOperating=@SecoundUnitOperating ,SecoundUnitFactor=@SecoundUnitFactor ,SecoundUnitBarcode=@SecoundUnitBarcode ,SecoundUnitPrice1=@SecoundUnitPrice1 ,SecoundUnitPrice2=@SecoundUnitPrice2 ,SecoundUnitPrice3=@SecoundUnitPrice3 ,ThreeUnit=@ThreeUnit ,ThreeUnitOperating=@ThreeUnitOperating ,ThreeUnitFactor=@ThreeUnitFactor ,ThreeUnitBarcode=@ThreeUnitBarcode ,ThreeUnitPrice1=@ThreeUnitPrice1 ,ThreeUnitPrice2=@ThreeUnitPrice2 ,ThreeUnitPrice3=@ThreeUnitPrice3 ,ProdecutBayPrice=@ProdecutBayPrice,DiscoundBay=@DiscoundBay ,DiscoundSale=@DiscoundSale ,UnitDefoult=@UnitDefoult ,Status=@Status ,ProductService=@ProductService  where ProdecutID=@ProdecutID", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@ProdecutName", SqlDbType.NVarChar, ProdecutName),
        Parameter("@CategoryID", SqlDbType.Int, CategoryID),
        Parameter("@ProdecutLocation", SqlDbType.NVarChar, ProdecutLocation),
        Parameter("@Company", SqlDbType.NVarChar, Company),
        Parameter("@LastUpdate", SqlDbType.DateTime, LastUpdate),
        Parameter("@RequestLimit", SqlDbType.Float, RequestLimit),
        Parameter("@FiestUnit", SqlDbType.NVarChar, FiestUnit),
        Parameter("@FiestUnitOperating", SqlDbType.NVarChar, FiestUnitOperating),
        Parameter("@FiestUnitFactor", SqlDbType.NVarChar, FiestUnitFactor),
        Parameter("@FiestUnitBarcode", SqlDbType.NVarChar, FiestUnitBarcode),
        Parameter("@FiestUnitPrice1", SqlDbType.Float, FiestUnitPrice1),
        Parameter("@FiestUnitPrice2", SqlDbType.Float, FiestUnitPrice2),
        Parameter("@FiestUnitPrice3", SqlDbType.Float, FiestUnitPrice3),
        Parameter("@SecoundUnit", SqlDbType.NVarChar, SecoundUnit),
        Parameter("@SecoundUnitOperating", SqlDbType.NVarChar, SecoundUnitOperating),
        Parameter("@SecoundUnitFactor", SqlDbType.NVarChar, SecoundUnitFactor),
        Parameter("@SecoundUnitBarcode", SqlDbType.NVarChar, SecoundUnitBarcode),
        Parameter("@SecoundUnitPrice1", SqlDbType.Float, SecoundUnitPrice1),
        Parameter("@SecoundUnitPrice2", SqlDbType.Float, SecoundUnitPrice2),
        Parameter("@SecoundUnitPrice3", SqlDbType.Float, SecoundUnitPrice3),
        Parameter("@ThreeUnit", SqlDbType.NVarChar, ThreeUnit),
        Parameter("@ThreeUnitOperating", SqlDbType.NVarChar, ThreeUnitOperating),
        Parameter("@ThreeUnitFactor", SqlDbType.NVarChar, ThreeUnitFactor),
        Parameter("@ThreeUnitBarcode", SqlDbType.NVarChar, ThreeUnitBarcode),
        Parameter("@ThreeUnitPrice1", SqlDbType.Float, ThreeUnitPrice1),
        Parameter("@ThreeUnitPrice2", SqlDbType.Float, ThreeUnitPrice2),
        Parameter("@ThreeUnitPrice3", SqlDbType.Float, ThreeUnitPrice3),
        Parameter("@ProdecutBayPrice", SqlDbType.Float, ProdecutBayPrice),
        //Parameter("@ProdecutAvg", SqlDbType.Float, ProdecutAvg),
        Parameter("@DiscoundBay", SqlDbType.Float, DiscoundBay),
        Parameter("@DiscoundSale", SqlDbType.Float, DiscoundSale),
        Parameter("@UnitDefoult", SqlDbType.Int, UnitDefoult),
        Parameter("@Status", SqlDbType.Bit, Status),
                Parameter("@ProductService", SqlDbType.Bit, ProductService));
    }


    //Update
    public void UpdateProdecutBayPrice(string ProdecutID, Double ProdecutBayPrice)
    {
        Execute_SQL("Update Prodecuts Set ProdecutBayPrice=@ProdecutBayPrice  where ProdecutID=@ProdecutID", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
        Parameter("@ProdecutBayPrice", SqlDbType.Float, ProdecutBayPrice));
    }




    //Delete
    public void Delete_Prodecuts(Int64 ProdecutID)
    {
        Execute_SQL("Delete  From Prodecuts where ProdecutID=@ProdecutID", CommandType.Text,
          Parameter("@ProdecutID", SqlDbType.Int, ProdecutID));
    }

    //Details ID
    public DataTable Details_Prodecuts(int ProdecutID)
    {
        return ExecteRader("SELECT  dbo.Prodecuts.*, dbo.Category.CategoryName FROM  dbo.Category INNER JOIN dbo.Prodecuts ON dbo.Category.CategoryID = dbo.Prodecuts.CategoryID where dbo.Prodecuts.ProdecutID=@ProdecutID", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID));
    }


    //Search where  Name
    public DataTable NameSearch_Prodecuts(String ProdecutName)
    {
        return ExecteRader("Select ProdecutName From Prodecuts where ProdecutName=@ProdecutName", CommandType.Text,
        Parameter("@ProdecutName", SqlDbType.NVarChar, ProdecutName));
    }

    //Search
    public DataTable Search_Prodecuts(String Search)
    {
        return ExecteRader("Select *  from Prodecuts where convert(nvarchar, ProdecutID) + ProdecutName like '%'+ @Search + '%'", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }

    //Search
    public DataTable SearchBill_Prodecuts(string Search)
    {
        //return ExecteRader("Select ProdecutID ,ProdecutName from Prodecuts where Status='1'",CommandType.Text);
        return ExecteRader(@"Select ProdecutID ,ProdecutName ,CategoryID ,FiestUnit ,FiestUnitOperating ,FiestUnitFactor ,FiestUnitBarcode ,FiestUnitPrice1 ,FiestUnitPrice2 ,FiestUnitPrice3 ,SecoundUnit ,SecoundUnitOperating ,SecoundUnitFactor ,SecoundUnitBarcode ,SecoundUnitPrice1 ,SecoundUnitPrice2 ,SecoundUnitPrice3 ,ThreeUnit ,ThreeUnitOperating ,ThreeUnitFactor ,ThreeUnitBarcode ,ThreeUnitPrice1 ,ThreeUnitPrice2 ,ThreeUnitPrice3 ,ProdecutBayPrice ,ProdecutAvg ,DiscoundBay ,DiscoundSale  ,UnitDefoult ,Status from Prodecuts
        where  ProdecutName = @Search or FiestUnitBarcode = @Search or SecoundUnitBarcode = @Search or ThreeUnitBarcode = @Search", CommandType.Text,
      Parameter("@Search", SqlDbType.NVarChar, Search));
    }

    //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    public DataTable SearchBill_Prodecuts(string Search,string StoreID)
    {
        return ExecteRader(@"SELECT        dbo.Store_Prodecut.ProdecutID, dbo.Store_Prodecut.StoreID, dbo.Store_Prodecut.Balence, dbo.Store_Prodecut.Price, dbo.Store_Prodecut.ProdecutAvg, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, 
                         dbo.Prodecuts.ProdecutLocation, dbo.Prodecuts.Company, dbo.Prodecuts.LastUpdate, dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, dbo.Prodecuts.FiestUnitOperating, dbo.Prodecuts.FiestUnitFactor, 
                         dbo.Prodecuts.FiestUnitBarcode, dbo.Prodecuts.FiestUnitPrice1, dbo.Prodecuts.FiestUnitPrice2, dbo.Prodecuts.FiestUnitPrice3, dbo.Prodecuts.SecoundUnit, dbo.Prodecuts.SecoundUnitOperating, 
                         dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.SecoundUnitBarcode, dbo.Prodecuts.SecoundUnitPrice1, dbo.Prodecuts.SecoundUnitPrice2, dbo.Prodecuts.SecoundUnitPrice3, dbo.Prodecuts.ThreeUnit, 
                         dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor, dbo.Prodecuts.ThreeUnitBarcode, dbo.Prodecuts.ThreeUnitPrice1, dbo.Prodecuts.ThreeUnitPrice2, dbo.Prodecuts.ThreeUnitPrice3, 
                         dbo.Prodecuts.ProdecutBayPrice, dbo.Prodecuts.DiscoundBay, dbo.Prodecuts.DiscoundSale, dbo.Prodecuts.UnitDefoult, dbo.Prodecuts.ProductService, dbo.Prodecuts.Status
FROM            dbo.Prodecuts INNER JOIN
                         dbo.Store_Prodecut ON dbo.Prodecuts.ProdecutID = dbo.Store_Prodecut.ProdecutID
    where (dbo.Store_Prodecut.StoreID =@StoreID ) AND   dbo.Prodecuts.ProdecutName = @Search or dbo.Prodecuts.FiestUnitBarcode = @Search or dbo.Prodecuts.SecoundUnitBarcode = @Search or dbo.Prodecuts.ThreeUnitBarcode = @Search", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search),
              Parameter("@StoreID", SqlDbType.NVarChar, StoreID));
    }
    // Use Mathed for Quentity to DGV WHERE Prodect id
    public DataTable Edit_Quentity_GDV_Bill_Prodecuts(string ProdecutID, string StoreID)
    {
        return ExecteRader(@"    SELECT dbo.Store_Prodecut.ProdecutID, dbo.Store_Prodecut.StoreID, dbo.Store_Prodecut.Balence, dbo.Store_Prodecut.Price, dbo.Store_Prodecut.ProdecutAvg, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID,
                             dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, dbo.Prodecuts.FiestUnitOperating, dbo.Prodecuts.FiestUnitFactor, dbo.Prodecuts.FiestUnitBarcode, dbo.Prodecuts.FiestUnitPrice1, dbo.Prodecuts.FiestUnitPrice2,
                             dbo.Prodecuts.FiestUnitPrice3, dbo.Prodecuts.SecoundUnit, dbo.Prodecuts.SecoundUnitOperating, dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.SecoundUnitBarcode, dbo.Prodecuts.SecoundUnitPrice1,
                             dbo.Prodecuts.SecoundUnitPrice2, dbo.Prodecuts.SecoundUnitPrice3, dbo.Prodecuts.ThreeUnit, dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor, dbo.Prodecuts.ThreeUnitBarcode,
                             dbo.Prodecuts.ThreeUnitPrice1, dbo.Prodecuts.ThreeUnitPrice2, dbo.Prodecuts.ThreeUnitPrice3, dbo.Prodecuts.ProdecutBayPrice, dbo.Prodecuts.DiscoundBay, dbo.Prodecuts.DiscoundSale, dbo.Prodecuts.UnitDefoult,
                             dbo.Prodecuts.Status
    FROM            dbo.Prodecuts INNER JOIN
                             dbo.Store_Prodecut ON dbo.Prodecuts.ProdecutID = dbo.Store_Prodecut.ProdecutID
    where (dbo.Store_Prodecut.StoreID =@StoreID ) AND   dbo.Store_Prodecut.ProdecutID = @ProdecutID ", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
              Parameter("@StoreID", SqlDbType.NVarChar, StoreID));
    }
    //=========================رصيد الصنف في المخازن======================================================

    public DataTable Prodecut_Store(string ProdecutID)
    {
        return ExecteRader(@"SELECT  dbo.Store_Prodecut.ProdecutID, dbo.Store_Prodecut.StoreID, dbo.Store_Prodecut.Balence, dbo.Store_Prodecut.Price, dbo.Store_Prodecut.ProdecutAvg, dbo.Stores.StoreName
                      FROM   dbo.Store_Prodecut INNER JOIN dbo.Stores ON dbo.Store_Prodecut.StoreID = dbo.Stores.StoreID
                     WHERE   (dbo.Store_Prodecut.ProdecutID =@ProdecutID)", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID));
    }


     public DataTable Prodecut_SearchBill(string Search)
    {
        return ExecteRader(@"SELECT        dbo.Store_Prodecut.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.FiestUnit, dbo.Prodecuts.FiestUnitOperating, dbo.Prodecuts.FiestUnitFactor, dbo.Prodecuts.FiestUnitBarcode, dbo.Prodecuts.FiestUnitPrice1, 
                         dbo.Prodecuts.FiestUnitPrice2, dbo.Prodecuts.FiestUnitPrice3, dbo.Store_Prodecut.Balence / dbo.Prodecuts.FiestUnitFactor AS Balence, dbo.Prodecuts.SecoundUnitBarcode, dbo.Prodecuts.ThreeUnitBarcode, 
                         dbo.Category.CategoryName, dbo.Store_Prodecut.Price / dbo.Prodecuts.FiestUnitFactor AS coust
FROM            dbo.Store_Prodecut INNER JOIN
                         dbo.Prodecuts ON dbo.Store_Prodecut.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Category ON dbo.Prodecuts.CategoryID = dbo.Category.CategoryID
                     WHERE   ProdecutName+FiestUnitBarcode+SecoundUnitBarcode+ThreeUnitBarcode+dbo.Category.CategoryName like '%'+@Search+ '%' ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }




    //Details ID
    public DataTable NameSearch__ProdecutsBarcode(String Barcode)
    {
        return ExecteRader("Select FiestUnitBarcode ,SecoundUnitBarcode ,ThreeUnitBarcode  from Prodecuts Where FiestUnitBarcode=@Barcode or SecoundUnitBarcode=@Barcode or ThreeUnitBarcode=@Barcode", CommandType.Text,
        Parameter("@Barcode", SqlDbType.NVarChar, Barcode));
    }







}

