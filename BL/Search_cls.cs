﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
namespace ByStro {
    public class Search_cls
    {



        public static void FillcombSoreUsers(ComboBox comb, bool ISNULL = true)
        {
            DAL.DBDataContext db = new ByStro.DAL.DBDataContext(Properties.Settings.Default.Connection_String);
             
            comb.DataSource = db.Inv_Stores.Select(x=> new { StoreName = x.Name, StoreID = x.ID }).ToList();
            comb.DisplayMember = "StoreName";
            comb.ValueMember = "StoreID";
            comb.Text = null;
            if (ISNULL == false) 
                comb.SelectedIndex = 0;


        }

        public static void FillcombUsers(ComboBox comb, bool ISNULL = true)
        {
            UserPermissions_CLS userPermissions_CLS = new UserPermissions_CLS();
            DataTable dt = userPermissions_CLS.Search_UserPermissions("");
            comb.DataSource = dt;
            comb.DisplayMember = "EmpName";
            comb.ValueMember = "ID";
            comb.Text = null;
            if (ISNULL == false)
                if (dt.Rows.Count > 0) comb.SelectedIndex = 0;

        }



        public static void FillcombPayType(ComboBox comb, bool ISNULL = true)
        {
            PayType_cls PayType_cls = new PayType_cls();
            DataTable dt = PayType_cls.Search__PayType("");
            comb.DataSource = dt;
            comb.DisplayMember = "PayTypeName";
            comb.ValueMember = "PayTypeID";
            comb.Text = null;
            if (ISNULL == false)
                if (dt.Rows.Count > 0) comb.SelectedIndex = 0;

        }
    }
}
