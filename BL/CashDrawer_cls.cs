﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    class CashDrawer_cls : DataAccessLayer
    {
        // Expenses
        public DataTable Search_ImportCustomer(DateTime PayDate)
        {
            return ExecteRader("select isnull (SUM(PayValue),0) as PayValue from Customers_Recived where PayDate=@PayDate", CommandType.Text,
                 Parameter("@PayDate", SqlDbType.Date, PayDate));
        }

        // Expenses
        public DataTable Search_ImportCustomer(DateTime PayDate, string UserAdd)
        {
            return ExecteRader("select isnull (SUM(PayValue),0) as PayValue from Customers_Recived where PayDate=@PayDate and UserAdd=@UserAdd", CommandType.Text,
                 Parameter("@PayDate", SqlDbType.Date, PayDate),
                  Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
        }
        // Expenses
        public DataTable Search_ExportCustomer(DateTime PayDate)
        {
            return ExecteRader("select isnull (SUM(PayValue),0) as PayValue from Customers_Pay where PayDate=@PayDate", CommandType.Text,
                 Parameter("@PayDate", SqlDbType.Date, PayDate));
        }

        // Expenses
        public DataTable Search_ExportCustomer(DateTime PayDate, string UserAdd)
        {
            return ExecteRader("select isnull (SUM(PayValue),0) as PayValue from Customers_Pay where PayDate=@PayDate and UserAdd=@UserAdd", CommandType.Text,
                 Parameter("@PayDate", SqlDbType.Date, PayDate),
                   Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
        }


        // Expenses
        public DataTable Search_ImportSuppliers(DateTime PayDate)
        {
            return ExecteRader("select isnull (SUM(PayValue),0) as PayValue from Supplier_Recived where PayDate=@PayDate", CommandType.Text,
                 Parameter("@PayDate", SqlDbType.Date, PayDate));
        }

        // Expenses
        public DataTable Search_ImportSuppliers(DateTime PayDate, string UserAdd)
        {
            return ExecteRader("select isnull (SUM(PayValue),0) as PayValue from Supplier_Recived where PayDate=@PayDate and UserAdd=@UserAdd", CommandType.Text,
                 Parameter("@PayDate", SqlDbType.Date, PayDate),
                  Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
        }

        // Expenses
        public DataTable Search_ExportSuppliers(DateTime PayDate)
        {
            return ExecteRader("select isnull (SUM(PayValue),0) as PayValue from Supplier_Pay  where PayDate=@PayDate", CommandType.Text,
                 Parameter("@PayDate", SqlDbType.Date, PayDate));
        }

        // Expenses
        public DataTable Search_ExportSuppliers(DateTime PayDate, string UserAdd)
        {
            return ExecteRader("select isnull (SUM(PayValue),0) as PayValue from Supplier_Pay  where PayDate=@PayDate and UserAdd=@UserAdd", CommandType.Text,
                 Parameter("@PayDate", SqlDbType.Date, PayDate),
                   Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
        }

        // Expenses
        public DataTable Search_Cash_Import(DateTime MayDate)
        {
            return ExecteRader("select isnull (SUM(Value),0) as Value from Cash_Import where MayDate=@MayDate", CommandType.Text,
                 Parameter("@MayDate", SqlDbType.Date, MayDate));
        }

        // Expenses
        public DataTable Search_Cash_Import(DateTime MayDate, string UserAdd)
        {
            return ExecteRader("select isnull (SUM(Value),0) as Value from Cash_Import where MayDate=@MayDate and UserAdd=@UserAdd", CommandType.Text,
                 Parameter("@MayDate", SqlDbType.Date, MayDate),
                   Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
        }
        // Expenses
        public DataTable Search_Cash_Export(DateTime MayDate)
        {
            return ExecteRader("select isnull (SUM(Value),0) as Value from Cash_Export  where MayDate=@MayDate", CommandType.Text,
                 Parameter("@MayDate", SqlDbType.Date, MayDate));
        }
        // Expenses
        public DataTable Search_Cash_Export(DateTime MayDate, string UserAdd)
        {
            return ExecteRader("select isnull (SUM(Value),0) as Value from Cash_Export  where MayDate=@MayDate and UserAdd=@UserAdd", CommandType.Text,
                 Parameter("@MayDate", SqlDbType.Date, MayDate),
                       Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
        }

        // Expenses
        public DataTable Search_Expenses(DateTime MyDate)
        {
            return ExecteRader("select isnull (SUM(ExpenseValue),0) as ExpenseValue from Expenses where MyDate=@MyDate", CommandType.Text,
                 Parameter("@MyDate", SqlDbType.Date, MyDate));
        }

        // Expenses
        public DataTable Search_Expenses(DateTime MyDate, string UserAdd)
        {
            return ExecteRader("select isnull (SUM(ExpenseValue),0) as ExpenseValue from Expenses where MyDate=@MyDate and UserAdd=@UserAdd", CommandType.Text,
                 Parameter("@MyDate", SqlDbType.Date, MyDate),
                   Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
        }
        // Expenses
        public DataTable Search_Sales(DateTime MyDate)
        {
            return ExecteRader("select isnull (SUM(PaidInvoice),0) as PaidInvoice , isnull (SUM(PaidReturnInvoice),0)    as PaidReturnInvoice from Sales_Main where MyDate=@MyDate", CommandType.Text,
         Parameter("@MyDate", SqlDbType.Date, MyDate));
            }
        // Expenses
        public DataTable Search_Sales(DateTime MyDate, string UserAdd)
        {
            return ExecteRader("select isnull (SUM(PaidInvoice),0) as PaidInvoice , isnull (SUM(PaidReturnInvoice),0)    as PaidReturnInvoice from Sales_Main where MyDate=@MyDate and UserAdd=@UserAdd", CommandType.Text,
         Parameter("@MyDate", SqlDbType.Date, MyDate),
          Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
        }

        // Expenses
        public DataTable Search_Bay(DateTime MyDate)
        {
            return ExecteRader("select isnull (SUM(PaidInvoice),0) as PaidInvoice , isnull (SUM(PaidReturnInvoice),0)    as PaidReturnInvoice from Bay_Main where MyDate=@MyDate", CommandType.Text,
         Parameter("@MyDate", SqlDbType.Date, MyDate));
        }
        // Expenses
        public DataTable Search_Bay(DateTime MyDate, string UserAdd)
        {
            return ExecteRader("select isnull (SUM(PaidInvoice),0) as PaidInvoice , isnull (SUM(PaidReturnInvoice),0)    as PaidReturnInvoice from Bay_Main where MyDate=@MyDate and UserAdd=@UserAdd", CommandType.Text,
         Parameter("@MyDate", SqlDbType.Date, MyDate),
           Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
        }

        // Expenses
        public DataTable Search_Maintenance(DateTime MyDate)
        {
            return ExecteRader("select isnull (SUM(PaidInvoice),0) as Paid from Maintenance_Main where  MyDate=@MyDate", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate));
        }

        // Expenses
        public DataTable Search_Maintenance(DateTime MyDate, string UserAdd)
        {
            return ExecteRader("select isnull (SUM(PaidInvoice),0) as Paid from Maintenance_Main where  MyDate=@MyDate and UserAdd=@UserAdd", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
         Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
        }

    }

