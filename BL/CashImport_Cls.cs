﻿
using System;
using System.Data;
class CashImport_Cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Cash_Import()
    {
        return Execute_SQL("select ISNULL (MAX(ImportID)+1,1) from Cash_Import", CommandType.Text);
    }



    // Insert
    public void Insert_Cash_Import(string ImportID, DateTime MayDate, string Remarks, string Value, string TreasuryID, int UserAdd)
    {
        Execute_SQL("insert into Cash_Import(ImportID ,MayDate ,Remarks ,Value ,TreasuryID,UserAdd )Values (@ImportID ,@MayDate ,@Remarks ,@Value,@TreasuryID ,@UserAdd )", CommandType.Text,
        Parameter("@ImportID", SqlDbType.NVarChar, ImportID),
        Parameter("@MayDate", SqlDbType.Date, MayDate),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@Value", SqlDbType.Float, Value),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void Update_Cash_Import(string ImportID, DateTime MayDate, string Remarks, string Value)
    {
        Execute_SQL("Update Cash_Import Set MayDate=@MayDate ,Remarks=@Remarks ,Value=@Value  where ImportID=@ImportID", CommandType.Text,
        Parameter("@ImportID", SqlDbType.NVarChar, ImportID),
        Parameter("@MayDate", SqlDbType.Date, MayDate),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@Value", SqlDbType.Float, Value));
    }

    //Delete
    public void Delete_Cash_Import(string ImportID)
    {
        Execute_SQL("Delete  From Cash_Import where ImportID=@ImportID", CommandType.Text,
        Parameter("@ImportID", SqlDbType.NVarChar, ImportID));
    }

        //Delete
    public DataTable Search_Cash_Import()
    {
       return ExecteRader("Select *  from Cash_Import ", CommandType.Text);
    }

    //Delete
    public DataTable Search_Cash_Import(string ImportID)
    {
        return ExecteRader("Select *  from Cash_Import where ImportID=@ImportID", CommandType.Text,
         Parameter("@ImportID", SqlDbType.NVarChar, ImportID));
    }

}

