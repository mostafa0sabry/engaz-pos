﻿
using System;
using System.Data;

class CombanyInformatuon_cls : DataAccessLayer
{
    //Details ID
    public  DataTable Details_CombanyData()
    {
        return ExecteRader("Select *  from CombanyData where ID='1'", CommandType.Text);
    }

  

    // Insert
    public void Insert_CombanyData(String ID, String CombanyName, String CombanyDescriptone, String MyName, String PhoneNamber, Byte[] Imge, String Title, String BillRemark, String MaintenanceRemark)
    {
        Execute_SQL("insert into CombanyData(ID ,CombanyName ,CombanyDescriptone ,MyName ,PhoneNamber ,Imge ,Title ,BillRemark ,MaintenanceRemark )Values (@ID ,@CombanyName ,@CombanyDescriptone ,@MyName ,@PhoneNamber ,@Imge ,@Title ,@BillRemark ,@MaintenanceRemark )", CommandType.Text,
        Parameter("@ID", SqlDbType.Int, ID),
        Parameter("@CombanyName", SqlDbType.NVarChar, CombanyName),
        Parameter("@CombanyDescriptone", SqlDbType.NVarChar, CombanyDescriptone),
        Parameter("@MyName", SqlDbType.NVarChar, MyName),
        Parameter("@PhoneNamber", SqlDbType.NVarChar, PhoneNamber),
        Parameter("@Imge", SqlDbType.Image, Imge),
        Parameter("@Title", SqlDbType.NVarChar, Title),
        Parameter("@BillRemark", SqlDbType.NText, BillRemark),
        Parameter("@MaintenanceRemark", SqlDbType.NText, MaintenanceRemark));
    }


    // Insert
    public void InsertNew_CombanyData(String ID, String CombanyName, String CombanyDescriptone, String MyName, String PhoneNamber, String Title, String BillRemark, String MaintenanceRemark)
    {
        Execute_SQL("insert into CombanyData(ID ,CombanyName ,CombanyDescriptone ,MyName ,PhoneNamber ,Title ,BillRemark ,MaintenanceRemark )Values (@ID ,@CombanyName ,@CombanyDescriptone ,@MyName ,@PhoneNamber ,@Title ,@BillRemark ,@MaintenanceRemark )", CommandType.Text,
        Parameter("@ID", SqlDbType.Int, ID),
        Parameter("@CombanyName", SqlDbType.NVarChar, CombanyName),
        Parameter("@CombanyDescriptone", SqlDbType.NVarChar, CombanyDescriptone),
        Parameter("@MyName", SqlDbType.NVarChar, MyName),
        Parameter("@PhoneNamber", SqlDbType.NVarChar, PhoneNamber),
        Parameter("@Title", SqlDbType.NVarChar, Title),
        Parameter("@BillRemark", SqlDbType.NText, BillRemark),
        Parameter("@MaintenanceRemark", SqlDbType.NText, MaintenanceRemark));
    }



    //Update
    public void Update_CombanyData(String ID, String CombanyName, String CombanyDescriptone, String MyName, String PhoneNamber, Byte[] Imge, String Title, String BillRemark, String MaintenanceRemark)
    {
        Execute_SQL("Update CombanyData Set ID=@ID ,CombanyName=@CombanyName ,CombanyDescriptone=@CombanyDescriptone ,MyName=@MyName ,PhoneNamber=@PhoneNamber ,Imge=@Imge ,Title=@Title ,BillRemark=@BillRemark ,MaintenanceRemark=@MaintenanceRemark  where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.Int, ID),
        Parameter("@CombanyName", SqlDbType.NVarChar, CombanyName),
        Parameter("@CombanyDescriptone", SqlDbType.NVarChar, CombanyDescriptone),
        Parameter("@MyName", SqlDbType.NVarChar, MyName),
        Parameter("@PhoneNamber", SqlDbType.NVarChar, PhoneNamber),
        Parameter("@Imge", SqlDbType.Image, Imge),
        Parameter("@Title", SqlDbType.NVarChar, Title),
        Parameter("@BillRemark", SqlDbType.NText, BillRemark),
        Parameter("@MaintenanceRemark", SqlDbType.NText, MaintenanceRemark));
    }






}

