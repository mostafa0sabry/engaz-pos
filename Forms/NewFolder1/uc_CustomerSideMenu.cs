﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ByStro.Clases;

namespace ByStro.Forms.UserControls
{
    public partial class uc_CustomerSideMenu : DevExpress.XtraEditors.XtraUserControl
    {
        private int _CustomerID;
        public int CustomerID
        {
            get
            {
                return _CustomerID;
            }
            set
            {
                _CustomerID = value;
                ViewBalance();
            }
        }
        void ViewBalance()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            var cus = (from c in db.Sl_Customers
                       where c.ID == CustomerID 
                       select c).FirstOrDefault();
            if (cus == null) return;
            
          var   CustomerBalance = Master.GetAccountBalance(cus.Account);
            accordionControlElement3.Text = LangResource.Balance +": " +  Math.Abs(CustomerBalance.Balance).ToString();
            accordionControlElement3.Text  +=" "+( (CustomerBalance.Balance >= 0) ? LangResource.Debit : LangResource.Credit);
        }
        public uc_CustomerSideMenu()
        {
            InitializeComponent(); 
        }
        private void accordionControl1_ExpandStateChanged(object sender, DevExpress.XtraBars.Navigation.ExpandStateChangedEventArgs e)
        {
        }
        int LastWidth;
        private void accordionControl1_StateChanged(object sender, EventArgs e)
        {
            if(accordionControl1.OptionsMinimizing.State == DevExpress.XtraBars.Navigation.AccordionControlState.Minimized)
            {
                LastWidth = this.Width; 
                this.Width  = accordionControl1.Width ;
            }
            else
            {
                this.Width    =LastWidth ;
            }
        }
        private void uc_CustomerSideMenu_SizeChanged(object sender, EventArgs e)
        {
            if (accordionControl1.OptionsMinimizing.State == DevExpress.XtraBars.Navigation.AccordionControlState.Normal)
            {
                accordionControl1.Width = this.Width; 
            }
        }

        private void accordionControlElement11_Click(object sender, EventArgs e)
        {
            // عرض فواتير المبيعات 
            //frm_Main.OpenForm(new frm_Inv_InvoiceList(Master.InvoiceType.SalesInvoice, Master.PartTypes.Customer,CustomerID));
        }

        private void accordionControlElement13_Click(object sender, EventArgs e)
        {
            //frm_Main.OpenForm(new frm_Inv_Invoice(CustomerID,Master.PartTypes.Customer ,Master.InvoiceType.SalesInvoice ));
        }
        private void accordionControlElement12_Click(object sender, EventArgs e)
        {

            if (CustomerID > 0)
            {
                var AccountId=CurrentSession.UserAccessbileCustomers.First(x => x.ID == CustomerID).Account;
                if (AccountId > 0)
                    PL.Main_frm.OpenForm(new frm_AccountStatment(AccountId), true);
            }
        }

        private void accordionControlElement7_Click(object sender, EventArgs e)
        {
            //frm_Main.OpenForm(new frm_CashNote(false ,CustomerID, "Customer"));
        }

        private void accordionControlElement4_Click(object sender, EventArgs e)
        {
            //frm_Main.OpenForm(new frm_CashNote(true, CustomerID, "Customer"));
        }
        private void accordionControlElement6_Click(object sender, EventArgs e)
        {
            //frm_Main.OpenForm(new frm_Inv_InvoiceList(Master.InvoiceType.SalesInvoice, Master.PartTypes.Customer,CustomerID));
        }

        private void accordionControlElement8_Click(object sender, EventArgs e)
        {
            //frm_Main.OpenForm(new frm_Sl_SlReturnInvoice(CustomerID, TypeToLoad: "cus"));
        }
    }
}
