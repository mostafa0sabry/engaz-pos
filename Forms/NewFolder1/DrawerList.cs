﻿using ByStro.Clases;
using ByStro.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.Forms
{
    public class DrawerList : FormLayoutList
    {
        public DrawerList():base("ID"){}
        DAL.Acc_Drawer Acc_Drawer;
        public override void PopulateColumns()
        {
            base.PopulateColumns();
            gridView1.CustomColumn(nameof(Acc_Drawer.ID), "م", true, false);
            gridView1.CustomColumn(nameof(Acc_Drawer.Name), "الاسم", true, false);
            gridView1.CustomColumn(nameof(Acc_Drawer.ACCID), "رقم الحساب", true, false);
            gridView1.CustomColumn(nameof(Acc_Drawer.Notes), "ملاحظات", true, false);
        }
        public override void RefreshData()
        {
            using (DAL.DBDataContext objDataContext = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                this.gridControl1.DataSource = objDataContext.Acc_Drawers.ToList();
            }
            base.RefreshData();
        }
        public override void OpenForm(int id)
        {
            Main_frm .OpenForm( new frm_Drawer(id),true);
            base.OpenForm(id);
            RefreshData();
        }
        public override void New()
        {
            Main_frm.OpenForm(new frm_Drawer(), true);
            base.New();
            RefreshData();
        }
    }
}
