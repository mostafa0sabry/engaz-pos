﻿using ByStro.Clases;
using ByStro.PL;
using DevExpress.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.Forms
{
  public  class frm_AccountTransactionList : FormLayoutList
    {
        DAL.Acc_CashTransfer Acc_CashTransfer;
        public frm_AccountTransactionList() : base("ID") { }
        public override void PopulateColumns()
        {
            base.PopulateColumns();
            gridView1.CustomColumn(nameof(Acc_CashTransfer.ID), "م", true, false);
            gridView1.CustomColumn(nameof(Acc_CashTransfer.Date), "التاريخ", true, false);
            gridView1.CustomColumn(nameof(Acc_CashTransfer.StoreID), "الفرع", true, false);
            gridView1.CustomColumn(nameof(Acc_CashTransfer.FromDrawerID), "من", true, false);
            gridView1.CustomColumn(nameof(Acc_CashTransfer.ToDrawerID), "الي", true, false);
            gridView1.CustomColumn(nameof(Acc_CashTransfer.Amount), "القية", true, false);
            gridView1.CustomColumn(nameof(Acc_CashTransfer.Note), "ملاحظات", true, false);
        }
        public override void RefreshData()
        {
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                this.gridControl1.DataSource = db.Acc_CashTransfers.Select(x=>new { 
                    x.ID,
                    x.Date,
                    StoreID = db.Inv_Stores.FirstOrDefault(a => a.ID == x.StoreID).Name??"",
                    FromDrawerID = db.Acc_Drawers.FirstOrDefault(a => a.ACCID == x.FromDrawerID).Name??"",
                    ToDrawerID = db.Acc_Drawers.FirstOrDefault(a => a.ACCID == x.ToDrawerID).Name??"",
                    x.Amount,
                    x.Note,
                }).ToList();
            }
            base.RefreshData();
        }
        public override void OpenForm(int id)
        {
            Main_frm.OpenForm(new frm_AccountTransaction(id), true);
            base.OpenForm(id);
            RefreshData();
        }
        public override void New()
        {
            Main_frm.OpenForm(new frm_AccountTransaction(), true);
            base.New();
            RefreshData();
        }
    }
}
