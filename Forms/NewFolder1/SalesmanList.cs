﻿using ByStro.Clases;
using ByStro.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.Forms
{
    public class SalesmanList : FormLayoutList
    {
        public SalesmanList():base("ID"){}
        DAL.Salesman  salesman;
        public override void PopulateColumns()
        {
            base.PopulateColumns();
            gridView1.CustomColumn(nameof(salesman.ID), "م", true, false);
            gridView1.CustomColumn(nameof(salesman.Name), "الاسم", true, false);
            gridView1.CustomColumn(nameof(salesman.Phone), "هاتف", true, false);
            gridView1.CustomColumn(nameof(salesman.Address), "العنوان", true, false);
            gridView1.CustomColumn(nameof(salesman.Commission), "العمولة", true, false);
            gridView1.CustomColumn(nameof(salesman.AccountID), "رقم الحساب", false, false);
        }
        public override void RefreshData()
        {
            using (DAL.DBDataContext objDataContext = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                this.gridControl1.DataSource = objDataContext.Salesmans.ToList();
            }
            base.RefreshData();
        }
        public override void OpenForm(int id)
        {
            Main_frm .OpenForm( new frm_Salesman(id),true);
            base.OpenForm(id);
            RefreshData();
        }
        public override void New()
        {
            Main_frm.OpenForm(new frm_Salesman(), true);
            base.New();
            RefreshData();
        }
    }
}
