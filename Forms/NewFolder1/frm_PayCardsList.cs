﻿using ByStro.Clases;
using ByStro.PL;
using DevExpress.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.Forms
{
   public class frm_PayCardsList:FormLayoutList
    {
        public frm_PayCardsList():base("ID")
        {

        }
        DAL.Acc_PayCard  acc_PayCard;
        public override void PopulateColumns()
        {
            base.PopulateColumns();
            gridView1.CustomColumn(nameof(acc_PayCard.ID), "م", true, false);
            gridView1.CustomColumn(nameof(acc_PayCard.BankID), "البنك", true, false);
            gridView1.CustomColumn(nameof(acc_PayCard.Number), "الرقم/الاسم", true, false);
            gridView1.CustomColumn(nameof(acc_PayCard.Commission), "عمولة البنك", true, false);
            gridView1.CustomColumn(nameof(acc_PayCard.CommissionAccount), "حساب العمولة", true, false);
            gridView1.CustomColumn(nameof(acc_PayCard.LinkedToBranch), "الفرع", true, false);
        }
        public override void RefreshData()
        {
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                this.gridControl1.DataSource = db.Acc_PayCards.Select(x => new
                {
                    x.ID,
                    BankID = db.Acc_Banks.FirstOrDefault(a => a.ID == x.BankID).BankName ?? "",
                    x.Number,
                    x.Commission,
                    CommissionAccount = db.Acc_Accounts.FirstOrDefault(a => a.ID == x.CommissionAccount).Name ?? "",
                    LinkedToBranch = db.Inv_Stores.FirstOrDefault(a => a.ID == x.LinkedToBranch).Name ?? "",
                }).ToList();
            }
            base.RefreshData();
        }
        public override void OpenForm(int id)
        {
            Main_frm.OpenForm(new frm_PayCards(id), true);
            base.OpenForm(id);
            RefreshData();
        }
        public override void New()
        {
            Main_frm.OpenForm(new frm_PayCards(), true);
            base.New();
            RefreshData();
        }
    }
}
