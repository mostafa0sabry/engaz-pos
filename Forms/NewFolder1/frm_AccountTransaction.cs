﻿using ByStro.Clases;
using ByStro.DAL;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_AccountTransaction : frm_Master
    {
        DAL.Acc_CashTransfer Trans = new DAL.Acc_CashTransfer();
        public frm_AccountTransaction()
        {
            InitializeComponent();
            New();
        }
        public frm_AccountTransaction(int TransID)
        {
            InitializeComponent();
            LoadObject(TransID);

        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            btn_Refresh.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_List.Visibility = btn_Print.Visibility = btn_SaveAndPrint.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;

            lkp_Store.Properties.ValueMember = "ID";
            lkp_Store.Properties.DisplayMember = "Name";
            lkp_DrawerTo.Properties.ValueMember=lkp_DrawerFrom.Properties.ValueMember  = "ACCID";
            lkp_DrawerTo.Properties.DisplayMember =lkp_DrawerFrom.Properties.DisplayMember = "Name";

            GetData();
            #region DataChangedEventHandlers
            lkp_DrawerFrom .EditValueChanged += DataChanged;
            lkp_DrawerTo .EditValueChanged += DataChanged;
            lkp_Store .EditValueChanged += DataChanged;
            spinEdit1 .EditValueChanged += DataChanged;
            memoEdit1 .EditValueChanged += DataChanged;

            #endregion

        }
        public override void RefreshData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String); 
            var drawers = (from c in db.Acc_Drawers
                           join a in db.Acc_Accounts
                           on (int)c.ACCID equals a.ID 
                           select new { c.ACCID , c.Name }).ToList();
            lkp_DrawerFrom.Properties.DataSource = drawers ;
            lkp_DrawerTo.Properties.DataSource = drawers;
            var Stores = from s in db.Inv_Stores select new { s.ID, s.Name };
            lkp_Store.Properties.DataSource = Stores.ToList();
           
            base.RefreshData();
        }
      
        public  void GoTo(int id)
        {
            if (id.ToString() == txt_ID.Text) return;
            if (ChangesMade && !SaveChangedData()) return;
            LoadObject(id);
            GetData();
        }
        private bool ValidData()
        {
          
            if (Convert.ToDouble(spinEdit1 .EditValue) < 0)
            {
                this.spinEdit1.ErrorText = "يجب ان تكون القيمه اكبر من صفر";
                this.spinEdit1.Focus();
                return false;
            }
            if (dateEdit1 .EditValue == null)
            {
                this.dateEdit1.ErrorText = LangResource.ErrorCantBeEmpry;
                this.dateEdit1.Focus();
                return false;
            }
            if (lkp_DrawerFrom .EditValue == null)
            {
                this.dateEdit1.ErrorText = LangResource.ErrorCantBeEmpry;
                this.dateEdit1.Focus();
                return false;
            }
            if (lkp_DrawerTo .EditValue == null)
            {
                this.lkp_DrawerTo.ErrorText = LangResource.ErrorCantBeEmpry;
                this.lkp_DrawerTo.Focus();
                return false;
            }


            return true;
        }
        public override void Print()
        {
            //if (CanPerformPrint() == false) return;

        }
        int GetNextID()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            try
            {
                return (int)db.Acc_CashTransfers.Max(n => n.ID) + 1;
            }
            catch
            {
                return (int)1;
            }
        }
        public override void Save()
        {
           // if (IsNew && !CanAdd) { XtraMessageBox.Show(LangResource.CantAddNoPermission, "", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }
            if (CanSave() == false) return;
            if (!ValidData()) { return; }
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
             
            if (IsNew)
            {
                Trans  = new DAL.Acc_CashTransfer ();
                Trans.ID = Convert.ToInt32(txt_ID.Text);
                db.Acc_CashTransfers.InsertOnSubmit(Trans);
            }
            else
            {
                Trans = db.Acc_CashTransfers .Where(s => s.ID == Convert.ToInt32(Trans .ID)).First();
                  
            }
            SetData();
            db.SubmitChanges();

            Master.DeleteAccountAcctivity("18", Trans.ID. ToString());
            Acc_Activity acctivity = new Acc_Activity()
            {

                Date = Trans.Date,
                StoreID = Trans.StoreID,
                Type = "18",
                TypeID = Trans.ID.ToString()
                , Note = LangResource.CashTransfer + " " + LangResource.Number + Environment.NewLine
                + LangResource.From + " " + lkp_DrawerFrom.Text + Environment.NewLine + LangResource.To + " " + lkp_DrawerTo.Text 

            };

           
            db.Acc_Activities.InsertOnSubmit(acctivity);
            db.SubmitChanges();



            Acc_ActivityDetial DrawerFromAcctivity  = new Acc_ActivityDetial()
            {
                ACCID = Convert.ToInt32( lkp_DrawerFrom .EditValue ),
                AcivityID = acctivity.ID,
                Note =  LangResource.CashTransfer + " " + LangResource.Number + LangResource.To + " " + lkp_DrawerTo.Text,
                Debit = 0,
                Credit = Convert.ToDouble(spinEdit1.EditValue ),
            };
            Acc_ActivityDetial DrawerToAcctivity = new Acc_ActivityDetial()
            {
                ACCID = Convert.ToInt32(lkp_DrawerTo .EditValue),
                AcivityID = acctivity.ID,
                Note = LangResource.CashTransfer + " " + LangResource.Number + LangResource.From  + " " + lkp_DrawerFrom .Text,
                Debit = Convert.ToDouble(spinEdit1.EditValue),
                Credit = 0,
            };
            db.Acc_ActivityDetials.InsertOnSubmit(DrawerFromAcctivity);
            db.Acc_ActivityDetials.InsertOnSubmit(DrawerToAcctivity);
            db.SubmitChanges();

            base.Save();


        }
        public override void New()
        {
            if (ChangesMade && !SaveChangedData()) return;
            Trans = new DAL.Acc_CashTransfer ();
            Trans.ID = GetNextID();
            Trans.Amount = 0;
            Trans.Date = DateTime.Now;
            Trans.Note = "";
            Trans.StoreID = CurrentSession.User.DefaultStore;
            IsNew = true;
            GetData();
            base.New();
            ChangesMade = false;
        }
        public override void Delete()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            

            if (IsNew) return;
            PartNumber = txt_ID.Text.ToString();
            if (Master.AskForDelete(this, IsNew, PartName, PartNumber))
            {
                base.Delete();
                Master.DeleteAccountAcctivity("18", Trans.ID.ToString()); 
                db.Acc_CashTransfers.DeleteAllOnSubmit(db.Acc_CashTransfers.Where(c =>  c.ID == Trans.ID ));
                db.SubmitChanges();
                Master.RefreshAllWindows();
                New();
            }
        }
        void SetData()
        {
            Trans .ID = Convert.ToInt32(txt_ID.Text);
            Trans.FromDrawerID = Convert.ToInt32(lkp_DrawerFrom.EditValue);
            Trans.ToDrawerID  = Convert.ToInt32(lkp_DrawerTo .EditValue);
            Trans.Amount = Convert.ToDouble(spinEdit1.EditValue);
            Trans.Date = dateEdit1.DateTime;
            Trans.Note = memoEdit1.Text; 
            Trans.StoreID = Convert.ToInt32(lkp_Store .EditValue); 
            PartNumber = txt_ID.Text;
            PartName = lkp_DrawerFrom.Text;

        }
        void GetData()
        {
            txt_ID.Text = Trans.ID.ToString();
            lkp_DrawerFrom.EditValue = Trans.FromDrawerID;
            lkp_DrawerTo.EditValue = Trans.ToDrawerID;
            spinEdit1.EditValue = Trans.Amount;
            dateEdit1.DateTime= Trans.Date;
            memoEdit1.Text= Trans.Note;
            lkp_Store.EditValue= Trans.StoreID;
        }
        void LoadObject(int TransID)
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            Trans  = (from i in db.Acc_CashTransfers where i.ID == TransID select i).First();
            IsNew = false;

        }

        private void lkp_DrawerFrom_EditValueChanged(object sender, EventArgs e)
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            if (lkp_DrawerTo.EditValue == lkp_DrawerFrom.EditValue)
                lkp_DrawerTo.EditValue = null;

            var drawers = (from c in db.Acc_Drawers
                           join a in db.Acc_Accounts
                           on (int)c.ACCID equals a.ID 
                           select new { c.ACCID, c.Name }).ToList();
            lkp_DrawerFrom.Properties.DataSource = drawers;
            lkp_DrawerTo.Properties.DataSource = drawers.Where(x => x.ACCID.ToString() != lkp_DrawerFrom.EditValue.ToString());

            if(lkp_DrawerTo.EditValue != null)
            {
                txt_DrawerToBalance .Text  =( from a in db.Acc_Activities
                                     from d in db.Acc_ActivityDetials.Where(x => x.AcivityID == a.ID)
                                     where d.ACCID.ToString() == lkp_DrawerTo.EditValue.ToString()
                                     select d.Debit - d.Credit ). ToList().DefaultIfEmpty().Sum().ToString(); 
            }

            if (lkp_DrawerFrom.EditValue != null)
            {
                txt_DrawerFromBalance.Text = (from a in db.Acc_Activities
                                            from d in db.Acc_ActivityDetials.Where(x => x.AcivityID == a.ID)
                                            where d.ACCID.ToString() == lkp_DrawerFrom.EditValue.ToString()
                                            select d.Debit - d.Credit).ToList().DefaultIfEmpty().Sum().ToString();
            }
        }
    }
}
