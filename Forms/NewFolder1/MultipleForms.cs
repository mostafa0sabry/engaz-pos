﻿using ByStro.PL;

namespace ByStro.Forms
{
    class MultipleForms
    {

    }
    public class PayInstallmentUnPaidList : frm_PayInstallmentList
    {
        public PayInstallmentUnPaidList() : base(false)
        {

        }
    }  
    public class PayInstallmentPaidList : frm_PayInstallmentList
    {
        public PayInstallmentPaidList() : base(true)
        {

        }
    }
    public class RbtCustomer_frm : RbtCustomerSuppliers_frm
    {
        public RbtCustomer_frm() : base(true)
        {

        }
    }
    public class RbtSuppliers_frm : RbtCustomerSuppliers_frm
    {
        public RbtSuppliers_frm() : base(false)
        {

        }
    }
    public class SalesInvoiceOther : frm_Inv_InvoiceOther
    {
        public SalesInvoiceOther() : base( Clases.MasterClass.InvoiceType.SalesInvoice)
        {

        }
    }
    public class SalesReturnInvoiceOther : frm_Inv_InvoiceOther
    {
        public SalesReturnInvoiceOther() : base(Clases.MasterClass.InvoiceType.SalesReturn)
        {

        }
    }
    public class SalesReturnInvoice : frm_Inv_Invoice
    {
        public SalesReturnInvoice() : base( Clases.MasterClass.InvoiceType.SalesReturn)
        {

        }
    }
    public class NewCashNoteIn : frm_CashNote
    {
        public NewCashNoteIn() : base(true)
        {

        }
    }
    public class NewCashNoteOut : frm_CashNote
    {
        public NewCashNoteOut() : base(false)
        {

        }
    }
    public class CashNoteOutList : frm_CashNoteList
    {
        public CashNoteOutList() : base(false)
        {

        }
    }
    public class CashNoteInList : frm_CashNoteList
    {
        public CashNoteInList() : base(true)
        {

        }
    }
    public class DiningTablesOrderDoneList : frm_DiningTablesOrderList
    {
        public DiningTablesOrderDoneList() : base(true)
        {
        }
    }   
    public class DiningTablesOrderNotDoneList : frm_DiningTablesOrderList
    {
        public DiningTablesOrderNotDoneList() : base(false)
        {
        }
    }
}


