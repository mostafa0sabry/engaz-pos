﻿using DevExpress.XtraLayout;

namespace ByStro.Forms
{
    partial class frm_Stores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        LayoutControl layoutControl1;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition4 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.chk_HasAccount = new DevExpress.XtraEditors.CheckEdit();
            this.lkp_ParentID = new DevExpress.XtraEditors.LookUpEdit();
            this.tlkp_PurchaseACC = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeListLookUpEdit1TreeList = new DevExpress.XtraTreeList.TreeList();
            this.tlkp_PurchaseReturnAcc = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.txt_Phone = new DevExpress.XtraEditors.TextEdit();
            this.tlkp_SalesACC = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList2 = new DevExpress.XtraTreeList.TreeList();
            this.txt_Address = new DevExpress.XtraEditors.TextEdit();
            this.tlkpSalesDiscountAcc = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList5 = new DevExpress.XtraTreeList.TreeList();
            this.txt_City = new DevExpress.XtraEditors.TextEdit();
            this.tlkp_SalesReturnAcc = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList3 = new DevExpress.XtraTreeList.TreeList();
            this.txt_Name = new DevExpress.XtraEditors.TextEdit();
            this.tlkp_PurchaseDiscountAcc = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList4 = new DevExpress.XtraTreeList.TreeList();
            this.txt_ID = new DevExpress.XtraEditors.TextEdit();
            this.LookUpEdit_OpenInventoryAccount = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList6 = new DevExpress.XtraTreeList.TreeList();
            this.LookUpEdit_CloseInventoryAccount = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList7 = new DevExpress.XtraTreeList.TreeList();
            this.LookUpEdit_InventoryAccount = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList8 = new DevExpress.XtraTreeList.TreeList();
            this.LookUpEdit_CostOfSoldGoodsAccount = new DevExpress.XtraEditors.TreeListLookUpEdit();
            this.treeList9 = new DevExpress.XtraTreeList.TreeList();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_LookUpEdit_InventoryAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_LookUpEdit_CostOfSoldGoodsAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_LookUpEdit_CloseInventoryAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_LookUpEdit_OpenInventoryAccount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_ParentID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkp_PurchaseACC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkp_PurchaseReturnAcc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Phone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkp_SalesACC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Address.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkpSalesDiscountAcc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_City.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkp_SalesReturnAcc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkp_PurchaseDiscountAcc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_OpenInventoryAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_CloseInventoryAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_InventoryAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_CostOfSoldGoodsAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_InventoryAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_CostOfSoldGoodsAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_CloseInventoryAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_OpenInventoryAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.chk_HasAccount);
            this.layoutControl1.Controls.Add(this.lkp_ParentID);
            this.layoutControl1.Controls.Add(this.tlkp_PurchaseACC);
            this.layoutControl1.Controls.Add(this.tlkp_PurchaseReturnAcc);
            this.layoutControl1.Controls.Add(this.txt_Phone);
            this.layoutControl1.Controls.Add(this.tlkp_SalesACC);
            this.layoutControl1.Controls.Add(this.txt_Address);
            this.layoutControl1.Controls.Add(this.tlkpSalesDiscountAcc);
            this.layoutControl1.Controls.Add(this.txt_City);
            this.layoutControl1.Controls.Add(this.tlkp_SalesReturnAcc);
            this.layoutControl1.Controls.Add(this.txt_Name);
            this.layoutControl1.Controls.Add(this.tlkp_PurchaseDiscountAcc);
            this.layoutControl1.Controls.Add(this.txt_ID);
            this.layoutControl1.Controls.Add(this.LookUpEdit_OpenInventoryAccount);
            this.layoutControl1.Controls.Add(this.LookUpEdit_CloseInventoryAccount);
            this.layoutControl1.Controls.Add(this.LookUpEdit_InventoryAccount);
            this.layoutControl1.Controls.Add(this.LookUpEdit_CostOfSoldGoodsAccount);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 53);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(507, 579);
            this.layoutControl1.TabIndex = 4;
            // 
            // chk_HasAccount
            // 
            this.chk_HasAccount.Location = new System.Drawing.Point(66, 246);
            this.chk_HasAccount.MenuManager = this.barManager1;
            this.chk_HasAccount.Name = "chk_HasAccount";
            this.chk_HasAccount.Properties.Caption = "انشاء الحسابات تلقائي";
            this.chk_HasAccount.Size = new System.Drawing.Size(374, 20);
            this.chk_HasAccount.StyleController = this.layoutControl1;
            this.chk_HasAccount.TabIndex = 0;
            this.chk_HasAccount.CheckedChanged += new System.EventHandler(this.chk_HasAccount_CheckedChanged);
            // 
            // lkp_ParentID
            // 
            this.lkp_ParentID.Location = new System.Drawing.Point(66, 177);
            this.lkp_ParentID.MenuManager = this.barManager1;
            this.lkp_ParentID.Name = "lkp_ParentID";
            this.lkp_ParentID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp_ParentID.Properties.NullText = "";
            this.lkp_ParentID.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.lkp_ParentID.Size = new System.Drawing.Size(267, 20);
            this.lkp_ParentID.StyleController = this.layoutControl1;
            this.lkp_ParentID.TabIndex = 5;
            this.lkp_ParentID.EditValueChanged += new System.EventHandler(this.lkp_ParentID_EditValueChanged);
            // 
            // tlkp_PurchaseACC
            // 
            this.tlkp_PurchaseACC.Location = new System.Drawing.Point(66, 270);
            this.tlkp_PurchaseACC.MenuManager = this.barManager1;
            this.tlkp_PurchaseACC.Name = "tlkp_PurchaseACC";
            this.tlkp_PurchaseACC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tlkp_PurchaseACC.Properties.NullText = "";
            this.tlkp_PurchaseACC.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.tlkp_PurchaseACC.Properties.TreeList = this.treeListLookUpEdit1TreeList;
            this.tlkp_PurchaseACC.Size = new System.Drawing.Size(267, 20);
            this.tlkp_PurchaseACC.StyleController = this.layoutControl1;
            this.tlkp_PurchaseACC.TabIndex = 1;
            // 
            // treeListLookUpEdit1TreeList
            // 
            this.treeListLookUpEdit1TreeList.Location = new System.Drawing.Point(0, 0);
            this.treeListLookUpEdit1TreeList.Name = "treeListLookUpEdit1TreeList";
            this.treeListLookUpEdit1TreeList.OptionsView.ShowIndentAsRowStyle = true;
            this.treeListLookUpEdit1TreeList.Size = new System.Drawing.Size(400, 200);
            this.treeListLookUpEdit1TreeList.TabIndex = 0;
            // 
            // tlkp_PurchaseReturnAcc
            // 
            this.tlkp_PurchaseReturnAcc.Location = new System.Drawing.Point(66, 294);
            this.tlkp_PurchaseReturnAcc.MenuManager = this.barManager1;
            this.tlkp_PurchaseReturnAcc.Name = "tlkp_PurchaseReturnAcc";
            this.tlkp_PurchaseReturnAcc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tlkp_PurchaseReturnAcc.Properties.NullText = "";
            this.tlkp_PurchaseReturnAcc.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.tlkp_PurchaseReturnAcc.Properties.TreeList = this.treeList1;
            this.tlkp_PurchaseReturnAcc.Size = new System.Drawing.Size(267, 20);
            this.tlkp_PurchaseReturnAcc.StyleController = this.layoutControl1;
            this.tlkp_PurchaseReturnAcc.TabIndex = 2;
            // 
            // treeList1
            // 
            this.treeList1.Location = new System.Drawing.Point(185, 114);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList1.Size = new System.Drawing.Size(400, 200);
            this.treeList1.TabIndex = 0;
            // 
            // txt_Phone
            // 
            this.txt_Phone.Location = new System.Drawing.Point(66, 153);
            this.txt_Phone.Name = "txt_Phone";
            this.txt_Phone.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txt_Phone.Size = new System.Drawing.Size(267, 20);
            this.txt_Phone.StyleController = this.layoutControl1;
            this.txt_Phone.TabIndex = 4;
            // 
            // tlkp_SalesACC
            // 
            this.tlkp_SalesACC.Location = new System.Drawing.Point(66, 318);
            this.tlkp_SalesACC.MenuManager = this.barManager1;
            this.tlkp_SalesACC.Name = "tlkp_SalesACC";
            this.tlkp_SalesACC.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tlkp_SalesACC.Properties.NullText = "";
            this.tlkp_SalesACC.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.tlkp_SalesACC.Properties.TreeList = this.treeList2;
            this.tlkp_SalesACC.Size = new System.Drawing.Size(267, 20);
            this.tlkp_SalesACC.StyleController = this.layoutControl1;
            this.tlkp_SalesACC.TabIndex = 3;
            // 
            // treeList2
            // 
            this.treeList2.Location = new System.Drawing.Point(193, 122);
            this.treeList2.Name = "treeList2";
            this.treeList2.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList2.Size = new System.Drawing.Size(400, 200);
            this.treeList2.TabIndex = 0;
            // 
            // txt_Address
            // 
            this.txt_Address.Location = new System.Drawing.Point(66, 129);
            this.txt_Address.Name = "txt_Address";
            this.txt_Address.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txt_Address.Size = new System.Drawing.Size(267, 20);
            this.txt_Address.StyleController = this.layoutControl1;
            this.txt_Address.TabIndex = 3;
            // 
            // tlkpSalesDiscountAcc
            // 
            this.tlkpSalesDiscountAcc.Location = new System.Drawing.Point(66, 390);
            this.tlkpSalesDiscountAcc.MenuManager = this.barManager1;
            this.tlkpSalesDiscountAcc.Name = "tlkpSalesDiscountAcc";
            this.tlkpSalesDiscountAcc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tlkpSalesDiscountAcc.Properties.NullText = "";
            this.tlkpSalesDiscountAcc.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.tlkpSalesDiscountAcc.Properties.TreeList = this.treeList5;
            this.tlkpSalesDiscountAcc.Size = new System.Drawing.Size(267, 20);
            this.tlkpSalesDiscountAcc.StyleController = this.layoutControl1;
            this.tlkpSalesDiscountAcc.TabIndex = 6;
            // 
            // treeList5
            // 
            this.treeList5.Location = new System.Drawing.Point(217, 146);
            this.treeList5.Name = "treeList5";
            this.treeList5.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList5.Size = new System.Drawing.Size(400, 200);
            this.treeList5.TabIndex = 0;
            // 
            // txt_City
            // 
            this.txt_City.Location = new System.Drawing.Point(66, 105);
            this.txt_City.Name = "txt_City";
            this.txt_City.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txt_City.Size = new System.Drawing.Size(267, 20);
            this.txt_City.StyleController = this.layoutControl1;
            this.txt_City.TabIndex = 2;
            // 
            // tlkp_SalesReturnAcc
            // 
            this.tlkp_SalesReturnAcc.Location = new System.Drawing.Point(66, 342);
            this.tlkp_SalesReturnAcc.MenuManager = this.barManager1;
            this.tlkp_SalesReturnAcc.Name = "tlkp_SalesReturnAcc";
            this.tlkp_SalesReturnAcc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tlkp_SalesReturnAcc.Properties.NullText = "";
            this.tlkp_SalesReturnAcc.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.tlkp_SalesReturnAcc.Properties.TreeList = this.treeList3;
            this.tlkp_SalesReturnAcc.Size = new System.Drawing.Size(267, 20);
            this.tlkp_SalesReturnAcc.StyleController = this.layoutControl1;
            this.tlkp_SalesReturnAcc.TabIndex = 4;
            // 
            // treeList3
            // 
            this.treeList3.Location = new System.Drawing.Point(201, 130);
            this.treeList3.Name = "treeList3";
            this.treeList3.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList3.Size = new System.Drawing.Size(400, 200);
            this.treeList3.TabIndex = 0;
            // 
            // txt_Name
            // 
            this.txt_Name.Location = new System.Drawing.Point(66, 81);
            this.txt_Name.Name = "txt_Name";
            this.txt_Name.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txt_Name.Size = new System.Drawing.Size(267, 20);
            this.txt_Name.StyleController = this.layoutControl1;
            this.txt_Name.TabIndex = 1;
            // 
            // tlkp_PurchaseDiscountAcc
            // 
            this.tlkp_PurchaseDiscountAcc.Location = new System.Drawing.Point(66, 366);
            this.tlkp_PurchaseDiscountAcc.MenuManager = this.barManager1;
            this.tlkp_PurchaseDiscountAcc.Name = "tlkp_PurchaseDiscountAcc";
            this.tlkp_PurchaseDiscountAcc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.tlkp_PurchaseDiscountAcc.Properties.NullText = "";
            this.tlkp_PurchaseDiscountAcc.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.tlkp_PurchaseDiscountAcc.Properties.TreeList = this.treeList4;
            this.tlkp_PurchaseDiscountAcc.Size = new System.Drawing.Size(267, 20);
            this.tlkp_PurchaseDiscountAcc.StyleController = this.layoutControl1;
            this.tlkp_PurchaseDiscountAcc.TabIndex = 5;
            // 
            // treeList4
            // 
            this.treeList4.Location = new System.Drawing.Point(209, 138);
            this.treeList4.Name = "treeList4";
            this.treeList4.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList4.Size = new System.Drawing.Size(400, 200);
            this.treeList4.TabIndex = 0;
            // 
            // txt_ID
            // 
            this.txt_ID.Location = new System.Drawing.Point(66, 57);
            this.txt_ID.MenuManager = this.barManager1;
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.txt_ID.Size = new System.Drawing.Size(267, 20);
            this.txt_ID.StyleController = this.layoutControl1;
            this.txt_ID.TabIndex = 0;
            // 
            // LookUpEdit_OpenInventoryAccount
            // 
            this.LookUpEdit_OpenInventoryAccount.Location = new System.Drawing.Point(66, 462);
            this.LookUpEdit_OpenInventoryAccount.Name = "LookUpEdit_OpenInventoryAccount";
            this.LookUpEdit_OpenInventoryAccount.Properties.NullText = "";
            this.LookUpEdit_OpenInventoryAccount.Properties.TreeList = this.treeList6;
            this.LookUpEdit_OpenInventoryAccount.Size = new System.Drawing.Size(267, 20);
            this.LookUpEdit_OpenInventoryAccount.StyleController = this.layoutControl1;
            this.LookUpEdit_OpenInventoryAccount.TabIndex = 51;
            // 
            // treeList6
            // 
            this.treeList6.Location = new System.Drawing.Point(0, 0);
            this.treeList6.Name = "treeList6";
            this.treeList6.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList6.Size = new System.Drawing.Size(400, 200);
            this.treeList6.TabIndex = 0;
            // 
            // LookUpEdit_CloseInventoryAccount
            // 
            this.LookUpEdit_CloseInventoryAccount.Location = new System.Drawing.Point(66, 486);
            this.LookUpEdit_CloseInventoryAccount.Name = "LookUpEdit_CloseInventoryAccount";
            this.LookUpEdit_CloseInventoryAccount.Properties.NullText = "";
            this.LookUpEdit_CloseInventoryAccount.Properties.TreeList = this.treeList7;
            this.LookUpEdit_CloseInventoryAccount.Size = new System.Drawing.Size(267, 20);
            this.LookUpEdit_CloseInventoryAccount.StyleController = this.layoutControl1;
            this.LookUpEdit_CloseInventoryAccount.TabIndex = 52;
            // 
            // treeList7
            // 
            this.treeList7.Location = new System.Drawing.Point(0, 0);
            this.treeList7.Name = "treeList7";
            this.treeList7.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList7.Size = new System.Drawing.Size(400, 200);
            this.treeList7.TabIndex = 0;
            // 
            // LookUpEdit_InventoryAccount
            // 
            this.LookUpEdit_InventoryAccount.Location = new System.Drawing.Point(66, 414);
            this.LookUpEdit_InventoryAccount.Name = "LookUpEdit_InventoryAccount";
            this.LookUpEdit_InventoryAccount.Properties.NullText = "";
            this.LookUpEdit_InventoryAccount.Properties.TreeList = this.treeList8;
            this.LookUpEdit_InventoryAccount.Size = new System.Drawing.Size(267, 20);
            this.LookUpEdit_InventoryAccount.StyleController = this.layoutControl1;
            this.LookUpEdit_InventoryAccount.TabIndex = 40;
            // 
            // treeList8
            // 
            this.treeList8.Location = new System.Drawing.Point(0, 0);
            this.treeList8.Name = "treeList8";
            this.treeList8.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList8.Size = new System.Drawing.Size(400, 200);
            this.treeList8.TabIndex = 0;
            // 
            // LookUpEdit_CostOfSoldGoodsAccount
            // 
            this.LookUpEdit_CostOfSoldGoodsAccount.Location = new System.Drawing.Point(66, 438);
            this.LookUpEdit_CostOfSoldGoodsAccount.Name = "LookUpEdit_CostOfSoldGoodsAccount";
            this.LookUpEdit_CostOfSoldGoodsAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_CostOfSoldGoodsAccount.Properties.NullText = "";
            this.LookUpEdit_CostOfSoldGoodsAccount.Properties.TreeList = this.treeList9;
            this.LookUpEdit_CostOfSoldGoodsAccount.Size = new System.Drawing.Size(267, 20);
            this.LookUpEdit_CostOfSoldGoodsAccount.StyleController = this.layoutControl1;
            this.LookUpEdit_CostOfSoldGoodsAccount.TabIndex = 59;
            // 
            // treeList9
            // 
            this.treeList9.Location = new System.Drawing.Point(0, 0);
            this.treeList9.Name = "treeList9";
            this.treeList9.OptionsView.ShowIndentAsRowStyle = true;
            this.treeList9.Size = new System.Drawing.Size(400, 200);
            this.treeList9.TabIndex = 0;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.layoutControlGroup2});
            this.Root.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.Root.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 50D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize;
            columnDefinition2.Width = 402D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 50D;
            this.Root.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3});
            rowDefinition1.Height = 20D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 189D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition3.Height = 309D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition4.Height = 80D;
            rowDefinition4.SizeType = System.Windows.Forms.SizeType.Percent;
            this.Root.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3,
            rowDefinition4});
            this.Root.Size = new System.Drawing.Size(507, 579);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControl_LookUpEdit_InventoryAccount,
            this.layoutControl_LookUpEdit_CostOfSoldGoodsAccount,
            this.layoutControlItem13,
            this.layoutControlItem6,
            this.layoutControl_LookUpEdit_CloseInventoryAccount,
            this.layoutControl_LookUpEdit_OpenInventoryAccount});
            this.layoutControlGroup1.Location = new System.Drawing.Point(42, 201);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlGroup1.OptionsTableLayoutItem.RowIndex = 2;
            this.layoutControlGroup1.Size = new System.Drawing.Size(402, 309);
            this.layoutControlGroup1.Text = "الحسابات";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.tlkp_PurchaseReturnAcc;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem1.Text = "مردود المشتريات";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.tlkp_SalesACC;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem2.Text = "المبيعات";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.tlkp_SalesReturnAcc;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem3.Text = "مردود مبيعات";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.tlkp_PurchaseDiscountAcc;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem4.Text = "خصم نقدي مكتسب";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.tlkpSalesDiscountAcc;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem5.Text = "خصم نقدي مسموح به";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControl_LookUpEdit_InventoryAccount
            // 
            this.layoutControl_LookUpEdit_InventoryAccount.Control = this.LookUpEdit_InventoryAccount;
            this.layoutControl_LookUpEdit_InventoryAccount.CustomizationFormText = "حساب المخزون";
            this.layoutControl_LookUpEdit_InventoryAccount.Location = new System.Drawing.Point(0, 168);
            this.layoutControl_LookUpEdit_InventoryAccount.Name = "layoutControl_LookUpEdit_InventoryAccount";
            this.layoutControl_LookUpEdit_InventoryAccount.Size = new System.Drawing.Size(378, 24);
            this.layoutControl_LookUpEdit_InventoryAccount.Text = "حساب المخزون";
            this.layoutControl_LookUpEdit_InventoryAccount.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControl_LookUpEdit_CostOfSoldGoodsAccount
            // 
            this.layoutControl_LookUpEdit_CostOfSoldGoodsAccount.Control = this.LookUpEdit_CostOfSoldGoodsAccount;
            this.layoutControl_LookUpEdit_CostOfSoldGoodsAccount.CustomizationFormText = "تكلفه البضاعه المباعه";
            this.layoutControl_LookUpEdit_CostOfSoldGoodsAccount.Location = new System.Drawing.Point(0, 192);
            this.layoutControl_LookUpEdit_CostOfSoldGoodsAccount.Name = "layoutControl_LookUpEdit_CostOfSoldGoodsAccount";
            this.layoutControl_LookUpEdit_CostOfSoldGoodsAccount.Size = new System.Drawing.Size(378, 24);
            this.layoutControl_LookUpEdit_CostOfSoldGoodsAccount.Text = "تكلفه البضاعه المباعه";
            this.layoutControl_LookUpEdit_CostOfSoldGoodsAccount.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.chk_HasAccount;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.tlkp_PurchaseACC;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem6.Text = "المشتريات";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControl_LookUpEdit_CloseInventoryAccount
            // 
            this.layoutControl_LookUpEdit_CloseInventoryAccount.Control = this.LookUpEdit_CloseInventoryAccount;
            this.layoutControl_LookUpEdit_CloseInventoryAccount.CustomizationFormText = "بضاعه نهايه المده";
            this.layoutControl_LookUpEdit_CloseInventoryAccount.Location = new System.Drawing.Point(0, 240);
            this.layoutControl_LookUpEdit_CloseInventoryAccount.Name = "layoutControl_LookUpEdit_CloseInventoryAccount";
            this.layoutControl_LookUpEdit_CloseInventoryAccount.Size = new System.Drawing.Size(378, 24);
            this.layoutControl_LookUpEdit_CloseInventoryAccount.Text = "بضاعه نهايه المده";
            this.layoutControl_LookUpEdit_CloseInventoryAccount.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControl_LookUpEdit_OpenInventoryAccount
            // 
            this.layoutControl_LookUpEdit_OpenInventoryAccount.Control = this.LookUpEdit_OpenInventoryAccount;
            this.layoutControl_LookUpEdit_OpenInventoryAccount.CustomizationFormText = "بضاعه اول المده";
            this.layoutControl_LookUpEdit_OpenInventoryAccount.Location = new System.Drawing.Point(0, 216);
            this.layoutControl_LookUpEdit_OpenInventoryAccount.Name = "layoutControl_LookUpEdit_OpenInventoryAccount";
            this.layoutControl_LookUpEdit_OpenInventoryAccount.Size = new System.Drawing.Size(378, 24);
            this.layoutControl_LookUpEdit_OpenInventoryAccount.Text = "بضاعه اول المده";
            this.layoutControl_LookUpEdit_OpenInventoryAccount.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem7,
            this.layoutControlItem12});
            this.layoutControlGroup2.Location = new System.Drawing.Point(42, 12);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlGroup2.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlGroup2.Size = new System.Drawing.Size(402, 189);
            this.layoutControlGroup2.Text = "بيانات اساسيه";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txt_ID;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(450, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "كود";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txt_Name;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem9.Text = "الاسم";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txt_City;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem10.Text = "المدينه";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txt_Address;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem11.Text = "العنوان";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lkp_ParentID;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem7.Text = "تابع لمخزن";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(104, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txt_Phone;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(378, 24);
            this.layoutControlItem12.Text = "الهاتف";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(104, 13);
            // 
            // frm_Stores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 632);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.MinimumSize = new System.Drawing.Size(473, 634);
            this.Name = "frm_Stores";
            this.RightToLeftLayout = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation;
            this.Text = "المخازن";
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_ParentID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkp_PurchaseACC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeListLookUpEdit1TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkp_PurchaseReturnAcc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Phone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkp_SalesACC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Address.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkpSalesDiscountAcc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_City.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkp_SalesReturnAcc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlkp_PurchaseDiscountAcc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_OpenInventoryAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_CloseInventoryAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_InventoryAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_CostOfSoldGoodsAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_InventoryAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_CostOfSoldGoodsAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_CloseInventoryAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_OpenInventoryAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txt_ID;
        private DevExpress.XtraEditors.TextEdit txt_Name;
        private DevExpress.XtraEditors.TextEdit txt_City;
        private DevExpress.XtraEditors.TextEdit txt_Address;
        private DevExpress.XtraEditors.TextEdit txt_Phone;
        private DevExpress.XtraEditors.TreeListLookUpEdit tlkp_PurchaseACC;
        private DevExpress.XtraTreeList.TreeList treeListLookUpEdit1TreeList;
        private DevExpress.XtraEditors.TreeListLookUpEdit tlkp_PurchaseReturnAcc;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.XtraEditors.TreeListLookUpEdit tlkp_SalesACC;
        private DevExpress.XtraTreeList.TreeList treeList2;
        private DevExpress.XtraEditors.TreeListLookUpEdit tlkp_SalesReturnAcc;
        private DevExpress.XtraTreeList.TreeList treeList3;
        private DevExpress.XtraEditors.TreeListLookUpEdit tlkp_PurchaseDiscountAcc;
        private DevExpress.XtraTreeList.TreeList treeList4;
        private DevExpress.XtraEditors.TreeListLookUpEdit tlkpSalesDiscountAcc;
        private DevExpress.XtraTreeList.TreeList treeList5;
        private DevExpress.XtraEditors.LookUpEdit lkp_ParentID;
        private DevExpress.XtraEditors.CheckEdit chk_HasAccount;
        private LayoutControlGroup Root;
        private LayoutControlGroup layoutControlGroup1;
        private LayoutControlItem layoutControlItem1;
        private LayoutControlItem layoutControlItem2;
        private LayoutControlItem layoutControlItem3;
        private LayoutControlItem layoutControlItem4;
        private LayoutControlItem layoutControlItem5;
        private LayoutControlItem layoutControl_LookUpEdit_InventoryAccount;
        private LayoutControlItem layoutControl_LookUpEdit_CostOfSoldGoodsAccount;
        private LayoutControlItem layoutControlItem6;
        private LayoutControlItem layoutControlItem13;
        private LayoutControlGroup layoutControlGroup2;
        private LayoutControlItem layoutControlItem8;
        private LayoutControlItem layoutControlItem9;
        private LayoutControlItem layoutControlItem10;
        private LayoutControlItem layoutControlItem11;
        private LayoutControlItem layoutControlItem7;
        private LayoutControlItem layoutControlItem12;
        private LayoutControlItem layoutControl_LookUpEdit_CloseInventoryAccount;
        private LayoutControlItem layoutControl_LookUpEdit_OpenInventoryAccount;
        private DevExpress.XtraEditors.TreeListLookUpEdit LookUpEdit_OpenInventoryAccount;
        private DevExpress.XtraTreeList.TreeList treeList6;
        private DevExpress.XtraEditors.TreeListLookUpEdit LookUpEdit_CloseInventoryAccount;
        private DevExpress.XtraTreeList.TreeList treeList7;
        private DevExpress.XtraEditors.TreeListLookUpEdit LookUpEdit_InventoryAccount;
        private DevExpress.XtraTreeList.TreeList treeList8;
        private DevExpress.XtraEditors.TreeListLookUpEdit LookUpEdit_CostOfSoldGoodsAccount;
        private DevExpress.XtraTreeList.TreeList treeList9;
    }
}