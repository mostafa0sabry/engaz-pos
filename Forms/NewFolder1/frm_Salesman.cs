﻿using ByStro.Clases;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_Salesman : frm_Master
    {
        DAL.Salesman  salesman;

        public frm_Salesman()
        {
            InitializeComponent();
            New();
        } 
        public frm_Salesman(int id)
        {
            InitializeComponent();
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                salesman = db.Salesmans.FirstOrDefault(x=>x.ID==id);
                GetData();
            }
        }
        public override void New()
        {
            salesman = new DAL.Salesman();
            IsNew = true;
            GetData();
            base.New();
        }
        void SetData()
        {
            salesman.Name = textEditName.Text;
            salesman.Address = textEditAddress.Text;
            salesman.Phone = textEditPhone.Text;
            salesman.Commission = double.Parse(spinEditCommission.EditValue.ToString());
        }
        void GetData()
        {
            textEditName.Text = salesman.Name;
            textEditAddress.Text = salesman.Address;
            textEditPhone.Text = salesman.Phone;
            spinEditCommission.EditValue = salesman.Commission;
        }
        public override void Save()
        {
            if (CanSave() == false) return;
            if (!ValidData()) { return; }
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            if (IsNew)
            {
                SetData();
                salesman.AccountID = (int)Master.InsertNewAccount(salesman.Name, AccountTemplate.Accounts.CommissionSalesmanAccount.ID);
                db.Salesmans.InsertOnSubmit(salesman);
            }
            else
            {
                salesman = db.Salesmans.Where(s => s.ID == salesman.ID).First();
                SetData();
                Master.UpdateAccount(salesman.AccountID, salesman.Name);
            }
            db.SubmitChanges();
            CurrentSession.UserAccessbileSalesman = db.Salesmans.ToList();
            base.Save();
        }
        public override void Delete()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            if (!CanPerformDelete()) return;
            if (IsNew) return;
            PartNumber = salesman.ID.ToString();
            PartName = salesman.Name;
            if (Master.AskForDelete(this, IsNew, PartName, PartNumber))
            {
                salesman = db.Salesmans.Where(c => c.ID == salesman.ID).First();
                db.Salesmans.DeleteOnSubmit(salesman);                 
                db.SubmitChanges();
                base.Delete();
                New();
            }

        }


        private bool ValidData()
        {
            if (CheckIfNameIsUsed(this.textEditName.Text))
            {
                this.textEditName.ErrorText = LangResource.ErorrThisNameIsUsedBefore;
                this.textEditName.Focus();
                return false;
            }
            return CheckErrorText(textEditName, textEditAddress, textEditPhone, spinEditCommission);
        }
        public bool CheckIfNameIsUsed(string Name)
        {
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var obj = (from s in db.Salesmans where salesman.ID != s.ID && s.Name == Name select s.ID).ToList();
                return (obj.Count() > 0);
            }
            
        }

        private void frm_Salesman_Load(object sender, EventArgs e)
        {
            btn_Refresh.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Print.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;

        }
    }
}
