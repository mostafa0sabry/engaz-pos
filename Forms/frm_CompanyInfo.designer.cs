﻿namespace ByStro.Forms
{
    partial class frm_CompanyInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_CompanyInfo));
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DateEdit_FYearEnd = new DevExpress.XtraEditors.DateEdit();
            this.DateEdit_FYearStart = new DevExpress.XtraEditors.DateEdit();
            this.txt_TaxCardNumber = new DevExpress.XtraEditors.TextEdit();
            this.txt_CBookNumber = new DevExpress.XtraEditors.TextEdit();
            this.txt_Mobile = new DevExpress.XtraEditors.TextEdit();
            this.txt_Phone = new DevExpress.XtraEditors.TextEdit();
            this.txt_Address = new DevExpress.XtraEditors.TextEdit();
            this.txt_CompanyCity = new DevExpress.XtraEditors.TextEdit();
            this.txt_CompanyName = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit_Logo = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_FYearEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_FYearEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_FYearStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_FYearStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TaxCardNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CBookNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Mobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Phone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Address.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CompanyCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CompanyName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit_Logo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton1
            // 
            resources.ApplyResources(this.simpleButton1, "simpleButton1");
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // DateEdit_FYearEnd
            // 
            resources.ApplyResources(this.DateEdit_FYearEnd, "DateEdit_FYearEnd");
            this.DateEdit_FYearEnd.Name = "DateEdit_FYearEnd";
            this.DateEdit_FYearEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("DateEdit_FYearEnd.Properties.Buttons"))))});
            this.DateEdit_FYearEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("DateEdit_FYearEnd.Properties.CalendarTimeProperties.Buttons"))))});
            this.DateEdit_FYearEnd.Properties.CalendarTimeProperties.Mask.EditMask = resources.GetString("DateEdit_FYearEnd.Properties.CalendarTimeProperties.Mask.EditMask");
            this.DateEdit_FYearEnd.Properties.CalendarTimeProperties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.DateEdit_FYearEnd.Properties.Mask.EditMask = resources.GetString("DateEdit_FYearEnd.Properties.Mask.EditMask");
            this.DateEdit_FYearEnd.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // DateEdit_FYearStart
            // 
            resources.ApplyResources(this.DateEdit_FYearStart, "DateEdit_FYearStart");
            this.DateEdit_FYearStart.Name = "DateEdit_FYearStart";
            this.DateEdit_FYearStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("DateEdit_FYearStart.Properties.Buttons"))))});
            this.DateEdit_FYearStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("DateEdit_FYearStart.Properties.CalendarTimeProperties.Buttons"))))});
            this.DateEdit_FYearStart.Properties.CalendarTimeProperties.Mask.EditMask = resources.GetString("DateEdit_FYearStart.Properties.CalendarTimeProperties.Mask.EditMask");
            this.DateEdit_FYearStart.Properties.CalendarTimeProperties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            this.DateEdit_FYearStart.Properties.Mask.EditMask = resources.GetString("DateEdit_FYearStart.Properties.Mask.EditMask");
            this.DateEdit_FYearStart.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // txt_TaxCardNumber
            // 
            resources.ApplyResources(this.txt_TaxCardNumber, "txt_TaxCardNumber");
            this.txt_TaxCardNumber.Name = "txt_TaxCardNumber";
            this.txt_TaxCardNumber.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // txt_CBookNumber
            // 
            resources.ApplyResources(this.txt_CBookNumber, "txt_CBookNumber");
            this.txt_CBookNumber.Name = "txt_CBookNumber";
            this.txt_CBookNumber.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // txt_Mobile
            // 
            resources.ApplyResources(this.txt_Mobile, "txt_Mobile");
            this.txt_Mobile.Name = "txt_Mobile";
            this.txt_Mobile.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // txt_Phone
            // 
            resources.ApplyResources(this.txt_Phone, "txt_Phone");
            this.txt_Phone.Name = "txt_Phone";
            this.txt_Phone.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // txt_Address
            // 
            resources.ApplyResources(this.txt_Address, "txt_Address");
            this.txt_Address.Name = "txt_Address";
            this.txt_Address.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // txt_CompanyCity
            // 
            resources.ApplyResources(this.txt_CompanyCity, "txt_CompanyCity");
            this.txt_CompanyCity.Name = "txt_CompanyCity";
            this.txt_CompanyCity.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // txt_CompanyName
            // 
            resources.ApplyResources(this.txt_CompanyName, "txt_CompanyName");
            this.txt_CompanyName.Name = "txt_CompanyName";
            this.txt_CompanyName.Properties.ShowNullValuePrompt = ((DevExpress.XtraEditors.ShowNullValuePromptOptions)((DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorFocused | DevExpress.XtraEditors.ShowNullValuePromptOptions.EditorReadOnly)));
            // 
            // pictureEdit_Logo
            // 
            resources.ApplyResources(this.pictureEdit_Logo, "pictureEdit_Logo");
            this.pictureEdit_Logo.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit_Logo.Name = "pictureEdit_Logo";
            this.pictureEdit_Logo.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pictureEdit_Logo.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            // 
            // frm_CompanyInfo
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DateEdit_FYearEnd);
            this.Controls.Add(this.DateEdit_FYearStart);
            this.Controls.Add(this.txt_TaxCardNumber);
            this.Controls.Add(this.txt_CBookNumber);
            this.Controls.Add(this.txt_Mobile);
            this.Controls.Add(this.txt_Phone);
            this.Controls.Add(this.txt_Address);
            this.Controls.Add(this.txt_CompanyCity);
            this.Controls.Add(this.txt_CompanyName);
            this.Controls.Add(this.pictureEdit_Logo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frm_CompanyInfo";
            this.Load += new System.EventHandler(this.frm_CompanyInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_FYearEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_FYearEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_FYearStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_FYearStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_TaxCardNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CBookNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Mobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Phone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Address.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CompanyCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_CompanyName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit_Logo.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.DateEdit DateEdit_FYearEnd;
        private DevExpress.XtraEditors.DateEdit DateEdit_FYearStart;
        private DevExpress.XtraEditors.TextEdit txt_TaxCardNumber;
        private DevExpress.XtraEditors.TextEdit txt_CBookNumber;
        private DevExpress.XtraEditors.TextEdit txt_Mobile;
        private DevExpress.XtraEditors.TextEdit txt_Phone;
        private DevExpress.XtraEditors.TextEdit txt_Address;
        private DevExpress.XtraEditors.TextEdit txt_CompanyCity;
        private DevExpress.XtraEditors.TextEdit txt_CompanyName;
        private DevExpress.XtraEditors.PictureEdit pictureEdit_Logo;
    }
}