﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.DataAccess.Excel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraSplashScreen;
using DevExpress.SpreadsheetSource;
using System.Threading;
using DevExpress.XtraWaitForm;
using ByStro.UControle;
using static ByStro.Forms.frm_AddProductsRange;

namespace ByStro.Forms
{
    public partial class frm_ImportFromExcel : XtraForm
    {
        string DefualtUnit, DefualtCompany;
        int DefualtGroup;
        BindingList<frm_AddProductsRange.Product> productsList;
        public frm_ImportFromExcel(ref BindingList<frm_AddProductsRange.Product> _productsList, string _DefualtUnit, string _DefualtCompany, int _DefualtGroup)
        {
            InitializeComponent();
            DefualtUnit = _DefualtUnit;
            DefualtCompany  = _DefualtCompany;
            DefualtGroup = _DefualtGroup;
            productsList = _productsList;
        }

        private void textEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            XtraOpenFileDialog dialog = new XtraOpenFileDialog();
            dialog.Filter = "Excel File(*.xls)|*.xls|Excel File(*.xlsx)|*.xlsx";
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            textEdit1.Text = dialog.FileName;
        }

        private void cb_Sheets_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_Sheets.SelectedIndex >= 0 && cb_Sheets.Text != string.Empty)
            {
                try
                {
                    gridControl1.DataSource = null;
                    gridView1.Columns.Clear();

                    ExcelDataSource ds = new ExcelDataSource();
                    ds.FileName = textEdit1.Text;
                    DevExpress.DataAccess.Excel.ExcelSourceOptions excelSourceOptions1 = new DevExpress.DataAccess.Excel.ExcelSourceOptions();
                    DevExpress.DataAccess.Excel.ExcelWorksheetSettings excelWorksheetSettings1 = new DevExpress.DataAccess.Excel.ExcelWorksheetSettings();
                    excelWorksheetSettings1.WorksheetName = cb_Sheets.Text;
                    excelSourceOptions1.ImportSettings = excelWorksheetSettings1;
                    ds.SourceOptions = excelSourceOptions1;
                    ds.Fill();

                    gridControl1.DataSource = ds;
                    gridView1.PopulateColumns();

                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                cb_Sheets.Properties.Items.Clear();
                cb_Sheets.Properties.Items.AddRange(GetExcelSheetNames(textEdit1.Text));

            }
            catch
            {

            }


        }

        public static string[] GetExcelSheetNames(string filelocation)
        {  
            using (ISpreadsheetSource spreadsheetSource = SpreadsheetSourceFactory.CreateSource(filelocation))
            {
                IWorksheetCollection worksheetCollection = spreadsheetSource.Worksheets;
                return worksheetCollection.Select(x => x.Name).ToArray();
            } 
        }

        private void gridControl1_DataSourceChanged(object sender, EventArgs e)
        {
            cb_Balance.Properties.Items.Clear();
            cb_Barcode.Properties.Items.Clear();
            cb_BuyPrice.Properties.Items.Clear();
            cb_Company .Properties.Items.Clear();
            cb_Group.Properties.Items.Clear();
            cb_Name.Properties.Items.Clear();
            cb_SellPrice.Properties.Items.Clear();
            cb_UOM.Properties.Items.Clear();

            cb_Balance.Properties.Items.Add("");
            cb_Barcode.Properties.Items.Add("");
            cb_BuyPrice.Properties.Items.Add("");
            cb_Group.Properties.Items.Add("");
            cb_Company.Properties.Items.Add("");
            cb_Name.Properties.Items.Add("");
            cb_SellPrice.Properties.Items.Add("");
            cb_UOM.Properties.Items.Add("");
            foreach (GridColumn column in gridView1.Columns)
            {


                cb_Balance.Properties.Items.Add(column.FieldName);
                cb_Barcode.Properties.Items.Add(column.FieldName);
                cb_BuyPrice.Properties.Items.Add(column.FieldName);
                cb_Group.Properties.Items.Add(column.FieldName);
                cb_Company.Properties.Items.Add(column.FieldName);
                cb_Name.Properties.Items.Add(column.FieldName);
                cb_SellPrice.Properties.Items.Add(column.FieldName);
                cb_UOM.Properties.Items.Add(column.FieldName);

            
            }
        }
  void StartImporting()
        {
            SplashScreenManager.ShowForm(this, typeof(WaitForm1), true, true, false);
            if (cb_Name.SelectedIndex <= 0)
            {
                cb_Name.ErrorText = "هذا الحقل مطلوب";
                return;
            }

            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            for (int i = Convert.ToInt32(spn_Start.EditValue) - 1; i < gridView1.RowCount; i++)
            {
                if (gridView1.GetRowCellValue(i, cb_Name.Text) != null && gridView1.GetRowCellValue(i, cb_Name.Text) != DBNull.Value &&
                        gridView1.GetRowCellValue(i, cb_Name.Text).ToString().Trim() != "")
                {
                    SplashScreenManager.Default.SetWaitFormDescription(i.ToString() + "%");



                    double Balance = 0;
                    double BuyPrice = 0;
                    double SellPrice = 0;

                    string Barcode = "";
                    string uom = "";
                    string company = "";


                    if (cb_Balance.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Balance.Text) !=null )
                        double.TryParse(gridView1.GetRowCellValue(i, cb_Balance.Text).ToString(), out Balance);
                    if (cb_BuyPrice.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_BuyPrice.Text) != null)
                        double.TryParse(gridView1.GetRowCellValue(i, cb_BuyPrice.Text).ToString(), out BuyPrice);
                    if (cb_SellPrice.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_SellPrice.Text) != null)
                        double.TryParse(gridView1.GetRowCellValue(i, cb_SellPrice.Text).ToString(), out SellPrice);
                    if (cb_Barcode.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Barcode.Text) != null)
                        Barcode = gridView1.GetRowCellValue(i, cb_Barcode.Text).ToString();
                    if (cb_UOM.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_UOM.Text) != null)
                        uom = gridView1.GetRowCellValue(i, cb_UOM.Text).ToString();
                    if (cb_Company.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Company.Text) != null)
                        company = gridView1.GetRowCellValue(i, cb_Company.Text).ToString();



                    uom = (string.IsNullOrEmpty(uom)) ? DefualtUnit : uom;
                    company = (string.IsNullOrEmpty(company)) ? DefualtCompany : company;
                    Product product = new Product()
                    {
                        Name = gridView1.GetRowCellValue(i, cb_Name.Text).ToString(),
                        OpenBalance = Balance,
                        Barcode = Barcode,
                        BuyPrice = BuyPrice,
                        Price = SellPrice,
                        Category = DefualtGroup,
                        Unit = uom,
                        Company = company,

                    };

                    if (cb_Group.SelectedIndex > 0 && gridView1.GetRowCellValue(i, cb_Group.Text) != null)
                    {
                        var groups = db.Categories.Where(x => x.CategoryName.Trim() == gridView1.GetRowCellValue(i, cb_Group.Text).ToString().Trim()).ToList();
                        int GroupID=0;
                        if(groups.FirstOrDefault() !=null )
                         GroupID = groups.FirstOrDefault().CategoryID;

                        DAL.Category Group = new DAL.Category()
                        {
                            CategoryName = gridView1.GetRowCellValue(i, cb_Group.Text).ToString(),
                            Remark = gridView1.GetRowCellValue(i, cb_Group.Text).ToString(),
                            CategoryID = GetNextCategoryID(),
                        };
                        if (groups.Count() == 0)
                        {
                            db.Categories.InsertOnSubmit(Group);
                            db.SubmitChanges();
                            GroupID = Group.CategoryID;
                        }
                        product.Category = GroupID;

                    }
                    productsList.Add(product);

                }
            }
            SplashScreenManager.CloseForm(false);
            this.Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
             StartImporting();
        }

        int CurrentID = 0;
        int GetNextCategoryID()
        {
            if (CurrentID == 0)
                using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
                    CurrentID = db.Categories .Max(x => (int?)x.CategoryID  ?? 0);

            CurrentID++;
            return CurrentID;


        }
    }
}