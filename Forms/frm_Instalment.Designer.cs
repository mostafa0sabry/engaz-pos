﻿namespace ByStro.Forms
{
    partial class frm_Instalment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code
        private void InitializeComponent()
        {
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition3 = new DevExpress.XtraLayout.RowDefinition();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.spinEdit_AdvancedPay = new DevExpress.XtraEditors.SpinEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TextEdit_ID = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_Code = new DevExpress.XtraEditors.TextEdit();
            this.DateEdit_Date = new DevExpress.XtraEditors.DateEdit();
            this.DateEdit_StartDate = new DevExpress.XtraEditors.DateEdit();
            this.SpinEdit_Amount = new DevExpress.XtraEditors.SpinEdit();
            this.SpinEdit_InstalmentsCount = new DevExpress.XtraEditors.SpinEdit();
            this.CheckEdit_ISPaid = new DevExpress.XtraEditors.CheckEdit();
            this.MemoEdit_Notes = new DevExpress.XtraEditors.MemoEdit();
            this.LookUpEdit_Invoice = new DevExpress.XtraEditors.LookUpEdit();
            this.SpinEdit_Net = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit_BenfietValu = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit_TotalBeforeAdvanced = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit_InstallPeriod = new DevExpress.XtraEditors.SpinEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControl_MemoEdit_Notes = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControl_LookUpEdit_Customer = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_DateEdit_Date = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_LookUpEdit_Invoice = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_CheckEdit_ISPaid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_SpinEdit_Amount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_DateEdit_StartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_TextEdit_ID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_SpinEdit_InstalmentsCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_TextEdit_Code = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControl_SpinEdit_Amount1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LookUpEdit_Customer = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit_AdvancedPay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_StartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_StartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_Amount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_InstalmentsCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ISPaid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoEdit_Notes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Invoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_Net.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit_BenfietValu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit_TotalBeforeAdvanced.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit_InstallPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_MemoEdit_Notes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_Customer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_DateEdit_Date)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_Invoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_CheckEdit_ISPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_SpinEdit_Amount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_DateEdit_StartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_TextEdit_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_SpinEdit_InstalmentsCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_TextEdit_Code)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_SpinEdit_Amount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Customer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.comboBoxEdit1);
            this.layoutControl1.Controls.Add(this.spinEdit_AdvancedPay);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.TextEdit_ID);
            this.layoutControl1.Controls.Add(this.TextEdit_Code);
            this.layoutControl1.Controls.Add(this.DateEdit_Date);
            this.layoutControl1.Controls.Add(this.DateEdit_StartDate);
            this.layoutControl1.Controls.Add(this.SpinEdit_Amount);
            this.layoutControl1.Controls.Add(this.SpinEdit_InstalmentsCount);
            this.layoutControl1.Controls.Add(this.CheckEdit_ISPaid);
            this.layoutControl1.Controls.Add(this.MemoEdit_Notes);
            this.layoutControl1.Controls.Add(this.LookUpEdit_Invoice);
            this.layoutControl1.Controls.Add(this.SpinEdit_Net);
            this.layoutControl1.Controls.Add(this.spinEdit_BenfietValu);
            this.layoutControl1.Controls.Add(this.spinEdit_TotalBeforeAdvanced);
            this.layoutControl1.Controls.Add(this.spinEdit_InstallPeriod);
            this.layoutControl1.Controls.Add(this.LookUpEdit_Customer);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 24);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.layoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(506, 713);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(245, 349);
            this.comboBoxEdit1.MenuManager = this.barManager1;
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Properties.Items.AddRange(new object[] {
            "يوم",
            "شهر",
            "سنه"});
            this.comboBoxEdit1.Size = new System.Drawing.Size(95, 20);
            this.comboBoxEdit1.StyleController = this.layoutControl1;
            this.comboBoxEdit1.TabIndex = 20;
            this.comboBoxEdit1.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit1_SelectedIndexChanged);
            // 
            // spinEdit_AdvancedPay
            // 
            this.spinEdit_AdvancedPay.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit_AdvancedPay.Location = new System.Drawing.Point(245, 229);
            this.spinEdit_AdvancedPay.MenuManager = this.barManager1;
            this.spinEdit_AdvancedPay.Name = "spinEdit_AdvancedPay";
            this.spinEdit_AdvancedPay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit_AdvancedPay.Size = new System.Drawing.Size(152, 20);
            this.spinEdit_AdvancedPay.StyleController = this.layoutControl1;
            this.spinEdit_AdvancedPay.TabIndex = 18;
            this.spinEdit_AdvancedPay.EditValueChanged += new System.EventHandler(this.spinEdit_BenfietValu_EditValueChanged);
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(30, 433);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(446, 251);
            this.gridControl1.TabIndex = 15;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsCustomization.AllowColumnMoving = false;
            this.gridView1.OptionsCustomization.AllowColumnResizing = false;
            this.gridView1.OptionsCustomization.AllowFilter = false;
            this.gridView1.OptionsCustomization.AllowGroup = false;
            this.gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView1.OptionsCustomization.AllowSort = false;
            this.gridView1.OptionsFind.AllowFindPanel = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // TextEdit_ID
            // 
            this.TextEdit_ID.Location = new System.Drawing.Point(321, 79);
            this.TextEdit_ID.Name = "TextEdit_ID";
            this.TextEdit_ID.Properties.ReadOnly = true;
            this.TextEdit_ID.Size = new System.Drawing.Size(76, 20);
            this.TextEdit_ID.StyleController = this.layoutControl1;
            this.TextEdit_ID.TabIndex = 4;
            // 
            // TextEdit_Code
            // 
            this.TextEdit_Code.Location = new System.Drawing.Point(245, 79);
            this.TextEdit_Code.Name = "TextEdit_Code";
            this.TextEdit_Code.Size = new System.Drawing.Size(72, 20);
            this.TextEdit_Code.StyleController = this.layoutControl1;
            this.TextEdit_Code.TabIndex = 5;
            // 
            // DateEdit_Date
            // 
            this.DateEdit_Date.EditValue = new System.DateTime(2020, 2, 9, 0, 0, 0, 0);
            this.DateEdit_Date.Location = new System.Drawing.Point(245, 133);
            this.DateEdit_Date.Name = "DateEdit_Date";
            this.DateEdit_Date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_Date.Size = new System.Drawing.Size(152, 20);
            this.DateEdit_Date.StyleController = this.layoutControl1;
            this.DateEdit_Date.TabIndex = 6;
            // 
            // DateEdit_StartDate
            // 
            this.DateEdit_StartDate.EditValue = new System.DateTime(2020, 2, 9, 0, 0, 0, 0);
            this.DateEdit_StartDate.Location = new System.Drawing.Point(245, 325);
            this.DateEdit_StartDate.Name = "DateEdit_StartDate";
            this.DateEdit_StartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEdit_StartDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_StartDate.Size = new System.Drawing.Size(152, 20);
            this.DateEdit_StartDate.StyleController = this.layoutControl1;
            this.DateEdit_StartDate.TabIndex = 8;
            this.DateEdit_StartDate.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.DateEdit_StartDate_EditValueChanging);
            // 
            // SpinEdit_Amount
            // 
            this.SpinEdit_Amount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_Amount.Enabled = false;
            this.SpinEdit_Amount.Location = new System.Drawing.Point(245, 301);
            this.SpinEdit_Amount.Name = "SpinEdit_Amount";
            this.SpinEdit_Amount.Size = new System.Drawing.Size(152, 20);
            this.SpinEdit_Amount.StyleController = this.layoutControl1;
            this.SpinEdit_Amount.TabIndex = 10;
            this.SpinEdit_Amount.EditValueChanged += new System.EventHandler(this.SpinEdit_InstalmentsCount_EditValueChanged);
            // 
            // SpinEdit_InstalmentsCount
            // 
            this.SpinEdit_InstalmentsCount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_InstalmentsCount.Location = new System.Drawing.Point(344, 373);
            this.SpinEdit_InstalmentsCount.Name = "SpinEdit_InstalmentsCount";
            this.SpinEdit_InstalmentsCount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SpinEdit_InstalmentsCount.Size = new System.Drawing.Size(53, 20);
            this.SpinEdit_InstalmentsCount.StyleController = this.layoutControl1;
            this.SpinEdit_InstalmentsCount.TabIndex = 11;
            this.SpinEdit_InstalmentsCount.EditValueChanged += new System.EventHandler(this.SpinEdit_InstalmentsCount_EditValueChanged);
            this.SpinEdit_InstalmentsCount.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.spinEdit1_EditValueChanging);
            // 
            // CheckEdit_ISPaid
            // 
            this.CheckEdit_ISPaid.Location = new System.Drawing.Point(245, 181);
            this.CheckEdit_ISPaid.Name = "CheckEdit_ISPaid";
            this.CheckEdit_ISPaid.Properties.Caption = "غير مسده";
            this.CheckEdit_ISPaid.Properties.ReadOnly = true;
            this.CheckEdit_ISPaid.Size = new System.Drawing.Size(152, 20);
            this.CheckEdit_ISPaid.StyleController = this.layoutControl1;
            this.CheckEdit_ISPaid.TabIndex = 12;
            // 
            // MemoEdit_Notes
            // 
            this.MemoEdit_Notes.Location = new System.Drawing.Point(30, 70);
            this.MemoEdit_Notes.Name = "MemoEdit_Notes";
            this.MemoEdit_Notes.Size = new System.Drawing.Size(196, 332);
            this.MemoEdit_Notes.StyleController = this.layoutControl1;
            this.MemoEdit_Notes.TabIndex = 13;
            // 
            // LookUpEdit_Invoice
            // 
            this.LookUpEdit_Invoice.Location = new System.Drawing.Point(245, 157);
            this.LookUpEdit_Invoice.Name = "LookUpEdit_Invoice";
            this.LookUpEdit_Invoice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Down)});
            this.LookUpEdit_Invoice.Properties.NullText = "";
            this.LookUpEdit_Invoice.Size = new System.Drawing.Size(152, 20);
            this.LookUpEdit_Invoice.StyleController = this.layoutControl1;
            this.LookUpEdit_Invoice.TabIndex = 14;
            this.LookUpEdit_Invoice.EditValueChanged += new System.EventHandler(this.LookUpEdit_Invoice_EditValueChanged);
            // 
            // SpinEdit_Net
            // 
            this.SpinEdit_Net.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_Net.Location = new System.Drawing.Point(245, 205);
            this.SpinEdit_Net.Name = "SpinEdit_Net";
            this.SpinEdit_Net.Size = new System.Drawing.Size(152, 20);
            this.SpinEdit_Net.StyleController = this.layoutControl1;
            this.SpinEdit_Net.TabIndex = 10;
            this.SpinEdit_Net.EditValueChanged += new System.EventHandler(this.spinEdit_BenfietValu_EditValueChanged);
            // 
            // spinEdit_BenfietValu
            // 
            this.spinEdit_BenfietValu.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit_BenfietValu.Location = new System.Drawing.Point(245, 277);
            this.spinEdit_BenfietValu.MenuManager = this.barManager1;
            this.spinEdit_BenfietValu.Name = "spinEdit_BenfietValu";
            this.spinEdit_BenfietValu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit_BenfietValu.Properties.Mask.EditMask = "p";
            this.spinEdit_BenfietValu.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEdit_BenfietValu.Size = new System.Drawing.Size(152, 20);
            this.spinEdit_BenfietValu.StyleController = this.layoutControl1;
            this.spinEdit_BenfietValu.TabIndex = 17;
            this.spinEdit_BenfietValu.EditValueChanged += new System.EventHandler(this.spinEdit_BenfietValu_EditValueChanged);
            // 
            // spinEdit_TotalBeforeAdvanced
            // 
            this.spinEdit_TotalBeforeAdvanced.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit_TotalBeforeAdvanced.Location = new System.Drawing.Point(245, 253);
            this.spinEdit_TotalBeforeAdvanced.Name = "spinEdit_TotalBeforeAdvanced";
            this.spinEdit_TotalBeforeAdvanced.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit_TotalBeforeAdvanced.Properties.ReadOnly = true;
            this.spinEdit_TotalBeforeAdvanced.Size = new System.Drawing.Size(152, 20);
            this.spinEdit_TotalBeforeAdvanced.StyleController = this.layoutControl1;
            this.spinEdit_TotalBeforeAdvanced.TabIndex = 18;
            // 
            // spinEdit_InstallPeriod
            // 
            this.spinEdit_InstallPeriod.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit_InstallPeriod.Location = new System.Drawing.Point(344, 349);
            this.spinEdit_InstallPeriod.MenuManager = this.barManager1;
            this.spinEdit_InstallPeriod.Name = "spinEdit_InstallPeriod";
            this.spinEdit_InstallPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit_InstallPeriod.Size = new System.Drawing.Size(53, 20);
            this.spinEdit_InstallPeriod.StyleController = this.layoutControl1;
            this.spinEdit_InstallPeriod.TabIndex = 19;
            this.spinEdit_InstallPeriod.EditValueChanged += new System.EventHandler(this.SpinEdit_InstalmentsCount_EditValueChanged);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1});
            this.Root.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.Root.Name = "Root";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 50D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize;
            columnDefinition2.Width = 480D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 50D;
            this.Root.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3});
            rowDefinition1.Height = 20D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            rowDefinition2.Height = 690D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition3.Height = 80D;
            rowDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            this.Root.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2,
            rowDefinition3});
            this.Root.Size = new System.Drawing.Size(506, 713);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
            this.layoutControlGroup1.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(3, 1);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.OptionsTableLayoutItem.ColumnIndex = 1;
            this.layoutControlGroup1.OptionsTableLayoutItem.RowIndex = 1;
            this.layoutControlGroup1.Size = new System.Drawing.Size(480, 690);
            this.layoutControlGroup1.Text = "ملف  تقسيط";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.layoutControlGroup2.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 363);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(456, 282);
            this.layoutControlGroup2.Text = "دفعات القسط";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(750, 450);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(450, 250);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(450, 255);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.layoutControlGroup3.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControl_MemoEdit_Notes});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(206, 363);
            this.layoutControlGroup3.Text = "الملاحظات";
            // 
            // layoutControl_MemoEdit_Notes
            // 
            this.layoutControl_MemoEdit_Notes.Control = this.MemoEdit_Notes;
            this.layoutControl_MemoEdit_Notes.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_MemoEdit_Notes.MaxSize = new System.Drawing.Size(250, 0);
            this.layoutControl_MemoEdit_Notes.MinSize = new System.Drawing.Size(120, 36);
            this.layoutControl_MemoEdit_Notes.Name = "layoutControl_MemoEdit_Notes";
            this.layoutControl_MemoEdit_Notes.Size = new System.Drawing.Size(200, 336);
            this.layoutControl_MemoEdit_Notes.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControl_MemoEdit_Notes.Text = "Notes";
            this.layoutControl_MemoEdit_Notes.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControl_MemoEdit_Notes.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControl_MemoEdit_Notes.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
            this.layoutControlGroup4.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControl_LookUpEdit_Customer,
            this.layoutControl_DateEdit_Date,
            this.layoutControl_LookUpEdit_Invoice,
            this.layoutControl_CheckEdit_ISPaid,
            this.layoutControl_SpinEdit_Amount,
            this.layoutControl_DateEdit_StartDate,
            this.layoutControl_TextEdit_ID,
            this.layoutControl_SpinEdit_InstalmentsCount,
            this.layoutControl_TextEdit_Code,
            this.simpleLabelItem2,
            this.layoutControl_SpinEdit_Amount1,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem2});
            this.layoutControlGroup4.Location = new System.Drawing.Point(206, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(250, 363);
            this.layoutControlGroup4.Text = "البيانات الاساسيه";
            // 
            // layoutControl_LookUpEdit_Customer
            // 
            this.layoutControl_LookUpEdit_Customer.Control = this.LookUpEdit_Customer;
            this.layoutControl_LookUpEdit_Customer.Location = new System.Drawing.Point(0, 24);
            this.layoutControl_LookUpEdit_Customer.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControl_LookUpEdit_Customer.Name = "layoutControl_LookUpEdit_Customer";
            this.layoutControl_LookUpEdit_Customer.Size = new System.Drawing.Size(226, 30);
            this.layoutControl_LookUpEdit_Customer.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControl_LookUpEdit_Customer.Text = "العميل";
            this.layoutControl_LookUpEdit_Customer.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControl_DateEdit_Date
            // 
            this.layoutControl_DateEdit_Date.Control = this.DateEdit_Date;
            this.layoutControl_DateEdit_Date.Location = new System.Drawing.Point(0, 54);
            this.layoutControl_DateEdit_Date.Name = "layoutControl_DateEdit_Date";
            this.layoutControl_DateEdit_Date.Size = new System.Drawing.Size(226, 24);
            this.layoutControl_DateEdit_Date.Text = "تاريخ الاضافه";
            this.layoutControl_DateEdit_Date.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControl_LookUpEdit_Invoice
            // 
            this.layoutControl_LookUpEdit_Invoice.Control = this.LookUpEdit_Invoice;
            this.layoutControl_LookUpEdit_Invoice.Location = new System.Drawing.Point(0, 78);
            this.layoutControl_LookUpEdit_Invoice.Name = "layoutControl_LookUpEdit_Invoice";
            this.layoutControl_LookUpEdit_Invoice.Size = new System.Drawing.Size(226, 24);
            this.layoutControl_LookUpEdit_Invoice.Text = "مربوط بفاتوره";
            this.layoutControl_LookUpEdit_Invoice.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControl_CheckEdit_ISPaid
            // 
            this.layoutControl_CheckEdit_ISPaid.Control = this.CheckEdit_ISPaid;
            this.layoutControl_CheckEdit_ISPaid.Location = new System.Drawing.Point(0, 102);
            this.layoutControl_CheckEdit_ISPaid.Name = "layoutControl_CheckEdit_ISPaid";
            this.layoutControl_CheckEdit_ISPaid.Size = new System.Drawing.Size(226, 24);
            this.layoutControl_CheckEdit_ISPaid.Text = "الحاله";
            this.layoutControl_CheckEdit_ISPaid.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControl_SpinEdit_Amount
            // 
            this.layoutControl_SpinEdit_Amount.Control = this.SpinEdit_Amount;
            this.layoutControl_SpinEdit_Amount.Location = new System.Drawing.Point(0, 222);
            this.layoutControl_SpinEdit_Amount.Name = "layoutControl_SpinEdit_Amount";
            this.layoutControl_SpinEdit_Amount.Size = new System.Drawing.Size(226, 24);
            this.layoutControl_SpinEdit_Amount.Text = "مبلغ التقسيط";
            this.layoutControl_SpinEdit_Amount.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControl_DateEdit_StartDate
            // 
            this.layoutControl_DateEdit_StartDate.Control = this.DateEdit_StartDate;
            this.layoutControl_DateEdit_StartDate.Location = new System.Drawing.Point(0, 246);
            this.layoutControl_DateEdit_StartDate.Name = "layoutControl_DateEdit_StartDate";
            this.layoutControl_DateEdit_StartDate.Size = new System.Drawing.Size(226, 24);
            this.layoutControl_DateEdit_StartDate.Text = "تاريخ اول قسط";
            this.layoutControl_DateEdit_StartDate.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControl_TextEdit_ID
            // 
            this.layoutControl_TextEdit_ID.Control = this.TextEdit_ID;
            this.layoutControl_TextEdit_ID.Location = new System.Drawing.Point(76, 0);
            this.layoutControl_TextEdit_ID.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControl_TextEdit_ID.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControl_TextEdit_ID.Name = "layoutControl_TextEdit_ID";
            this.layoutControl_TextEdit_ID.Size = new System.Drawing.Size(150, 24);
            this.layoutControl_TextEdit_ID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControl_TextEdit_ID.Text = "كود";
            this.layoutControl_TextEdit_ID.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControl_SpinEdit_InstalmentsCount
            // 
            this.layoutControl_SpinEdit_InstalmentsCount.Control = this.SpinEdit_InstalmentsCount;
            this.layoutControl_SpinEdit_InstalmentsCount.Location = new System.Drawing.Point(99, 294);
            this.layoutControl_SpinEdit_InstalmentsCount.Name = "layoutControl_SpinEdit_InstalmentsCount";
            this.layoutControl_SpinEdit_InstalmentsCount.Size = new System.Drawing.Size(127, 24);
            this.layoutControl_SpinEdit_InstalmentsCount.Text = "عدد الاقساط";
            this.layoutControl_SpinEdit_InstalmentsCount.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControl_TextEdit_Code
            // 
            this.layoutControl_TextEdit_Code.Control = this.TextEdit_Code;
            this.layoutControl_TextEdit_Code.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_TextEdit_Code.Name = "layoutControl_TextEdit_Code";
            this.layoutControl_TextEdit_Code.Size = new System.Drawing.Size(76, 24);
            this.layoutControl_TextEdit_Code.Text = "Code";
            this.layoutControl_TextEdit_Code.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControl_TextEdit_Code.TextVisible = false;
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 294);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Size = new System.Drawing.Size(99, 24);
            this.simpleLabelItem2.Text = "قسط";
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControl_SpinEdit_Amount1
            // 
            this.layoutControl_SpinEdit_Amount1.Control = this.SpinEdit_Net;
            this.layoutControl_SpinEdit_Amount1.ControlAlignment = System.Drawing.ContentAlignment.TopRight;
            this.layoutControl_SpinEdit_Amount1.CustomizationFormText = "المبلغ";
            this.layoutControl_SpinEdit_Amount1.Location = new System.Drawing.Point(0, 126);
            this.layoutControl_SpinEdit_Amount1.Name = "layoutControl_SpinEdit_Amount1";
            this.layoutControl_SpinEdit_Amount1.Size = new System.Drawing.Size(226, 24);
            this.layoutControl_SpinEdit_Amount1.Text = "المبلغ";
            this.layoutControl_SpinEdit_Amount1.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.spinEdit_AdvancedPay;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem4.Text = "المدفوع مقدما";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.spinEdit_InstallPeriod;
            this.layoutControlItem6.Location = new System.Drawing.Point(99, 270);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(127, 24);
            this.layoutControlItem6.Text = "مده التقسيط";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.spinEdit_BenfietValu;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 198);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem3.Text = "نسبه الفائده";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.spinEdit_TotalBeforeAdvanced;
            this.layoutControlItem5.ControlAlignment = System.Drawing.ContentAlignment.TopRight;
            this.layoutControlItem5.CustomizationFormText = "المدفوع مقدما";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 174);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem5.Text = "الاجمالي";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(67, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.comboBoxEdit1;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 270);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(99, 24);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // LookUpEdit_Customer
            // 
            this.LookUpEdit_Customer.Location = new System.Drawing.Point(245, 103);
            this.LookUpEdit_Customer.Name = "LookUpEdit_Customer";
            this.LookUpEdit_Customer.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Down)});
            this.LookUpEdit_Customer.Properties.NullText = "";
            this.LookUpEdit_Customer.Properties.PopupView = this.gridLookUpEdit1View;
            this.LookUpEdit_Customer.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.LookUpEdit_Customer.Size = new System.Drawing.Size(152, 20);
            this.LookUpEdit_Customer.StyleController = this.layoutControl1;
            this.LookUpEdit_Customer.TabIndex = 7;
            this.LookUpEdit_Customer.EditValueChanged += new System.EventHandler(this.LookUpEdit_Customer_EditValueChanged);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowAutoFilterRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // frm_Instalment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(506, 737);
            this.Controls.Add(this.layoutControl1);
            this.IconOptions.SvgImage = global::ByStro.Properties.Resources.financial;
            this.Name = "frm_Instalment";
            this.Text = "ملف قسط ";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(255)))));
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit_AdvancedPay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_StartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_StartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_Amount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_InstalmentsCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ISPaid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoEdit_Notes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Invoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_Net.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit_BenfietValu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit_TotalBeforeAdvanced.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit_InstallPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_MemoEdit_Notes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_Customer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_DateEdit_Date)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_LookUpEdit_Invoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_CheckEdit_ISPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_SpinEdit_Amount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_DateEdit_StartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_TextEdit_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_SpinEdit_InstalmentsCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_TextEdit_Code)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_SpinEdit_Amount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Customer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.TextEdit TextEdit_ID;
        private DevExpress.XtraEditors.TextEdit TextEdit_Code;
        private DevExpress.XtraEditors.DateEdit DateEdit_Date;
        private DevExpress.XtraEditors.DateEdit DateEdit_StartDate;
        private DevExpress.XtraEditors.SpinEdit SpinEdit_Amount;
        private DevExpress.XtraEditors.SpinEdit SpinEdit_InstalmentsCount;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_ISPaid;
        private DevExpress.XtraEditors.MemoEdit MemoEdit_Notes;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_Invoice;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_TextEdit_ID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_LookUpEdit_Invoice;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_CheckEdit_ISPaid;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_SpinEdit_Amount;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_DateEdit_StartDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_TextEdit_Code;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_LookUpEdit_Customer;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_DateEdit_Date;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_SpinEdit_InstalmentsCount;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_MemoEdit_Notes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
   
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private DevExpress.XtraEditors.SpinEdit SpinEdit_Net;
        private DevExpress.XtraEditors.SpinEdit spinEdit_BenfietValu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_SpinEdit_Amount1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SpinEdit spinEdit_AdvancedPay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SpinEdit spinEdit_TotalBeforeAdvanced;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SpinEdit spinEdit_InstallPeriod;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.GridLookUpEdit LookUpEdit_Customer;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
    }
}