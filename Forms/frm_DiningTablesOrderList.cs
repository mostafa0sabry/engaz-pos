﻿using ByStro.Clases;
using ByStro.DAL;
using ByStro.PL;
using DevExpress.XtraEditors.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.Forms
{
    public class frm_DiningTablesOrderList : FormLayoutList
    {
        bool IsDone;
        public frm_DiningTablesOrderList(bool _IsDone):base("ID")
        {
            IsDone = _IsDone;
        }
        public override void PopulateColumns()
        {
            base.PopulateColumns();

            this.Text = IsDone ? "اوامر طاولات المنتهيه" : "اوامر طاولات الجاريه";
            var insta = new DiningTablesOrder();
            gridView1.OptionsBehavior.Editable = false;
            gridView1.CustomColumn(nameof(insta.ID), "م", true, false);
            gridView1.CustomColumn(nameof(insta.DiningTableID), "الطاولة", true, false);
            gridView1.CustomColumn(nameof(insta.HasReturned), "تم الانتهاء", true, false);
            gridView1.CustomColumn(nameof(insta.Orders), "الفتير", false, false);
            gridView1.CustomColumn(nameof(insta.OrdersAmount), "قيمة الفواتير", true, false);
            gridView1.CustomColumn(nameof(insta.OrdersCount), "عدد الفواتير", true, false);
            gridView1.CustomColumn(nameof(insta.RecivedAmount), "المبلغ المحصل", true, false);

            gridView1.Columns[nameof(insta.RecivedAmount)].Summary.Clear();
            gridView1.Columns[nameof(insta.RecivedAmount)].Summary.Add(DevExpress.Data.SummaryItemType.Sum, nameof(insta.RecivedAmount), "الاجمالي{0}");
            gridView1.Columns[nameof(insta.OrdersCount)].Summary.Clear();
            gridView1.Columns[nameof(insta.OrdersCount)].Summary.Add(DevExpress.Data.SummaryItemType.Count, nameof(insta.OrdersCount), "العدد{0}");
            btn_Delete.Visibility =
            btn_List.Visibility =
            btn_Print.Visibility =
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Refresh.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
        }
        public override void RefreshData()
        {
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var source = db.DiningTablesOrders.Where(x => x.HasReturned == IsDone);
                gridControl1.DataSource = source.ToList();
                RepositoryItemLookUpEdit repo = new RepositoryItemLookUpEdit();
                repo.DataSource = db.DiningTables.Select(x => new { x.ID, x.Name }).ToList();
                repo.ValueMember = "ID";
                repo.DisplayMember = "Name";
                gridControl1.RepositoryItems.Add(repo);
                gridView1.Columns["DiningTableID"].ColumnEdit = repo;
            }
            base.RefreshData();

        }
        public override void New()
        {
            Main_frm.OpenForm(new frm_DiningTablesOrder(), true);
            base.New();
            RefreshData();
        }
        public override void OpenForm(int id)
        {
            Main_frm.OpenForm(new frm_DiningTablesOrder(id), true);
            base.OpenForm(id);
            RefreshData();
        }
    }
}
