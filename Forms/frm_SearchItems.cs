﻿using DevExpress.DataProcessing;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_SearchItems : DevExpress.XtraBars.FluentDesignSystem.FluentDesignForm
    {
        public static frm_SearchItems instance;
        public static frm_SearchItems Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed == true)
                    instance = new frm_SearchItems();
                return instance;
            }
            set => instance = value;
        }
        public static void ShowSearchWindow(object sender, EventArgs e)
        {
            frm_SearchItems.Instance.ShowDialog();
        }
        public frm_SearchItems()
        {
            InitializeComponent();

            this.Load += new System.EventHandler(this.frm_SearchItems_Load);
            this.TopMost = false; // To keep the window allways on top 

            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;  // 
            this.StartPosition = FormStartPosition.CenterScreen;
            gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 500;
            this.gridView1.OptionsFind.FindNullPrompt = "ابحث عن الصنف";
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsFind.ShowFindButton = false;
            this.gridView1.OptionsPrint.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowIndicator = false;


            gridControl1.ViewRegistered += GridControl1_ViewRegistered;


        }
        private void frm_SearchItems_Load(object sender, EventArgs e)
        {
            RefreshData();
            gridView1.Columns["ID"].Caption = "رقم";
            gridView1.Columns["Code"].Caption = "ياركود";
            gridView1.Columns["Name"].Caption = "اسم الصنف";
        }

        private void GridControl1_ViewRegistered(object sender, DevExpress.XtraGrid.ViewOperationEventArgs e)
        {

            var view = e.View as GridView;
            if (view != null && view.LevelName == "Units")
            {
                view.ViewCaption = "تفاصيل وحدات الصنف";
                view.OptionsView.ShowViewCaption = true;
                view.Columns["Name"].Caption = "اسم الوحدة";
                view.Columns["Price1"].Caption = "السعر الاول";
                view.Columns["Price2"].Caption = "السعر الثاني";
                view.Columns["Price3"].Caption = "السعر الثالث";
                view.Columns["Balance"].Caption = "رصيد الوحدة";
            }
        }

        void RefreshData()
        {
            using (DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var newData = (from x in db.Prodecuts
                               select new CustomItem
                               {
                                   ID = x.ProdecutID,
                                   Code = x.FiestUnitBarcode,
                                   Name = x.ProdecutName,
                                   Units = CustomUnit.GetUnits(x)
                               });
                gridControl1.DataSource = newData.ToList();
                gridView1.Columns.ForEach(x=>x.OptionsColumn.AllowEdit=false);
            }
        }


    }
    public class CustomItem
    {

        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public List<CustomUnit> Units { get; set; }

    }
    public class CustomUnit
    {
        public static string GetBalance(DAL.Prodecut x) 
        {
            StringBuilder stringBuilder = new StringBuilder();
            using (DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var logSumIN = dbc.Inv_StoreLogs.Where(s => s.ItemID == x.ProdecutID).Sum(s => s.ItemQuIN) ?? 0;
                var logSumOut = dbc.Inv_StoreLogs.Where(s => s.ItemID == x.ProdecutID).Sum(s => s.ItemQuOut) ?? 0;
                var balance = ((double)logSumIN - (double)logSumOut);
                double ThreeBalance=0, SecoundBalance = 0, FiestBalance = 0;
                double ThreeBalanceMod = 0, SecoundBalanceMod = 0;
                if (!string.IsNullOrWhiteSpace(x.ThreeUnitOperating))
                {
                    stringBuilder.Append(x.ThreeUnit);
                    ThreeBalance = balance / Convert.ToDouble(x.ThreeUnitOperating);
                    ThreeBalanceMod = balance % Convert.ToDouble(x.ThreeUnitOperating);
                    stringBuilder.Append($"({ ThreeBalance }) / ");
                }

                if (!string.IsNullOrWhiteSpace(x.SecoundUnitOperating))
                {
                    stringBuilder.Append(x.SecoundUnit);
                    if (ThreeBalance == 0)
                    {
                        SecoundBalance =(int) (balance / Convert.ToDouble(x.SecoundUnitOperating));
                        SecoundBalanceMod = balance % Convert.ToDouble(x.SecoundUnitOperating);
                        stringBuilder.Append($"({ SecoundBalance }) / ");
                    }
                    else
                    {
                        SecoundBalance = ThreeBalanceMod / Convert.ToDouble(x.SecoundUnitOperating);
                        SecoundBalanceMod = ThreeBalanceMod % Convert.ToDouble(x.SecoundUnitOperating);
                        stringBuilder.Append($"({ SecoundBalance }) / ");

                    }
                }
                if (!string.IsNullOrWhiteSpace(x.FiestUnitOperating))
                {
                    stringBuilder.Append(x.FiestUnit);
                    if (SecoundBalance == 0)
                    {
                        FiestBalance = balance / Convert.ToDouble(x.FiestUnitOperating);
                        stringBuilder.Append($"({ SecoundBalance }) / ");
                    }
                    else
                    {
                        FiestBalance = SecoundBalanceMod / Convert.ToDouble(x.FiestUnitOperating);
                        stringBuilder.Append($"({ FiestBalance }) / ");
                    }
                }
            }
            return stringBuilder.ToString();
        }
         
        public static List<CustomUnit> GetUnits(DAL.Prodecut x)
        {
            List<CustomUnit> customUnits = new List<CustomUnit>();
            using (DAL.DBDataContext dbc = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                var logSumIN = dbc.Inv_StoreLogs.Where(s => s.ItemID == x.ProdecutID).Sum(s => s.ItemQuIN) ?? 0;
                var logSumOut = dbc.Inv_StoreLogs.Where(s => s.ItemID == x.ProdecutID).Sum(s => s.ItemQuOut) ?? 0;
                var balance = ((double)logSumIN - (double)logSumOut);
                if (!string.IsNullOrWhiteSpace(x.FiestUnitOperating))
                    customUnits.Add(new CustomUnit { Balance = balance / Convert.ToDouble(x.FiestUnitOperating), Name = x.FiestUnit, Price1 = x.FiestUnitPrice1, Price2 = x.FiestUnitPrice2, Price3 = x.FiestUnitPrice3 });
                if (!string.IsNullOrWhiteSpace(x.SecoundUnitOperating))
                    customUnits.Add(new CustomUnit { Balance = balance / Convert.ToDouble(x.SecoundUnitOperating), Name = x.SecoundUnit, Price1 = x.SecoundUnitPrice1, Price2 = x.SecoundUnitPrice2, Price3 = x.SecoundUnitPrice3 });
                if (!string.IsNullOrWhiteSpace(x.ThreeUnitOperating))
                    customUnits.Add(new CustomUnit { Balance = balance / Convert.ToDouble(x.ThreeUnitOperating), Name = x.ThreeUnit, Price1 = x.ThreeUnitPrice1, Price2 = x.ThreeUnitPrice2, Price3 = x.ThreeUnitPrice3 });
                return customUnits;
            }
        }
        public string Name { get; set; }
        public double? Price1 { get; set; }
        public double? Price2 { get; set; }
        public double? Price3 { get; set; }
        public double Balance { get; set; }
    }

}
