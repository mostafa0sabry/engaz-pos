﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.IO;


namespace ByStro.RPT
{
    public partial class rpt_Master : DevExpress.XtraReports.UI.XtraReport
    {
        public static string ReportPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Bystro\\" + "\\PrintTemplets";
        public rpt_Master()
        {


        }
        public virtual void LoadData()
        {

        }

        private TopMarginBand topMarginBand1;
        private DetailBand detailBand1;
        public bool UseDefaultPrinter = true;
        public void LoadTemplete()
        {
            this.Name = this.GetType().Name;
            PrintingSystem.Document.AutoFitToPagesWidth = 1;
            RightToLeftLayout = RightToLeftLayout.Yes;
            RightToLeft = RightToLeft.Yes;
            // ShowPrintMarginsWarning = false;
            if (System.IO.File.Exists(ReportPath + "\\" + this.Name + ".xml"))
                LoadLayout(ReportPath + "\\" + this.Name + ".xml");

            RightToLeftLayout = RightToLeftLayout.Yes;
            RightToLeft = RightToLeft.Yes;

            //if (UseDefaultPrinter)
            //{
            //    if (Program.setting.DefaultPrinter != string.Empty && this.PrinterName == string.Empty)
            //        PrinterName = Program.setting.DefaultPrinter;
            //}
            //else
            //{
            //    if (Program.setting.CasherPrinter != string.Empty && this.PrinterName == string.Empty)
            //        PrinterName = Program.setting.CasherPrinter;
            //}

        }

        private void InitializeComponent()
        {
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.detailBand1 = new DevExpress.XtraReports.UI.DetailBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // detailBand1
            // 
            this.detailBand1.Name = "detailBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // rpt_Master
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.topMarginBand1,
            this.detailBand1,
            this.bottomMarginBand1});
            this.Version = "19.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        private BottomMarginBand bottomMarginBand1;
    }
}
