﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.Office.Utils;
using ByStro.ReportModules;
using System.Collections.Generic;
using ByStro.Clases;

namespace ByStro.RPT
{
    public partial class rpt_ItemInfo : DevExpress.XtraReports.UI.XtraReport
    {
        public rpt_ItemInfo()
        {
            InitializeComponent();
            //lbl_PrinDate.Text = DateTime.Now.ToString();
            //lbl_UserName.Text = CurrentSession.user.UserName;
            lbl_companyName.Text = CurrentSession.Company.CompanyName;
            lbl_Cpmpany_Phone.Text = CurrentSession.Company.CompanyPhone;

            try
            {
                xrPictureBox1.Image = MasterClass.GetImageFromByte(CurrentSession.Company.CompanyLogo.ToArray());
            }
            catch (Exception)
            {
            }
        }
        public static void ShowReport(List<ProductCardWithQty> ds)
        {
            rpt_ItemInfo rpt = new rpt_ItemInfo();
            rpt.DataSource = ds;
            rpt.DataMember = "";
            rpt.ShowRibbonPreviewDialog();
        }
    }
}
