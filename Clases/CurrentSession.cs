﻿using ByStro.DAL;
using DevExpress.Data.ODataLinq.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace ByStro 
{
    public  class CurrentSession
    {

        public static DAL.St_CompanyInfo   Company { get {
                DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
                return db.St_CompanyInfos .FirstOrDefault();

            }
        }
        public static class User
        {

            public static int DefaultStore => 1; 
            public static int ID { get; set; } 
            public static int DefaultRawStore => 1; 
            public static int DefaultDrawer => 1; 
        }
        public static DAL.SystemSetting SystemSettings
        {
            get
            {
                DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
                return db.SystemSettings.First();

            }
        } 
        public static string DTemplatePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\ERP\\PrintTemplets";
        
        public static List<DAL.Acc_Account> UserAccessbileAccounts = new List<DAL.Acc_Account>();
        public static List<DAL.Inv_Store> UserAccessbileStores = new List<DAL.Inv_Store>();
        public static List<DAL.Sl_Customer> UserAccessbileCustomers = new List<DAL.Sl_Customer>();
        public static List<DAL.Sl_CustomerGroup> UserAccessbileCustomergroup = new List<DAL.Sl_CustomerGroup>();
        public static List<DAL.Pr_Vendor> UserAccessbileVendors = new List<DAL.Pr_Vendor>();
        public static List<DAL.Pr_VendorGroup> UserAccessbileVendorgroup = new List<DAL.Pr_VendorGroup>();
        public static List<DAL.Acc_Drawer> UserAccessibleDrawer = new List<DAL.Acc_Drawer>();
        public static List<DAL.Acc_Bank> UserAccessibleBanks = new List<DAL.Acc_Bank>();
        public static List<DAL.Acc_PayCard> UserAccessiblePayCards = new List<DAL.Acc_PayCard>();
        public static List<DAL.Salesman> UserAccessbileSalesman = new List<DAL.Salesman>();

       
        public static string MachineID { get; set; }
       
      
        static bool IsNetworkUp()
        {
            return NetworkInterface.GetIsNetworkAvailable();
        }
       
    }
}
