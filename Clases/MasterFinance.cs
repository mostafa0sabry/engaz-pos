﻿using ByStro.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro.Clases
{
    public static class MasterFinance
    {
        public class PayDetails
        {
            public int AccountID { get; set; }
            public int  PayAccountID { get; set; }
            public int  DiscountAccount { get; set; }
            public int StoreID { get; set; }
            public DateTime  Date { get; set; }
            public DateTime LastUpdateDate { get; set; }
            public int LastUpdateUserID { get; set; }
            public int InsertUser { get; set; }
            public DateTime InsertDate { get; set; }
            public Master.PayTypes PayTypes { get; set; }
            public   Master.SystemProssess ProcessType  { get; set; }
            public int ProcessID { get; set; }
            public string Notes { get; set; }


            public double Amount { get; set; }
            public double Discount { get; set; }
            
            public bool  IsPartCredit { get; set; }

        }

        public static int  ProcessPay(PayDetails details ,int? acctivityMainID = null )
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            Acc_Activity acctivity = new Acc_Activity()
            {

                Date = details.Date,
                StoreID = details.StoreID,
                Type = ((int)details.ProcessType).ToString(),
                TypeID = details.ProcessID .ToString(),
                Note = details.Notes ,
                InsertDate = details.Date,
                LastUpdateDate = details.LastUpdateDate,
                LastUpdateUserID = details.LastUpdateUserID  ,
                UserID = details.InsertUser 
            };
            if(acctivityMainID == null)
            {

            db.Acc_Activities.InsertOnSubmit(acctivity);
            db.SubmitChanges(); 
            }else
            {
                acctivity.ID = acctivityMainID.Value ;
            }
            switch (details .PayTypes )
            {
                case  Master.PayTypes.Drawer  :// Drawer
                case  Master.PayTypes.Bank :// Bank
                case  Master.PayTypes.Account :// Account
                    db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
                    {
                        ACCID = details.PayAccountID,
                        Debit = details.IsPartCredit ? details.Amount : 0,
                        Credit = details.IsPartCredit ? 0 :details.Amount,
                        AcivityID = acctivity.ID,
                        DueDate =details.Date,
                        Note = string.Format("{0}-{1}", details.Notes, LangResource.Pay),
                    });

                    break;

                case  Master.PayTypes.PayCard  ://Pay Card 
                    var card = db.Acc_PayCards.Where(x => x.ID == details.PayAccountID).SingleOrDefault();
                    var bank = db.Acc_Banks.Where(x => x.ID == card.BankID).SingleOrDefault();
                    db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
                    {
                        ACCID = bank.AccountID,
                        Debit =details.IsPartCredit ?details.Amount - (details.Amount * card.Commission) : 0,
                        Credit =details.IsPartCredit ? 0 :details.Amount - (details.Amount * card.Commission),
                        AcivityID = acctivity.ID,
                        DueDate =details.Date,
                        Note = string.Format("{0}-{1}", details.Notes, LangResource.Pay),
                    });
                    if (card.Commission > 0) db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
                    {
                        ACCID = card.CommissionAccount,
                        Debit =details.IsPartCredit ? (details.Amount * card.Commission) : 0,
                        Credit =details.IsPartCredit ? 0 : (details.Amount * card.Commission),
                        AcivityID = acctivity.ID,
                        DueDate =details.Date,
                        Note = string.Format("{0}-{2}-{1}", details.Notes, LangResource.Pay, LangResource.Commission),
                    });

                    break;

                default:
                    break;
            }


            // Part Account
            db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
            {
                ACCID = details. AccountID,
                Debit =details.IsPartCredit ? 0 :details.Amount,
                Credit =details.IsPartCredit ?details.Amount : 0,
                AcivityID = acctivity.ID,
                DueDate =details.Date,
                Note = string.Format("{0}-{1}", details.Notes, LangResource.Pay),
            });


            if (details .Discount  > 0)
            {
                db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
                {
                    ACCID =details. DiscountAccount,
                    Debit =details . IsPartCredit ? details.Discount  : 0,
                    Credit = details.IsPartCredit ? 0 : details.Discount ,
                    AcivityID = acctivity.ID,
                    Note = string.Format("{0}-{1}", details.Notes , LangResource.Discount),


                });
                db.Acc_ActivityDetials.InsertOnSubmit(new Acc_ActivityDetial()
                {
                    ACCID = details.AccountID,
                    Debit = details.IsPartCredit ? 0 : details.Discount,
                    Credit = details.IsPartCredit ? details.Discount  : 0,
                    AcivityID = acctivity.ID,
                    Note = string.Format("{0}-{1}", details.Notes , LangResource.Discount),
                });
            }
            db.SubmitChanges();
            return acctivity.ID;
        }
    }
}
