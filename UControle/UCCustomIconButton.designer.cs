﻿namespace ByStro.UControle
{
    partial class UCCustomIconButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.TableLayout.TableColumnDefinition tableColumnDefinition1 = new DevExpress.XtraEditors.TableLayout.TableColumnDefinition();
            DevExpress.XtraEditors.TableLayout.TableRowDefinition tableRowDefinition1 = new DevExpress.XtraEditors.TableLayout.TableRowDefinition();
            DevExpress.XtraEditors.TableLayout.TableRowDefinition tableRowDefinition2 = new DevExpress.XtraEditors.TableLayout.TableRowDefinition();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement1 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement2 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            this.colCaption = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colIconImage = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colSvgImage = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.elementBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TileView_Element = new DevExpress.XtraGrid.Views.Tile.TileView();
            this.colId = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colMasterId = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colName = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colIsPanel = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colIsShowDialog = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TileView_Element)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // colCaption
            // 
            this.colCaption.FieldName = "Caption";
            this.colCaption.Name = "colCaption";
            this.colCaption.Visible = true;
            this.colCaption.VisibleIndex = 3;
            // 
            // colIconImage
            // 
            this.colIconImage.FieldName = "IconImage";
            this.colIconImage.Name = "colIconImage";
            this.colIconImage.OptionsColumn.ReadOnly = true;
            this.colIconImage.Visible = true;
            this.colIconImage.VisibleIndex = 7;
            // 
            // colSvgImage
            // 
            this.colSvgImage.FieldName = "SvgImage";
            this.colSvgImage.Name = "colSvgImage";
            this.colSvgImage.Visible = true;
            this.colSvgImage.VisibleIndex = 4;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.elementBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.TileView_Element;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageEdit1,
            this.repositoryItemPictureEdit1});
            this.gridControl1.Size = new System.Drawing.Size(559, 354);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.TileView_Element});
            // 
            // elementBindingSource
            // 
            this.elementBindingSource.DataSource = typeof(ByStro.Clases.Element);
            // 
            // TileView_Element
            // 
            this.TileView_Element.Appearance.ItemNormal.BorderColor = System.Drawing.Color.Navy;
            this.TileView_Element.Appearance.ItemNormal.Options.UseBorderColor = true;
            this.TileView_Element.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.TileView_Element.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colMasterId,
            this.colName,
            this.colCaption,
            this.colSvgImage,
            this.colIsPanel,
            this.colIsShowDialog,
            this.colIconImage});
            this.TileView_Element.GridControl = this.gridControl1;
            this.TileView_Element.Name = "TileView_Element";
            this.TileView_Element.OptionsBehavior.AllowSmoothScrolling = true;
            this.TileView_Element.OptionsTiles.HighlightFocusedTileOnGridLoad = true;
            this.TileView_Element.OptionsTiles.HighlightFocusedTileStyle = DevExpress.XtraGrid.Views.Tile.HighlightFocusedTileStyle.BorderAndContent;
            this.TileView_Element.OptionsTiles.HorizontalContentAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TileView_Element.OptionsTiles.IndentBetweenItems = 25;
            this.TileView_Element.OptionsTiles.ItemBorderVisibility = DevExpress.XtraEditors.TileItemBorderVisibility.Always;
            this.TileView_Element.OptionsTiles.ItemSize = new System.Drawing.Size(120, 120);
            this.TileView_Element.OptionsTiles.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Center;
            this.TileView_Element.TileColumns.Add(tableColumnDefinition1);
            tableRowDefinition1.Length.Value = 70D;
            tableRowDefinition2.Length.Value = 35D;
            this.TileView_Element.TileRows.Add(tableRowDefinition1);
            this.TileView_Element.TileRows.Add(tableRowDefinition2);
            tileViewItemElement1.Column = this.colCaption;
            tileViewItemElement1.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileViewItemElement1.ImageOptions.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomInside;
            tileViewItemElement1.RowIndex = 1;
            tileViewItemElement1.Text = "colCaption";
            tileViewItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileViewItemElement2.Column = this.colIconImage;
            tileViewItemElement2.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileViewItemElement2.ImageOptions.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomInside;
            tileViewItemElement2.Text = "colIconImage";
            tileViewItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            this.TileView_Element.TileTemplate.Add(tileViewItemElement1);
            this.TileView_Element.TileTemplate.Add(tileViewItemElement2);
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.Visible = true;
            this.colId.VisibleIndex = 0;
            // 
            // colMasterId
            // 
            this.colMasterId.FieldName = "MasterId";
            this.colMasterId.Name = "colMasterId";
            this.colMasterId.Visible = true;
            this.colMasterId.VisibleIndex = 1;
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 2;
            // 
            // colIsPanel
            // 
            this.colIsPanel.FieldName = "IsPanel";
            this.colIsPanel.Name = "colIsPanel";
            this.colIsPanel.Visible = true;
            this.colIsPanel.VisibleIndex = 5;
            // 
            // colIsShowDialog
            // 
            this.colIsShowDialog.FieldName = "IsShowDialog";
            this.colIsShowDialog.Name = "colIsShowDialog";
            this.colIsShowDialog.Visible = true;
            this.colIsShowDialog.VisibleIndex = 6;
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // UCCustomIconButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "UCCustomIconButton";
            this.Size = new System.Drawing.Size(559, 354);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elementBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TileView_Element)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Tile.TileView TileView_Element;
        private System.Windows.Forms.BindingSource elementBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraGrid.Columns.TileViewColumn colId;
        private DevExpress.XtraGrid.Columns.TileViewColumn colMasterId;
        private DevExpress.XtraGrid.Columns.TileViewColumn colName;
        private DevExpress.XtraGrid.Columns.TileViewColumn colCaption;
        private DevExpress.XtraGrid.Columns.TileViewColumn colSvgImage;
        private DevExpress.XtraGrid.Columns.TileViewColumn colIsPanel;
        private DevExpress.XtraGrid.Columns.TileViewColumn colIsShowDialog;
        private DevExpress.XtraGrid.Columns.TileViewColumn colIconImage;
    }
}
