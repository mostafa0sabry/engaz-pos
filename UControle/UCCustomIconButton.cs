﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Tile;

namespace ByStro.UControle
{
    public partial class UCCustomIconButton : UserControl
    {
        public event EventHandler<Clases.Element> ElementClick;
        #region Proparety
        public ICollection<Clases.Element> ElementsDataSource
        {
            set { elementBindingSource.DataSource = value; }
        }
        public Orientation ElementOrientation
        {
            get => this.TileView_Element.OptionsTiles.Orientation;
            set => this.TileView_Element.OptionsTiles.Orientation = value;
        }
        public int ElementColumnCount
        {
            get => this.TileView_Element.OptionsTiles.ColumnCount;
            set => this.TileView_Element.OptionsTiles.ColumnCount = value;
        }
        public Size ElementItemSize
        {
            get => this.TileView_Element.OptionsTiles.ItemSize;
            set => this.TileView_Element.OptionsTiles.ItemSize = value;
        }
        public Color ElementBackColor
        {
            get => this.TileView_Element.Appearance.ItemNormal.BackColor;
            set => this.TileView_Element.Appearance.ItemNormal.BackColor = value;
        }
        public Color ElementBackColorPressed
        {
            get => this.TileView_Element.Appearance.ItemPressed.BackColor;
            set => this.TileView_Element.Appearance.ItemPressed.BackColor = value;
        }
        public Font ElementFont
        {
            get => this.TileView_Element.Appearance.ItemNormal.Font;
            set
            {
                this.TileView_Element.Appearance.ItemNormal.Options.UseFont = true;
                this.TileView_Element.Appearance.ItemNormal.Font = value;
            }
        }
     
        #endregion
        public UCCustomIconButton()
        {
            InitializeComponent();
            this.TileView_Element.Click += TileView_Click;
        }
        private void TileView_Click(object sender, EventArgs e)
        {
            if (e is DevExpress.Utils.DXMouseEventArgs args)
            {
                TileView view = sender as TileView;
                var row = view.GetFocusedRow(); 
                if (row != null && row is Clases.Element element)
                {
                    ElementClick?.Invoke(sender, element);
                }
            }
        }
    }
}