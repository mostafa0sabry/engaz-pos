﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ByStro.Forms.UserControls
{
    public partial class uc_AddExpenceAccount : DevExpress.XtraEditors.XtraUserControl
    {
        public uc_AddExpenceAccount()
        {
            InitializeComponent();
            RefreshData();
        }
        void RefreshData()
        {
            lookUpEdit1.Properties.DataSource = CurrentSession.UserAccessbileAccounts.Where(x => x.Number.ToString().StartsWith(
                     new DAL.ERPDataContext(Program.setting.con).Acc_Accounts.Where(xa => xa.ID == CurrentSession . Company.ExpensesAccount).Single().Number)).Select(x => new { x.ID , x.Name }).ToList();
            lookUpEdit1.Properties.DisplayMember = "Name";
            lookUpEdit1.Properties.ValueMember  = "ID";
            lookUpEdit1.EditValue = CurrentSession . Company.ExpensesAccount;


        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (txt_Name.Text.Trim() == string.Empty)
            {
                txt_Name.ErrorText = LangResource.ErrorCantBeEmpry;
                return;
            }
            DAL.ERPDataContext dbc = new DAL.ERPDataContext(Program.setting.con);
            if (dbc.Acc_Accounts.Where(x => x.ParentID == CurrentSession . Company.ExpensesAccount  && x.Name == txt_Name.Text.TrimEnd().TrimStart()).Select(x => x.Number).Count() > 0)
            {
                XtraMessageBox.Show("");
                return;
            }
            Master.InsertNewAccount(txt_Name.Text, Convert.ToInt32(lookUpEdit1.EditValue ), true, CurrentSession.user.AccountSecrecyLevel, 2);
            txt_Name.Text = string.Empty;
            
            CurrentSession.UserAccessbileAccounts = (from a in dbc.Acc_Accounts where a.Secrecy >= CurrentSession.user.AccountSecrecyLevel select a).ToList();
            lookUpEdit1.Properties.DataSource = CurrentSession.UserAccessbileAccounts.Where(x => x.Number.ToString().StartsWith(
                   dbc.Acc_Accounts.Where(xa => xa.ID == CurrentSession . Company.ExpensesAccount ).Single().Number)).Select(x => new { x.ID , x.Name }).ToList();
        }
    }
}
