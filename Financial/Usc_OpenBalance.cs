﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace ByStro.GUI
{
    public partial class Usc_OpenBalance : DevExpress.XtraEditors.XtraUserControl
    {
        private int _account;
        public int Account { get { return _account; } set{ _account = value; } }
        public int CapitalAccount { get; set; }

        public Usc_OpenBalance()
        {
            InitializeComponent();
            this.Load += Usc_OpenBalance_Load;
            cb_Balance.SelectedIndexChanged += cb_Balance_SelectedIndexChanged;
            lkp_Currancy.EditValueChanged += lkp_Currancy_EditValueChanged;
            RefreshData();

        }
        public  void RefreshData()
        {
            DAL.ERPDataContext db = new DAL.ERPDataContext(Program.setting.con); 
            var accounts = CurrentSession.UserAccessbileAccounts.Select(x => new { x.ID, x.Name, x.Number }).ToList();
            lookUpEdit_OpenBalanceAccount.Properties.DataSource = accounts;
            lookUpEdit_OpenBalanceAccount.Properties.ValueMember = "ID";
            lkp_Currancy.Properties.DataSource = db.Acc_Currencies.Select(x => new { x.ID, x.Name }).ToList();
            lkp_Currancy.Properties.ValueMember = "ID";
            lookUpEdit_OpenBalanceAccount.Properties.DisplayMember = "Name";
            lkp_Currancy.Properties.DisplayMember = "Name";
            lkp_Currancy.EditValue  = db.Acc_Currencies.First().ID;



        }

        private void Usc_OpenBalance_Load(object sender, EventArgs e)
        {
            layoutControl1.AllowCustomization = false;
            lookUpEdit_OpenBalanceAccount.EditValue  = CurrentSession.Company.CapitalAccount;
           
        }
       public void Delete()
        {
            Master.DeleteAccountAcctivity("0", Account.ToString());
        }
        public  bool ValidData()
        {
            
            if (cb_Balance.SelectedIndex > 0)
            {
                if (Convert.ToDouble(spn_Balance.EditValue) <= 0)
                {
                    this.spn_Balance.ErrorText = LangResource.ErrorValMustBeGreaterThan0;
                    this.spn_Balance.Focus();
                    return false;
                }
                if (date_Balance.EditValue == null)
                {
                    this.date_Balance.ErrorText = LangResource.ErrorCantBeEmpry;
                    this.date_Balance.Focus();
                    return false;
                }
                if (lkp_Currancy.EditValue == null)
                {
                    this.lkp_Currancy.ErrorText = LangResource.ErrorCantBeEmpry;
                    this.lkp_Currancy.Focus();
                    return false;
                }
                if (spn_Rate.EditValue == null && Convert.ToDouble(spn_Rate.EditValue) <= 0)
                {
                    this.spn_Rate.ErrorText = LangResource.ErrorValMustBeGreaterThan0;
                    this.spn_Rate.Focus();
                    return false;
                }
            }

            return true;
        }
        public void Save()
        {
           
            if (cb_Balance.SelectedIndex > 0)
            {
                Master.AccountOpenBalance OP = new Master.AccountOpenBalance();
                OP.IsDebit = (cb_Balance.SelectedIndex == 1);
                OP.Amount = Convert.ToDouble(spn_Balance.EditValue);
                OP.Date = date_Balance.DateTime;
                OP.CurrencyID = (int?)lkp_Currancy.EditValue ?? 1;
                OP.CurrencyRate = Convert.ToDouble((decimal?)spn_Rate.EditValue ?? 1);

                Master.CreatAccOpenBalance(Account, Name, OP);
            }
            else
            {
                Master.DeleteAccountAcctivity("0", Account.ToString());
            }
             
        }
        public void GetData()
        {
            Master.AccountOpenBalance OP = Master.GetAccountOpenBalance(Account);
            if (OP.Amount > 0)
            {
                cb_Balance.SelectedIndex = (OP.IsDebit) ? 1 : 2;
                spn_Balance.EditValue = OP.Amount;
                date_Balance.EditValue = OP.Date;
                OP.Date = date_Balance.DateTime;
                lkp_Currancy.EditValue = OP.CurrencyID;
                spn_Rate.EditValue = OP.CurrencyRate;
            }
            else cb_Balance.SelectedIndex = 0;

            cb_Balance.SelectedIndex = 0;

        }

        private void cb_Balance_SelectedIndexChanged(object sender, EventArgs e)
        {
            date_Balance.Enabled = spn_Balance.Enabled = lkp_Currancy.Enabled = (cb_Balance.SelectedIndex > 0);
            if (date_Balance.DateTime.Year < 1950)
                date_Balance.DateTime = DateTime.Now;
               
            
        }



        private void lkp_Currancy_EditValueChanged(object sender, EventArgs e)
        {
            DAL.ERPDataContext db = new DAL.ERPDataContext(Program.setting.con);
            int id;
            if (lkp_Currancy.EditValue != null && int.TryParse(lkp_Currancy.EditValue.ToString(), out id))
            {
                spn_Rate.EditValue = db.Acc_Currencies.Where(x => x.ID == id).Select(x => x.LastRate).FirstOrDefault();
                spn_Rate.Enabled = !(Convert.ToInt32(lkp_Currancy.EditValue) == 1);

            }
            else
            {
                spn_Rate.EditValue = null;
                spn_Rate.Enabled = false;

            }
        }

        private void gb_OpenBalance_Click(object sender, EventArgs e)
        {
            this.Size = gb_OpenBalance.Size; 
        }
    } 
}


