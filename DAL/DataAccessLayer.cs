﻿using System;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Security.Cryptography;
using System.IO;

 class DataAccessLayer
{
    public DataAccessLayer()
    {
        connSQLServer = new SqlConnection(ByStro.Properties.Settings.Default.Connection_String);
    }
    // اسم المستخدم عند الدخول
    public static String UserNameLogIn = "";

    // اسم المستخدم عند الدخول
    public static String Load_Bill = "NO";


    public static string Version= "Engaz POS V3.0";
    public static bool closeMainform = false;

    public static string paukupautomatic = "close";
    // الاضافة والتعديل والحذف
    //public static string UserAddEdit = "3";

     
    #region permation
    //Sales
    public static Boolean TypeUser = false;
    public static Boolean CS_12 = false;
    public static Boolean CS_13 = false;
    //bay
    public static Boolean CS_16 = false;
    public static Boolean CS_17 = false;

    // ربح الفاتورة
    public static Boolean CS_44 = false;
    #endregion

    public static String Finull_Sarial = "";

    public static Hoorof.clsHoorofArabic horof = new Hoorof.clsHoorofArabic();


    public SqlConnection connSQLServer = new SqlConnection();
    private SqlCommand cmd ;
    private SqlDataAdapter da = new SqlDataAdapter();
    private DataTable dt ;



    //=======================================ExecuteReader ============================================

    public DataSet ExecteRader_DataSet(String sql, System.Data.CommandType commandType, params SqlParameter[] param)
    {
        try
        {
            DataSet ds = new DataSet();
            if (connSQLServer.State == ConnectionState.Closed) connSQLServer.Open();
             cmd = new SqlCommand(sql, connSQLServer);
            cmd.CommandType = commandType;
            cmd.Parameters.AddRange(param);
            da.SelectCommand = cmd;
            da.Fill(ds);
            return ds;
        }
        catch //(Exception ex)
        {
            //MessageBox.Show(ex.Message.ToString());
            return null;
        }
        finally
        {
            if (connSQLServer.State == ConnectionState.Open) connSQLServer.Close();
            cmd.Dispose();
            //da.Dispose();
        }
    }
    //=======================================ExecuteReader ============================================
    public DataTable ExecteRader(String sql, System.Data.CommandType commandType, params SqlParameter[] param)
    {
        try
        {
            dt = new DataTable();
            if (connSQLServer.State == ConnectionState.Closed) connSQLServer.Open();
            cmd = new SqlCommand(sql, connSQLServer);
            cmd.CommandType = commandType;
            cmd.Parameters.AddRange(param);
            da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);
            return dt;
        }
        catch //(Exception ex)
        {
           // MessageBox.Show(ex.Message.ToString());
            return null;
        }
        finally
        {
            if (connSQLServer.State == ConnectionState.Open) connSQLServer.Close();
            cmd.Dispose();
            da.Dispose();
        }
    }
    //=======================================ExcuteNoneQuery ============================================                   

    public string Execute_SQL(String sql, System.Data.CommandType commandType, params SqlParameter[] param)
    {
        try
        {
            if (connSQLServer.State == ConnectionState.Closed) connSQLServer.Open();
            cmd = new SqlCommand(sql, connSQLServer);
            cmd.CommandType = commandType;
            cmd.Parameters.AddRange(param);
            cmd.ExecuteNonQuery();
            if (cmd.ExecuteScalar() != null)
            {
                return cmd.ExecuteScalar().ToString();
            }
            else
            {
                return "";
            }
        }
        catch
        {
            return null;
        }
        finally
        {
            if (connSQLServer.State == ConnectionState.Open) connSQLServer.Close();
            cmd.Dispose();
        }

    }

    public string Execute_SQL2(String sql, System.Data.CommandType commandType, params SqlParameter[] param)
    {
        try
        {
            if (connSQLServer.State == ConnectionState.Closed) connSQLServer.Open();
            cmd = new SqlCommand(sql, connSQLServer);
            cmd.CommandType = commandType;
            cmd.Parameters.AddRange(param);
            cmd.ExecuteNonQuery();
          
            return "";
           
        }
        catch
        {
            return null;
        }
        finally
        {
            if (connSQLServer.State == ConnectionState.Open) connSQLServer.Close();
            cmd.Dispose();
        }

    }



    //=======================================CreateParmeter ============================================                    
    public SqlParameter Parameter(String name, SqlDbType type, object value)
    {
        SqlParameter parm = new SqlParameter();
        parm.ParameterName = name;
        parm.SqlDbType = type;
        parm.Value = value;
        return parm;
    }





  



























    #region "ارجاع دالة التشفير"
    public static string DecryptData(string ciphertext)
    {

        try
        {

            string Encryptionkey = "ENG11MOHAMMED55519";

            byte[] cipherbytes = Convert.FromBase64String(ciphertext);

            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(Encryptionkey, new byte[] {
				0x1,
				0x11,
				0x36,
				0x97,
				0x69,
				0x8d,
				0x11,
				0x12,
				0x13,
				0x14,
				0x15,
				0x16,
				0x17
			});

                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);

                using (MemoryStream MS = new MemoryStream())
                {

                    using (CryptoStream CS = new CryptoStream(MS, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        CS.Write(cipherbytes, 0, cipherbytes.Length);
                        CS.Close();
                    }

                    ciphertext = Encoding.Unicode.GetString(MS.ToArray());
                }
            }


        }
        catch
        {
        }
        return ciphertext;
    }

    #endregion




    #region "دالة التفعيل"

    public static string GetHash(string s)
    {
        return GetHexString(new MD5CryptoServiceProvider().ComputeHash(new ASCIIEncoding().GetBytes(s)));
    }

    private static string GetHexString(byte[] bt)
    {
        string str1 = string.Empty;
        for (int index = 0; index < bt.Length; ++index)
        {
            int num1 = (int)bt[index];
            int num2 = 15;
            int num3 = num1 & num2;
            int num4 = 4;
            int num5 = num1 >> num4 & 15;
            string str2 = num5 <= 8 ? str1 + num5.ToString() : str1 + ((char)(num5 - 8 + 65)).ToString();
            str1 = num3 <= 8 ? str2 + num3.ToString() : str2 + ((char)(num3 - 8 + 65)).ToString();
            if (index + 1 != bt.Length && (index + 1) % 2 == 0)
                str1 += "-";
        }
        return str1;
    }

    #endregion






    #region"تحميل بيانات الشركة للطباعة "
    //public static DataTable Dt_CombanyData = new DataTable();
    //public static void Load_CombanyData()
    //{
    //    Dt_CombanyData.Clear();
    //    SqlCommand cmd = new SqlCommand("select *  from CombanyData ", connSQLServer);
    //    Openconn();
    //    Dt_CombanyData.Load(cmd.ExecuteReader());
    //    Closeconn();
    //}

    #endregion








    public static void MoveLast(Form parentForm, DataTable dt)
    {
        parentForm.BindingContext[dt].Position = parentForm.BindingContext[dt].Count - 1;
    }




    



  

    #region "منع المستخدم من كتابة الحروف والرموز"
    public static void UseNamberOnly(KeyPressEventArgs e)
    {
        if (!char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar != Convert.ToChar("."))
        {
            e.Handled = true;
        }
    }
    #endregion




    public static void first(DataGridView DGV)
    {
        try
        {
            DGV.CurrentCell = DGV.Rows[0].Cells[0];
        }
        catch { MessageBox.Show(" record not founded"); }
    }

    //===================================================================================
    public static void next(DataGridView dataGridView1)
    {
        try
        {
            Int32 selectedRowCount = dataGridView1.Rows.GetRowCount(DataGridViewElementStates.Selected);

            // index out of range on this line
            dataGridView1.Rows[dataGridView1.SelectedRows[selectedRowCount].Index].Selected = true;

            dataGridView1.FirstDisplayedScrollingRowIndex = selectedRowCount + 1;
        }
        catch //(Exception ex)
        {
            return;
        }
    }

    //===================================================================================
    public static void last(DataGridView DGV)
    {
        try
        {
            DGV.Rows[DGV.Rows.Count - 1].Selected = true;

            //DGV.CurrentCell = DGV.Rows[(DGV.Rows.Count) - 1].Cells[0];
        }
        catch { MessageBox.Show(" record not founded"); }
    }

    //===================================================================================



}

