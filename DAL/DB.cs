using System;
using System.Data.Linq.Mapping;
using System.Reflection;

namespace ByStro.DAL
{
    partial class DBDataContext
    {
        public static string DefualtConnection;
        [Function(Name = "GetDate", IsComposable = true)]
        public DateTime GetSystemDate()
        {
            return DateTime.Now;
     
            MethodInfo mi = MethodBase.GetCurrentMethod() as MethodInfo;
            return (DateTime)this.ExecuteMethodCall(this, mi, new object[] { }).ReturnValue;
        }
    }
}