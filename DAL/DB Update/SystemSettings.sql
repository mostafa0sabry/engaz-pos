
CREATE TABLE [dbo].[SystemSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UseScalBarcode] [bit] NOT NULL,
	[ItemCodeLength] [int] NOT NULL,
	[WeightCodeLength] [int] NOT NULL,
	[DivideWeightBy] [int] NOT NULL,
 CONSTRAINT [PK_SystemSettings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
INSERT [dbo].[SystemSettings] ( [UseScalBarcode], [ItemCodeLength], [WeightCodeLength], [DivideWeightBy]) VALUES ( 1, 7, 6, 10)
