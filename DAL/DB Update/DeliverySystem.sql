
ALTER TABLE Sales_Main
  ADD DriverID  [int] NULL;
CREATE TABLE [dbo].[Drivers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](max)NOT NULL,
 CONSTRAINT [PK_Drivers] PRIMARY KEY CLUSTERED 
( [ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[DeliveryOrder](
	[ID] [int]      IDENTITY(1,1)  NOT NULL,
	[DriverID]      [int]          NOT NULL,
	[Orders]	    [nvarchar](max)NOT NULL,
	[OrdersCount]   [int]		   NOT NULL,
	[OrdersAmount]  [float]        NOT NULL,
	[RecivedAmount] [float]		   NOT NULL,
	[HasReturned]   [bit]		   NOT NULL,
	[OutDate]       [DateTime]     NOT NULL,
	[ReturnDate]    [DateTime]         NULL,
	
 CONSTRAINT [PK_DeliveryOrder] PRIMARY KEY CLUSTERED 
( [ID] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]





